//
//  imageCore.swift
//  Scuola
//
//  Created by ISCA-IOS on 8/22/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ImageCore{
    private var urlApi: String = ""
    private var imageToUpload: [UIImage]
    private var progressBar: UIProgressView?
    private var lblPercentage: UILabel?
    
    var dataToSend: [String: String]?
    
    
    
    init(urlLink: String, imageToUpload: [UIImage], progressBars: UIProgressView?, lblPercentage: UILabel?) {
        self.urlApi = urlLink
        self.imageToUpload = imageToUpload
        self.progressBar = progressBars
        self.lblPercentage = lblPercentage
    }
    
    init(urlLink: String, imageToUpload: [UIImage]) {
        self.urlApi = urlLink
        self.imageToUpload = imageToUpload
    }
    
    private func start(){
        self.progressBar!.isHidden = false
        self.lblPercentage!.isHidden = false
    }
    private func end(progress: Progress){
        self.lblPercentage!.text = String(Int(progress.fractionCompleted * 100)) + "%"
        self.progressBar!.progress = Float(progress.fractionCompleted)
        print("test progress is: \(Int(progress.fractionCompleted * 100))%")
    }
    
    func uploadFile(_ completion: @escaping(_ result: Bool) -> Void){
        
        var currentIndex: Int = 0
        let url = urlApi
        if(!Reachability.isConnectedToInternet()){
            completion(false)
            return
        }
        start()
        for imageData in imageToUpload{
            Alamofire.upload(multipartFormData: { (form: MultipartFormData) in
                
                if let data = imageData.jpegData(compressionQuality: 0.5){
                    form.append(data, withName: "file", fileName: "photo.jpg", mimeType: "image/jpg")
                }
                for info in self.dataToSend!{
                    form.append(info.value.data(using: String.Encoding.utf8)!, withName: info.key)
                }
                
                
            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: nil) { (result: SessionManager.MultipartFormDataEncodingResult) in
                switch result{
                case .failure(let error):
                    print(error)
                case.success(request: let upload, streamingFromDisk: _ , streamFileURL: _ ):
                    upload.responseJSON { response in
                        switch response.result{
                        case .failure(let value):
                            print(value)
                        case .success(let value):
                            print("test upload image \(value)")
                            currentIndex = currentIndex + 1
                            if currentIndex == self.imageToUpload.count{
                                completion(true)
                            }
                            
                            
                        }
                    }
                    upload.uploadProgress(closure: { (progress: Progress) in
                        self.end(progress: progress)
                    })
                }
            }
            
        }
            
         
        
    }
}
