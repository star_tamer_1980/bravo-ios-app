//
//  internet.swift
//  School
//
//  Created by ISCA-IOS on 6/20/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension Alerts{
    struct internet{
        struct connection{
            struct failed {
                static let head = "Internet connection"
                static let description = "Error on internet connection"
                static let image = "http"
            }
            struct notFound {
                static let head = "Internet connection"
                static let description = "Not found internet connection"
                static let image = "http"
            }
        }
    }
}
