//
//  Alerts.swift
//  School
//
//  Created by ISCA-IOS on 6/20/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

class Alerts{
    struct warning {
        struct parentRequestType {
            static let title = "Warning"
            static let content = "You must choose type."
        }
        struct save {
            struct closeWithoutSave {
                static let title = "Warning"
                static let content = "Do you want close without save data??"
            }
            struct willSave {
                static let content = "We will save data."
            }
        }
        struct delete {
            struct areYouSure {
                static let title = "Warning"
                static let content = "Do you want delete?"
            }
            
        }
        
        
        
    }
    struct dropDownTitle {
        struct selectCategory {
            static let title = "Select category"
        }
        struct AllStage {
            static let title = "All stages"
        }
        struct AllGrade {
            static let title = "All grades"
        }
        struct AllClasses {
            static let title = "All classes"
        }
        struct selectClass {
            static let title = "Select class"
        }
        struct AllStudents {
            static let title = "All students"
        }
        
    }
}
