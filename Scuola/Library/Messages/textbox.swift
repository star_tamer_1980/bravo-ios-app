//
//  textbox.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension Alerts{
    struct textbox{
        
        struct phoneNumber{
            struct empty {
                static let head = "Error"
                static let description = "The phone number is empty, you must write the phone before login."
                static let image = "http"
            }
        }
        
        struct mandatary {
            static let head = "Error"
            static let description = "You must write data."
        }
        
        struct notesContentMandatary{
            static let description = "You must write content note."
        }
        struct notesCategoryMandatary{
            static let description = "You must choose category."
        }
        struct notesClassMandatary{
            static let description = "You must choose class."
        }
        
    }
    
    struct account {
        struct logout {
            static let head = "Logout"
            static let description = "You now will logout, do you want??"
        }
    }
}
