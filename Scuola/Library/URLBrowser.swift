//
//  URLBrowser.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
import StoreKit
class URLBrowser{
    public class func openURL(view: UIViewController,url: String){
        if let url = URL(string: url), UIApplication.shared.canOpenURL(url){
            if #available(iOS 10, *){
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }else{
                UIApplication.shared.openURL(url)
            }
        }else{
            let alert = UIAlertController(title: "Error", message: "This \(url) not number", preferredStyle: .alert)
            let alertOk = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(alertOk)
            view.present(alert, animated: true, completion: nil)
        }
    }
    
    
    public class func openAppleStore(view: UIViewController,url: String){
        if let url = NSURL(string: url), UIApplication.shared.canOpenURL(url as URL){
            if #available(iOS 10, *){
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }else{
                UIApplication.shared.openURL(url as URL)
            }
        }else{
            let alert = UIAlertController(title: "Error", message: "Can't open app store", preferredStyle: .alert)
            let alertOk = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(alertOk)
            view.present(alert, animated: true, completion: nil)
        }
    }
}
