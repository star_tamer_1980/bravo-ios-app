//
//  URLs.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation


struct URLs {
    private static let serverURL = "http://scuola.isca-group.com/"
    static let iTunesAppleLink = "itms-apps://itunes.apple.com/app/id1447938190"
    static let uploadFiles = serverURL + "attach_file.php"
    static let uploadFolders = serverURL + "files/"
    static let login = serverURL + "login.php"
    static let parentRequestList = serverURL + "parent/parent-request/parent_request_list.php"
    static let parentRequestTeachersList = serverURL + "parent/parent-request/teachers_list.php"
    static let parentRequestAdd = serverURL + "parent/parent-request/add.php"
    static let teacherRequestList = serverURL + "teacher/parent-request/parent_request_list.php"
    static let teacherRequestReplyAdd = serverURL + "teacher/parent-request/reply.php"
    static let managementRequestList = serverURL + "sm/parent-request/parent_request_list.php"
    static let managementRequestReplyAdd = serverURL + "sm/parent-request/reply.php"
    static let notesParentList = serverURL + "notes/parent/notes_list.php"
    static let notesTeacherList = serverURL + "notes/teacher/notes_list.php"
    static let categoryList = serverURL + "notes/category.php"
    static let studentList = serverURL + "notes/class_students.php"
    static let classesList = serverURL + "notes/teacher/teacher_classes.php"
    static let addNote = serverURL + "notes/teacher/add.php"
    static let notesManagementList = serverURL + "notes/management/notes_list.php"
    static let addNoteManagement = serverURL + "notes/management/add.php"
    static let schoolStructure = serverURL + "notes/classes.php"
    static let planParentList = serverURL + "plan/parent/list.php"
    static let planTeacherList = serverURL + "plan/teacher/list.php"
    static let planManagementList = serverURL + "plan/management/list.php"
    static let planAdd = serverURL + "plan/teacher/add.php"
    static let planEdit = serverURL + "plan/teacher/edit.php"
    static let planDelete = serverURL + "plan/teacher/delete.php"
}

