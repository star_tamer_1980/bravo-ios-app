//
//  GlobalStyle.swift
//  School
//
//  Created by ISCA-IOS on 6/17/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class GlobalColor: UIColor{
    
    struct APP{
        static let white            = UIColor.hexStringToUIColor(hex: "#FFFFFF", alpha: 1.0)
        static let metalic          = UIColor.hexStringToUIColor(hex: "#BFBFBF", alpha: 1.0)
        static let gray             = UIColor.hexStringToUIColor(hex: "#707070", alpha: 1.0)
        static let gray10           = UIColor.hexStringToUIColor(hex: "#707070", alpha: 0.1)
        static let blue             = UIColor.hexStringToUIColor(hex: "#4363CC", alpha: 1.0)
        static let blue50           = UIColor.hexStringToUIColor(hex: "#4363CC", alpha: 0.5)
        static let blue10           = UIColor.hexStringToUIColor(hex: "#4363CC", alpha: 0.1)
        static let green            = UIColor.hexStringToUIColor(hex: "#77D994", alpha: 1.0)
        static let green50          = UIColor.hexStringToUIColor(hex: "#77D994", alpha: 0.5)
        static let red              = UIColor.hexStringToUIColor(hex: "#E64EA7", alpha: 1.0)
    }
    
    struct Basic {
        static let textFieldActive  = UIColor.hexStringToUIColor(hex: "#707070", alpha: 0.2)
    }
    
}
