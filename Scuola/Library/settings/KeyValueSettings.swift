//
//  settings.swift
//  School
//
//  Created by ISCA-IOS on 7/23/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class KeyValueSettings: UIViewController{
    // if save data = 1 , not save = 0
    private static var isSaveData: String! = "isSaveData"
    
    private class func setData(_ name: String, _ value: String){
        let def = UserDefaults.standard
        def.set(value, forKey: name)
        def.synchronize()
    }
    private class func getData(_ name: String) -> String{
        let def = UserDefaults.standard
        if let result = def.object(forKey: name){
            return result as! String
        }
        return ""
    }
    
    
    class func setIsSaveData(_ value: String){
        setData(self.isSaveData, value)
    }
    class func getIsSaveData() -> String{
        return getData(self.isSaveData)
    }
    
    
}
