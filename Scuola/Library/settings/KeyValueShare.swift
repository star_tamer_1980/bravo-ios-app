//
//  KeyValueShare.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class KeyValueShare: UIViewController{
    private static var StudentId: String! = "StudentId"
    private static var SchoolId : String! = "SchoolId"
    private static var ManagementSchoolId: String! = "ManagementSchoolId"
    
    private class func setData(_ name: String, _ value: String){
        let def = UserDefaults.standard
        def.set(value, forKey: name)
        def.synchronize()
    }
    private class func getData(_ name: String) -> String{
        let def = UserDefaults.standard
        if let result = def.object(forKey: name){
            return result as! String
        }
        return ""
    }
    
    
    class func setStudentId(_ value: String){
        setData(self.StudentId, value)
    }
    
    class func getStudentId() -> String{
        return getData(self.StudentId)
    }
    class func setSchoolId(_ value: String){
        setData(self.SchoolId, value)
    }
    class func getSchoolId() -> String{
        return getData(self.SchoolId)
    }
    
    class func setManagementSchoolId(_ value: String){
        setData(self.ManagementSchoolId, value)
    }
    class func getManagementSchoolId() -> String{
        return getData(self.ManagementSchoolId)
    }
    
    
}
