//
//  InfoDevice.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
class InfoDevice{
    private class func getVersionMajor() -> Int{
        let OS = ProcessInfo().operatingSystemVersion
        return OS.majorVersion
    }
    private class func getVersionMinor() -> Int{
        let OS = ProcessInfo().operatingSystemVersion
        return OS.minorVersion
    }
    private class func getVersionPatch() -> Int{
        let OS = ProcessInfo().operatingSystemVersion
        return OS.patchVersion
    }
    class func getVersionOS() -> String{
        return String(self.getVersionMajor()) + "." + String(self.getVersionMinor()) + "." + String(self.getVersionPatch())
    }
}

