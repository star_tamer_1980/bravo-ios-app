//
//  CallPhones.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit


class  CallPhones {
    class func dialNumber(view: UIViewController , number: String){
        if let url = URL(string: "tel://+2" + number), UIApplication.shared.canOpenURL(url){
            if #available(iOS 10, *){
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }else{
                UIApplication.shared.openURL(url)
            }
        }else{
            print(URL(string: "tel://+2" + number))
            let alert = UIAlertController(title: "Error", message: "This tel://+2\(number) not number", preferredStyle: .alert)
            let alertOk = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(alertOk)
            view.present(alert, animated: true, completion: nil)
        }
    }
}
