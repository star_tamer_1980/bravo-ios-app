//
//  validateString.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation


class validatePhoneNumber{
    // get first 2 char from staring phone and maches with code country for egypt
    class func matchCodeCountry(value: String) -> Bool{
        if String(value.prefix(2)) == "+2"{
            return true
        }else{
            return false
        }
    }
    
    // if phone != 10 char
    class func ifNotEqual10(value: String) -> Bool{
        if (value.characters.count == 10) || (value.characters.count == 11){
            return true
        }
        return false
    }
    
    // add country code in starting phone if not found
    class func addCodeCountry(value: String) -> String{
        if String(value.prefix(2)) == "+20"{
            return value
        }else{
            return "+20" + value
        }
    }
    // if phone have 0 remove it
    class func remove0(value: inout String) -> String{
        if value.prefix(1) == "0"{
            value.remove(at: value.startIndex)
        }
        return value
    }
    
    // if phone numbers contain 10 - 11 - 12 - 15 on start
    class func isHaveCodeMobilePhone(value: String) -> Bool{
        if (value.prefix(2) == "12" || value.prefix(2) == "11" || value.prefix(2) == "15" || value.prefix(2) == "10"){
            return true
        }
        return false
    }
    
    
    class func convertIntToStringHexa(Hourse: Int, Minutes: Int) -> String {
        var totalHours: String = "00"
        var totalMinuts: String = "00"
        if Hourse < 10{
            totalHours = String("0\(Hourse)")
        }else{
            totalHours = String("\(Hourse)")
        }
        
        if Minutes < 10{
            totalMinuts = String("0\(Minutes)")
        }else{
            totalMinuts = String("\(Minutes)")
        }
        return "\(totalHours):\(totalMinuts)"
    }
    
    
    class func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
