//
//  TimerUtil.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation


class timerUtil{
    //MARK: Delay func
    class func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay *
                Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}
