//
//  DateUtile.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//


import UIKit



class DateUtil{
    
    
    class func getCustomDate() -> DateComponents{
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        return components
    }
    
    class func getCurrentDate() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    
    
    class func getCurrentDateTime() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm"
        return formatter.string(from: date)
    }
    
    
    class func getNextDayMonth(vDate: Int) -> (dayWeek: String, dayDate: String, month: String, monthName: String, year: String){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let dateIs = Calendar.current.date(byAdding: .day, value: vDate, to: Date())!
        let monthDay = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let weekDay = ["", "SUN", "MON", "TU", "WE", "TH", "FR", "SAT"]
        let dateData = Calendar.current.dateComponents([.weekday, .year, .month] , from: dateIs)
        let day = weekDay[dateData.weekday ?? 0]
        let MonthName = monthDay[dateData.month ?? 0]
        return (day, dateFormatter.string(from: dateIs), String(dateData.month ?? 0), String(MonthName), String(dateData.year ?? 0))
    }
    
    
    class func getDifferentBetween2Dates(startDate: String) -> DateComponents{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var formatedStartDate = Date()
        if dateFormatter.date(from: startDate) != nil{
            formatedStartDate = dateFormatter.date(from: startDate)!
        }
        let currentDate = Date()
        let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
        let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate, to: currentDate)
        return differenceOfDate
    }
    
    
    class func getDifferentBetween2Times(startTime: String, endTime: String) -> DateComponents{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        let item = startTime
        let startTime = dateFormatter.date(from: item)
        let to = endTime
        let toTime = dateFormatter.date(from: to)
        
        let components = Set<Calendar.Component>([.second, .minute, .hour])
        let differenceOfDate = Calendar.current.dateComponents(components, from: startTime!, to: toTime!)
        return differenceOfDate
        
    }
    
    class func convertStringToDate(value: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateFinal = dateFormatter.date(from: value)
        return dateFinal ?? Date()
    }
    
    
}
