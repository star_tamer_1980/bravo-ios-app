//
//  teacherReplyNetwork.swift
//  School
//
//  Created by ISCA-IOS on 7/24/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func teacherReply(_ idRequest: String, _ replyContent: String, _ completion: @escaping(_ result: Bool, _ msg: String?) -> Void){
        let url = URLs.teacherRequestReplyAdd
        let parameters = [
            "id": idRequest,
            "reply": replyContent
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let error):
                print(error.localizedDescription)
                completion(false, error.localizedDescription)
            case .success(let value):
                print(value)
                let json = JSON(value)
                completion(json["status"].bool ?? true, json["msg"].string ?? "success")
            }
        }
    }
}
