//
//  requestTeacherNib.swift
//  School
//
//  Created by ISCA-IOS on 7/24/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class requestTeacherNib: UITableViewCell {

    @IBOutlet weak var btnReplyHandler: btnReplyDev!
    @IBOutlet weak var imgStudent: ImageOptions!
    @IBOutlet weak var lblStudentName: BigLabel!
    @IBOutlet weak var lblStudentClass: SmallLabel!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var tvDescription: GrayTextView!
    @IBOutlet weak var lblDate: SmallLabel!
    @IBOutlet weak var btnAttachmentHandle: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnReplyHandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cellConfiguration(studentImage: String, studentName: String, studentClass: String, teacherName: String, description: String, dateAdd: String, haveAttachment: Bool){
        
        let url = URL(string: studentImage)
        imgStudent.kf.setImage(with: url)
        lblStudentName.text = studentName
        lblStudentClass.text = studentClass
        lblTeacherName.text = teacherName
        tvDescription.text = description
        lblDate.text = dateAdd
        if haveAttachment != true{
            btnAttachmentHandle.isHidden = true
        }else{
            btnAttachmentHandle.isHidden = false
        }
    }
    
}
