//
//  TeacherRequestAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/23/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class TeacherRequestAdapter: UIViewController {
    var celHeightClosed: CGFloat = 360.0
    var celHeightOpened: CGFloat = 205.0
    @IBOutlet weak var tblOpenRequest: UITableView!
    @IBOutlet weak var tblClosedRequest: UITableView!
    var openedRequests = [parentRequestModel]()
    var closedRequests = [parentRequestModel]()
    let cellIdentifier = "requestTeacherNib"
    var limitOpened: String = "5"
    var limitClosed: String = "5"
    var isClosed = true
    var isLoadingOpened: Bool = false
    var isLoadingClosed: Bool = false
    var isLoadMoreOpened: Bool = true
    var isLoadMoreClosed: Bool = true
    var schoolId = ""
    var userId = ""
    var listClosedFromLocal: Bool = true
    var listOpenedFromLocal: Bool = true
    lazy var refresherForOpened: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(RfresherOpened), for: .valueChanged)
        return refresher
    }()
    lazy var refresherForClosed: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(RfresherClosed), for: .valueChanged)
        return refresher
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        schoolId = KeyValueShare.getSchoolId()
        userId = DatabaseManager.getInstance().getTeacherInfo().id
        getData()
    }
    override func viewWillAppear(_ animated: Bool) {
        if KeyValueSettings.getIsSaveData() == "1"{
            getData()
            KeyValueSettings.setIsSaveData("0")
        }
    }

    func getData(){
        print("get data local")
        DatabaseManager.getInstance().get_parent_request_List("0", "2") { (haveRequests: Bool, openedRequestsList: [parentRequestModel]) in
            if haveRequests == true{
                self.openedRequests = openedRequestsList
            }else{
                print("test opened error")
            }
            
        }
        DatabaseManager.getInstance().get_parent_request_List("1", "2") { (haveRequests: Bool, openedRequestsList: [parentRequestModel]) in
            if haveRequests == true{
                self.closedRequests = openedRequestsList
            }else{
                print("test closed error")
            }
            
        }
        print("get data online")

        self.tblClosedRequest.reloadData()
        self.tblOpenRequest.reloadData()
        handleRefresh()
    }

    @objc func handleRefresh(){
        RfresherOpened()
        RfresherClosed()

    }


    @objc func RfresherOpened(){
        self.refresherForOpened.endRefreshing()
        self.tblOpenRequest.reloadData()
        guard !isLoadingOpened else {
            return
        }
        isLoadingOpened = true
        
        API.TeacherRequestList(userId: userId, isClosed: "0", itemCount: "0", limit: "5", school_id: schoolId) { (result: Bool, msg: String, requests: [parentRequestModel]?, isLoadMore: Bool) in
            self.isLoadingOpened = false
            if result == true{
                if let requests = requests{
                    self.openedRequests = requests
                    self.tblOpenRequest.reloadData()
                    self.isLoadMoreOpened = isLoadMore
                    self.limitOpened = "10"
                }
            }
        }
    }

    @objc func RfresherClosed(){
        self.refresherForClosed.endRefreshing()
        self.tblClosedRequest.reloadData()
        guard !isLoadingClosed else {
            return
        }
        self.isLoadingClosed = true
        
        API.TeacherRequestList(userId: userId, isClosed: "1", itemCount: "0", limit: "5", school_id: schoolId) { (result: Bool, msg: String, requests: [parentRequestModel]?, isLoadMore: Bool) in
            self.isLoadingClosed = false
            if result == true{
                if let requests = requests{
                    self.closedRequests = requests
                    self.tblClosedRequest.reloadData()
                    self.isLoadMoreClosed = isLoadMore
                    self.limitClosed = "10"
                }
            }else{
                self.isLoadMoreClosed = false
            }
        }
    }



    @objc func handleRfreshForOpened(){
        self.refresherForOpened.endRefreshing()
        self.tblOpenRequest.reloadData()
        guard !isLoadingOpened else {
            return
        }
        isLoadingOpened = true
        API.TeacherRequestList(userId: userId, isClosed: "0", itemCount: String(openedRequests.count), limit: self.limitOpened, school_id: schoolId) { (result: Bool, msg: String, requests: [parentRequestModel]?, isLoadMore: Bool) in
            self.isLoadingOpened = false
            if result == true{
                if let requests = requests{
                    if self.openedRequests.count == 0{
                        self.openedRequests = requests
                    }else{
                        self.openedRequests.append(contentsOf: requests)
                    }

                    self.tblOpenRequest.reloadData()
                    self.isLoadMoreOpened = isLoadMore
                    self.limitOpened = "10"
                }
            }
        }
    }

    @objc func handleRfreshForClosed(){
        self.refresherForClosed.endRefreshing()
        self.tblClosedRequest.reloadData()
        guard !isLoadingClosed else {
            return
        }
        self.isLoadingClosed = true
        API.TeacherRequestList(userId: userId, isClosed: "0", itemCount: String(closedRequests.count), limit: self.limitClosed, school_id: schoolId) { (result: Bool, msg: String, requests: [parentRequestModel]?, isLoadMore: Bool) in
            self.isLoadingClosed = false
            if result == true{
                if let requests = requests{
                    if self.closedRequests.count == 0{
                        self.closedRequests = requests
                    }else{
                        self.closedRequests.append(contentsOf: requests)
                    }
                    self.tblClosedRequest.reloadData()
                    self.isLoadMoreClosed = isLoadMore
                    self.limitClosed = "10"
                }
            }else{
                self.isLoadMoreClosed = false
            }
        }
    }

    func loadOpenedMore(){
        guard !isLoadingOpened else {
            return
        }
        guard isLoadMoreOpened else {
            return
        }
        //isLoadingOpened = true
        handleRfreshForOpened()
    }

    func loadClosedMore(){
        guard !isLoadingClosed else {
            return
        }
        guard isLoadMoreClosed else {
            return
        }
        //isLoadingClosed = true
        handleRfreshForClosed()
    }
    @objc func requestReply( _ sender: btnReplyDev){
        let storyboard = UIStoryboard(name: "teacherRequest", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TeacherRequestReply") as! TeacherRequestReply
        controller.request = sender.parentRequest!
        self.present(controller, animated: true)
    }
    
}

extension TeacherRequestListVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == tblClosedRequest{
            count = closedRequests.count
        }else{
            count = openedRequests.count
        }
        return count!
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblClosedRequest{
            let cell = tableView.dequeueReusableCell(withIdentifier: "parentRequestClosedNib", for: indexPath) as! parentRequestClosedNib
            cell.cellConfiguration(requestData: closedRequests[indexPath.row])
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! requestTeacherNib
            cell.btnReplyHandler.addTarget(self, action: #selector(TeacherRequestListVC.requestReply(_:)), for: .touchUpInside)
            cell.btnReplyHandler.parentRequest = openedRequests[indexPath.row]
            cell.cellConfiguration(studentImage: openedRequests[indexPath.row].student_photo,
                                   studentName: openedRequests[indexPath.row].name,
                                   studentClass: openedRequests[indexPath.row].class_name,
                                   teacherName: openedRequests[indexPath.row].teacher_name,
                                   description: openedRequests[indexPath.row].request,
                                   dateAdd: openedRequests[indexPath.row].create_date,
                                   haveAttachment: false)
            return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "ParentRequest", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "ParentRequestDetailsVC") as! ParentRequestDetailsVC
        if tableView == tblClosedRequest{
            myController.request = closedRequests[indexPath.row]
        }else{
            myController.request = openedRequests[indexPath.row]
        }
        self.present(myController, animated: true)
    }

}

extension TeacherRequestListVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat?
        if tableView == tblOpenRequest{
            height = celHeightOpened
        }else{
            height = celHeightClosed
        }
        return height!

    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        return view
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var count: Int?
        if tableView == tblClosedRequest {
            count = closedRequests.count
            if indexPath.row == count!-1{
                self.loadClosedMore()
            }
        }else{
            count = openedRequests.count
            if indexPath.row == count!-1{
                self.loadOpenedMore()
            }
        }
    }
}
