//
//  TeacherRequestListVC.swift
//  School
//
//  Created by ISCA-IOS on 7/23/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class TeacherRequestListVC: TeacherRequestAdapter {
    
    @IBOutlet weak var btnOpenedHandler: UIButton!
    @IBOutlet weak var btnClosedhandler: UIButton!
    @IBOutlet weak var tblOpenRequestConstrainWidth: NSLayoutConstraint!
    @IBOutlet weak var tblClosedRequestConstrainWidth: NSLayoutConstraint!
    @IBOutlet weak var viRequestsContainer: UIView!
    @IBOutlet weak var viTitle: UIView!
    var currentRequest = "closed"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblClosedRequest.tableFooterView = UIView()
        self.tblClosedRequest.separatorInset = .zero
        self.tblClosedRequest.contentInset = .zero
        self.tblClosedRequest.separatorStyle = .none
        self.tblClosedRequest.addSubview(refresherForClosed)
        self.tblClosedRequest.register(UINib.init(nibName: "parentRequestClosedNib", bundle: nil), forCellReuseIdentifier: "parentRequestClosedNib")
        self.tblClosedRequest.dataSource = self
        self.tblClosedRequest.delegate = self
        
        self.tblOpenRequest.tableFooterView = UIView()
        self.tblOpenRequest.separatorInset = .zero
        self.tblOpenRequest.contentInset = .zero
        self.tblOpenRequest.separatorStyle = .none
        self.tblOpenRequest.addSubview(refresherForOpened)
        self.tblOpenRequest.register(UINib.init(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        self.tblOpenRequest.dataSource = self
        self.tblOpenRequest.delegate = self
        
        btnOpenedHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        viTitle.backgroundColor = GlobalColor.APP.blue
        tblOpenRequestConstrainWidth.constant = view.bounds.width
        tblClosedRequestConstrainWidth.constant = view.bounds.width
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(animateToOpen))
        swipeLeft.direction = .left
        viRequestsContainer.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(animateToClose))
        swipeRight.direction = .right
        viRequestsContainer.addGestureRecognizer(swipeRight)
    }
    
    @objc func animateToOpen(UITapGestureRecognizer: UISwipeGestureRecognizer){
        print("left to right")
        
        animteToOpen()
        
    }
    @objc func animateToClose(UITapGestureRecognizer: UISwipeGestureRecognizer){
        print("right to left")
        animeToClose()
        
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOpenedAction(_ sender: Any) {
        animteToOpen()
        
    }
    @IBAction func btnClosedAction(_ sender: Any) {
        animeToClose()
    }
    @IBAction func btnAddRequest(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "ParentRequest", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "ParentRequestAddVC") as! ParentRequestAddVC
        self.present(myController, animated: true)
    }
    
    func animteToOpen(){
        self.isClosed = false
        btnOpenedHandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
        btnClosedhandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        if currentRequest != "opened"{
            currentRequest = "opened"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblClosedRequest.frame.origin.x -= self.view.bounds.width
                self.tblOpenRequest.frame.origin.x -= self.view.bounds.width
            }, completion: nil)
        }
    }
    func animeToClose(){
        self.isClosed = true
        btnOpenedHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnClosedhandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
        if currentRequest != "closed"{
            currentRequest = "closed"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblClosedRequest.frame.origin.x += self.view.bounds.width
                self.tblOpenRequest.frame.origin.x += self.view.bounds.width
            }, completion: nil)
        }
    }
    
}
