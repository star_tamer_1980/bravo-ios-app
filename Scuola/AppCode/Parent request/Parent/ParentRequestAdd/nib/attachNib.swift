//
//  attachNib.swift
//  School
//
//  Created by ISCA-IOS on 7/21/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Kingfisher

class attachNib: UITableViewCell {

    @IBOutlet weak var imgAttach: UIImageView!
    @IBOutlet weak var lblAttachName: UILabel!
    @IBOutlet weak var btnRemoveHandle: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func celConfiger(image: UIImage){
        imgAttach.image = image
    }
    func celConfigerURL(nameFile: String){
        self.lblAttachName.text = nameFile
        let url = URL(string: URLs.uploadFolders + nameFile)
        print("test url \(URLs.uploadFolders + nameFile)")
        imgAttach.kf.setImage(with: url)
    }
    
}
