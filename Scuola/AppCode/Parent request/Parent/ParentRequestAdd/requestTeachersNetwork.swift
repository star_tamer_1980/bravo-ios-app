//
//  requestTeachersNetwork.swift
//  School
//
//  Created by ISCA-IOS on 7/16/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension API{
    class func getTeachersForRequests(_ class_id: String, _ complition: @escaping(_ result: Bool?, _ msg: String?, _ teachers: [teacherRequestModel]?) -> Void){
        let url = URLs.parentRequestTeachersList
        let parameters = [
        "class_id": class_id
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                complition(false, value.localizedDescription, nil)
            case .success(let value):
                print(value)
                let json = JSON(value)
                if json["status"].bool! == false{
                    complition(false, json["msg"].string!, nil)
                }else{
                    guard let resultArr = json["Result"].array else{
                        complition(false, json["msg"].string!, nil)
                        return
                    }
                    var teachersList = [teacherRequestModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let teacher = teacherRequestModel()
                        teacher.teacher_user_id = data["teacher_user_id"]?.string ?? "0"
                        teacher.name = data["name"]?.string ?? ""
                        teachersList.append(teacher)
                    }
                    complition(true, json["msg"].string!, teachersList)
                }
                
            }
        }
    }
}
