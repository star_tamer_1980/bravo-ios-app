//
//  teachersNameAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/17/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class teachersNameAdapter: UIViewController {
    
    @IBOutlet weak var tblTeachersName: UITableView!
    var teachersName = [teacherRequestModel]()
    var teacher_id: String = "0"

    @IBOutlet weak var tblAttachmentFiles: UITableView!
    var attachmentFiles = [String]()
    var attachmentFilesResources = [UIImage]()
    var celTable: String = "attachNib"
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    

    func getData(class_id: String){
        print("teacher test 0")
        API.getTeachersForRequests(class_id) { (status: Bool?, msg: String?, teachers: [teacherRequestModel]?) in
            print("teacher test 1")
            if status == true{
                if let teachers = teachers{
                    self.teachersName = teachers
                    self.tblTeachersName.reloadData()
                    print("teacher test2")
                }
                
            }else{
                print("not found teachers")
            }
        }
    }

}

extension ParentRequestAddVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == tblTeachersName{
            count = teachersName.count
        }else{
            count = attachmentFilesResources.count
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblTeachersName{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
            cell.textLabel?.text = teachersName[indexPath.row].name
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "attachNib", for: indexPath) as! attachNib
            cell.celConfiger(image: attachmentFilesResources[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblTeachersName{
            self.choosedTeacher = teachersName[indexPath.row]
            self.lblTitleDrowDownMenu.text = choosedTeacher.name
            self.teacher_id = choosedTeacher.teacher_user_id
            self.tblTeachersName.isHidden = true
        }else{
            
        }
    }
}
extension ParentRequestAddVC: UITableViewDelegate{
    
}
