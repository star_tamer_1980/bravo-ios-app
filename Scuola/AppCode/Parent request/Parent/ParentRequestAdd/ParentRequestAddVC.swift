//
//  ParentRequestAddVC.swift
//  School
//
//  Created by ISCA-IOS on 7/16/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
import TTGSnackbar


class ParentRequestAddVC: teachersNameAdapter, UITextViewDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var viDropDownMenu: UIView!
    @IBOutlet weak var lblTitleDrowDownMenu: Blue12Label!
    @IBOutlet weak var viTitle: UIView!
    @IBOutlet weak var viFooter: UIView!
    @IBOutlet weak var viFooterConstraint: NSLayoutConstraint!
    var keyboardHeight: CGFloat = 0.0
    @IBOutlet weak var imgStudent: ImageOptions!
    @IBOutlet weak var lblStudentName: WhiteLabel!
    @IBOutlet weak var lblStudentClass: WhiteLabel!
    var studentInfo = StudentModel()
    var choosedTeacher = teacherRequestModel()
    var parentRequestBackup = parentRequestModel()
    @IBOutlet weak var tvRequest: UITextView!
    @IBOutlet weak var btnManagementHandler: UIButton!
    @IBOutlet weak var btnTeacherHandler: UIButton!
    @IBOutlet weak var btnAddHandler: Blue!
    @IBOutlet weak var btnAttachHandler: UIButton!
    var typeSelecting: Int = 0 // 0 => not selected, 1 => teacher, 2 => management
    var request_from: Int = 0 // 0=> not signed, 1 => teacher, 2 => management
    
    
    
    var feature_type = "0"
    var feature_id = "0"
    var school_id = "0"
    @IBOutlet weak var proUpload: UIProgressView!
    @IBOutlet weak var lblPercentage: UILabel!
    var imagePicker: UIImagePickerController!
    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parentRequestBackup = DatabaseManager.getInstance().get_parent_request_add_data(student_id: KeyValueShare.getStudentId())
        if parentRequestBackup.student_id != ""{
            tvRequest.text = parentRequestBackup.request
            self.teacher_id = parentRequestBackup.teacher_user_id
            self.lblTitleDrowDownMenu.text = parentRequestBackup.teacher_name
            if parentRequestBackup.request_from == "2"{
                typeSelecting = 2
                selectType()
            }else{
                typeSelecting = 1
                selectType()
            }
        }
        tblTeachersName.tableFooterView = UIView()
        tblTeachersName.separatorInset = .zero
        tblTeachersName.contentInset = .zero
        tblTeachersName.separatorStyle = .none
        tblTeachersName.dataSource = self
        tblTeachersName.delegate = self
        
        
        tblAttachmentFiles.tableFooterView = UIView()
        tblAttachmentFiles.separatorInset = .zero
        tblAttachmentFiles.contentInset = .zero
        tblAttachmentFiles.separatorStyle = .none
        tblAttachmentFiles.register(UINib(nibName: "attachNib", bundle: nil), forCellReuseIdentifier: "attachNib")
        tblAttachmentFiles.dataSource = self
        tblAttachmentFiles.delegate = self
        
        
        tvRequest.delegate = self
        studentInfo = DatabaseManager.getInstance().getStudentInfo(idStudent: KeyValueShare.getStudentId())
        viTitle.backgroundColor = GlobalColor.APP.blue
        let url = URL(string: studentInfo.student_photo)
        imgStudent.kf.setImage(with: url)
        lblStudentName.text = studentInfo.first_name + " " + studentInfo.last_name
        lblStudentClass.text = studentInfo.class_name
        btnManagementHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnTeacherHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnAddHandler.isEnabled = false
        self.getData(class_id: studentInfo.class_id)
        let tapDrop = UITapGestureRecognizer(target: self, action: #selector(tapDropMenu))
        viDropDownMenu.addGestureRecognizer(tapDrop)
    }
    @objc func tapDropMenu(tapGesture: UITapGestureRecognizer){
        self.tblTeachersName.isHidden = false
    }
    @IBAction func btnBack(_ sender: Any) {
        if tvRequest.text.count == 0{
            self.dismiss(animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: Alerts.warning.save.closeWithoutSave.title, message: Alerts.warning.save.closeWithoutSave.content, preferredStyle: .alert)
            let AlertYes = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
                let message = TTGSnackbar(message: Alerts.warning.save.willSave.content, duration: .long)
                message.show()
                let requestData = parentRequestModel()
                requestData.student_id = KeyValueShare.getStudentId()
                requestData.request = self.tvRequest.text
                requestData.teacher_user_id = self.teacher_id
                requestData.teacher_name = self.lblTitleDrowDownMenu.text ?? ""
                requestData.request_from = String(self.typeSelecting)
                requestData.class_id = self.studentInfo.class_id
                requestData.school_id = self.studentInfo.school_id
                DatabaseManager.getInstance().delete_parent_request_add(student_id: KeyValueShare.getStudentId())
                DatabaseManager.getInstance().save_Parent_request_add_Info(requestData)
                
                self.dismiss(animated: true, completion: nil)
            }
            let AlertNo = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(AlertYes)
            alert.addAction(AlertNo)
            self.present(alert, animated: true)
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if tvRequest.text.count != 0{
            btnAddHandler.isEnabled = true
        }else{
            btnAddHandler.isEnabled = false
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        self.viFooterConstraint.constant = 0.0
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.viFooterConstraint.constant = keyboardRectangle.height
        }
    }
    
    @IBAction func btnManagementAction(_ sender: Any) {
        typeSelecting = 2
        selectType()
    }
    @IBAction func btnTeacherAction(_ sender: Any) {
        typeSelecting = 1
        selectType()
    }
    @IBAction func btnAddAction(_ sender: Any) {
        var isHaveAttach: Bool = false
        if self.attachmentFilesResources.count != 0{
            isHaveAttach = true
        }
        if typeSelecting != 0{
            self.btnAddHandler.isEnabled = false
            API.addParentRequest(student_id: KeyValueShare.getStudentId(), teacher_user_id: teacher_id, request_from: String(self.typeSelecting), request: tvRequest.text, class_id: studentInfo.class_id, school_id: studentInfo.school_id) { (result: Bool, msg: String, id: Int) in
                if result == true{
                    KeyValueSettings.setIsSaveData("1")
                    
                    
                    if isHaveAttach == true{
                        var dataSend = [String: String]()
                        dataSend["school_id"] = self.studentInfo.school_id
                        dataSend["feature_id"] = String(id)
                        dataSend["isParent"] = "1"
                        dataSend["feature_type"] = "1"
                        
                        //let uploadData = ImageCore(urlLink: URLs.uploadFiles, imageToUpload: self.attachmentFilesResources)
                        let uploadData = ImageCore(urlLink: URLs.uploadFiles, imageToUpload: self.attachmentFilesResources, progressBars: self.proUpload, lblPercentage: self.lblPercentage)
                        uploadData.dataToSend = dataSend
                        
                        
                        
                        uploadData.uploadFile({ (result: Bool) in
                            let alert = UIAlertController(title: "result", message: msg, preferredStyle: .alert)
                            let AlertOK = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                                DatabaseManager.getInstance().delete_parent_request_add(student_id: KeyValueShare.getStudentId())
                                self.dismiss(animated: true, completion: nil)
                            })
                            alert.addAction(AlertOK)
                            self.present(alert, animated: true)
                            
                        })
                    }else{
                        let alert = UIAlertController(title: "result", message: msg, preferredStyle: .alert)
                        let AlertOK = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                            DatabaseManager.getInstance().delete_parent_request_add(student_id: KeyValueShare.getStudentId())
                            self.dismiss(animated: true, completion: nil)
                        })
                        alert.addAction(AlertOK)
                        self.present(alert, animated: true)
                    }
                   
                    
                }else{
                    let alert = UIAlertController(title: "result", message: msg, preferredStyle: .alert)
                    let AlertOK = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        
                        self.btnAddHandler.isEnabled = true
                    })
                    alert.addAction(AlertOK)
                    self.present(alert, animated: true)
                }
                
            }
        }else{
            let alert = UIAlertController(title: Alerts.warning.parentRequestType.title, message: Alerts.warning.parentRequestType.content, preferredStyle: .alert)
            let alertOk = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(alertOk)
            self.present(alert, animated: true)
            self.btnAddHandler.isEnabled = true
        }
    }

    
    func selectType(){
        if typeSelecting == 2{
            // is management
            btnManagementHandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
            btnManagementHandler.setLeftImage(imageName: UIImage(named: "radioSelected")!, renderMode: UIImage.RenderingMode.alwaysOriginal)
            btnTeacherHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
            btnTeacherHandler.setLeftImage(imageName: UIImage(named: "radioDeselected")!, renderMode: UIImage.RenderingMode.alwaysOriginal)
            viDropDownMenu.isHidden = true
            tblTeachersName.isHidden = true
        }else if typeSelecting == 1{
            // is teacher
            btnTeacherHandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
            btnTeacherHandler.setLeftImage(imageName: UIImage(named: "radioSelected")!, renderMode: UIImage.RenderingMode.alwaysOriginal)
            btnManagementHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
            btnManagementHandler.setLeftImage(imageName: UIImage(named: "radioDeselected")!, renderMode: UIImage.RenderingMode.alwaysOriginal)
            viDropDownMenu.isHidden = false
            tblTeachersName.isHidden = true
        }
    }
    
    @IBAction func btnAttachAction(_ sender: Any) {
        self.btnAddHandler.isEnabled = false
        let chooseSelectedSource = UIAlertController(title: "Choose", message: "Do you want open library or camera??", preferredStyle: .alert)
        
        let chooseCamera = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            self.selectImageFrom(.camera)
        }
        let chooseLibrary = UIAlertAction(title: "Photo Library", style: .default) { (UIAlertAction) in
            self.selectImageFrom(.photoLibrary)
        }
        chooseSelectedSource.addAction(chooseCamera)
        chooseSelectedSource.addAction(chooseLibrary)
        self.present(chooseSelectedSource, animated: true, completion: nil)
    }
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }

    
}



extension ParentRequestAddVC: UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        //TakePhoto.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
        self.attachmentFilesResources.append(selectedImage)
        self.tblAttachmentFiles.reloadData()
    }
}
