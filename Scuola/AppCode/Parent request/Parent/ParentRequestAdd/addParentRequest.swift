//
//  addParentRequest.swift
//  School
//
//  Created by ISCA-IOS on 7/21/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func addParentRequest(student_id: String, teacher_user_id: String, request_from: String, request: String, class_id: String, school_id: String, complition: @escaping(_ status: Bool, _ msg: String, _ idFeature: Int) -> Void){
        let url = URLs.parentRequestAdd
        let parameters = [
            "student_id": student_id,
            "teacher_user_id": teacher_user_id,
            "request_from": request_from,
            "request": request,
            "class_id": class_id,
            "school_id": school_id
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                complition(false, value.localizedDescription, 0)
            case .success(let value):
                print(value)
                let json = JSON(value)
                complition(json["status"].bool ?? false, json["msg"].string ?? "", json["id"].int ?? 0)
            }
        }
    }
}
