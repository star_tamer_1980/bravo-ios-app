//
//  ParentRequestDetailsVC.swift
//  School
//
//  Created by ISCA-IOS on 7/15/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Kingfisher

class ParentRequestDetailsVC: parentAttachAdapter {
    @IBOutlet weak var viTitle: UIView!
    @IBOutlet weak var imgStudent: ImageOptions!
    @IBOutlet weak var lblStudentName: WhiteLabel!
    @IBOutlet weak var lblClassName: WhiteLabel!
    @IBOutlet weak var tvRequest: GrayTextView!
    @IBOutlet weak var imgTeacher: ImageOptions!
    @IBOutlet weak var lblTeacherName: Blue12Label!
    @IBOutlet weak var lblReplyDate: Blue9Label!
    @IBOutlet weak var tvReply: GrayTextView!
    @IBOutlet weak var viRequstReply: UIView!
    
    var request = parentRequestModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        viTitle.backgroundColor = GlobalColor.APP.blue
        let url = URL(string: request.student_photo)
        imgStudent.kf.setImage(with: url)
        lblStudentName.text = request.name
        lblClassName.text = request.class_name
        tvRequest.text = request.request
        let urlTeacher = URL(string: request.student_photo)
        imgTeacher.kf.setImage(with: urlTeacher)
        lblTeacherName.text = request.teacher_name
        lblReplyDate.text = request.reply_date
        tvReply.text = request.reply
        
        let tabGesture = UITapGestureRecognizer(target: self, action: #selector(backToPrevious(_:)))
        viTitle.addGestureRecognizer(tabGesture)
        if request.req_status == "1"{
            viRequstReply.isHidden = false
        }
        print("count file attach: \(request.attachment.count)")
        getData(requestAttach: request.attachment, replyAttach: request.attachment)
    }
    
    @objc func backToPrevious(_ tabgesture: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    


}
