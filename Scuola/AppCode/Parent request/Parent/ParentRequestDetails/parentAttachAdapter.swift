//
//  parentAttachAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/22/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class parentAttachAdapter: UIViewController {
    
    @IBOutlet weak var viContainScrollConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var tblReplyAttachment: UITableView!
    @IBOutlet weak var tblReplyConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tblRequestAttachment: UITableView!
    @IBOutlet weak var tblRequestConstraint: NSLayoutConstraint!
    
    
    let cellIdentifier = "attachNib"
    var filesRequest = [attachmentModel]()
    var filesReply = [attachmentModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblRequestAttachment.tableFooterView = UIView()
        self.tblRequestAttachment.separatorInset = .zero
        self.tblRequestAttachment.contentInset = .zero
        self.tblRequestAttachment.separatorStyle = .none
        self.tblRequestAttachment.register(UINib.init(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        self.tblRequestAttachment.dataSource = self
        self.tblRequestAttachment.delegate = self
        
        self.tblReplyAttachment.tableFooterView = UIView()
        self.tblReplyAttachment.contentInset = .zero
        self.tblReplyAttachment.separatorInset = .zero
        self.tblReplyAttachment.separatorStyle = .none
        self.tblReplyAttachment.register(UINib.init(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        self.tblReplyAttachment.dataSource = self
        self.tblReplyAttachment.delegate = self
    }
    
    func getData(requestAttach: [attachmentModel], replyAttach: [attachmentModel]){
        filesRequest = requestAttach
        filesReply = replyAttach
        tblRequestConstraint.constant = CGFloat(filesRequest.count * 64)
        tblReplyConstraint.constant = CGFloat(filesReply.count * 64)
        viContainScrollConstrain.constant += CGFloat(filesRequest.count * 64) + CGFloat(filesReply.count * 64)
        tblRequestAttachment.reloadData()
        tblReplyAttachment.reloadData()
    }
    


}

extension parentAttachAdapter: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == tblRequestAttachment{
            count = filesRequest.count
        }else{
            count = filesReply.count
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! attachNib
        if tableView == tblRequestAttachment{
            cell.celConfigerURL(nameFile: filesRequest[indexPath.row].file_name)
        }else{
            cell.celConfigerURL(nameFile: filesReply[indexPath.row].file_name)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Other", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoom
        if tableView == tblRequestAttachment{
            controller.urlImage = filesRequest[indexPath.row].file_path
        }else{
            controller.urlImage = filesReply[indexPath.row].file_path
        }
        self.present(controller, animated: true)
    }
    
}

extension parentAttachAdapter: UITableViewDelegate{
    
}
