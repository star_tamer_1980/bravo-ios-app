//
//  parentRequstOpened.swift
//  School
//
//  Created by ISCA-IOS on 7/11/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
class parentRequestModel: NSObject{
    var id: Int = 0
    var request: String = ""
    var req_status: String = ""
    var reply: String = ""
    var request_from: String = ""
    var teacher_user_id: String = ""
    var school_id: String = ""
    var create_date: String = ""
    var reply_date: String = ""
    var student_id: String = ""
    var class_id: String = ""
    var name: String = ""
    var class_name: String = ""
    var student_photo: String = ""
    var teacher_name: String = ""
    var type_member: String = ""
    var attachment = [attachmentModel]()
}
