//
//  ParentRequestListNetwork.swift
//  School
//
//  Created by ISCA-IOS on 7/14/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func ParentRequestList(studentId: String, isClosed: String, itemCount: String, limit: String, complition: @escaping(_ status: Bool, _ msg: String, _ requests: [parentRequestModel]?, _ isLoad: Bool) -> Void){
        let url = URLs.parentRequestList
        let parameters = [
            "student_id"    : studentId,
            "status"        : isClosed,
            "item_count"    : itemCount,
            "limit"         : limit
        ]
        
        
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                complition(false, value.localizedDescription, nil, false)
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].bool! == false{
                    print("test not found data")
                    complition(false, json["msg"].string!, nil, false)
                }else{
                    print("test found data")
                    guard let resultArr = json["Result"].array else{
                        complition(false, json["msg"].string!, nil, false)
                        return
                    }
                    if resultArr.count != 0 && itemCount  == "0"{
                        DatabaseManager.getInstance().remove_Parent_request_data("1", isClosed, studentId)
                    }
                    var requestsList = [parentRequestModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let request = parentRequestModel()
                        request.id = Int(data["id"]?.string ?? "0")!
                        request.request = data["request"]?.string ?? ""
                        request.req_status = data["req_status"]?.string ?? ""
                        request.reply = data["reply"]?.string ?? ""
                        request.request_from = data["request_from"]?.string ?? ""
                        request.teacher_user_id = data["teacher_user_id"]?.string ?? ""
                        request.school_id = data["school_id"]?.string ?? ""
                        request.create_date = data["create_date"]?.string ?? ""
                        request.reply_date = data["reply_date"]?.string ?? ""
                        request.student_id = data["student_id"]?.string ?? ""
                        request.class_id = data["class_id"]?.string ?? ""
                        request.name = data["name"]?.string ?? ""
                        request.class_name = data["class_name"]?.string ?? ""
                        request.student_photo = data["student_photo"]?.string ?? ""
                        request.teacher_name = data["teacher_name"]?.string ?? ""
                        request.type_member = "1"
                        
                        var filesArray = [attachmentModel]()
                        if data["attach"] != nil{
                            if let attach_files = data["attach"]!.array{
                                for allFiles in attach_files{
                                    guard let file = allFiles.dictionary else {return}
                                    let fileModule = attachmentModel()
                                    fileModule.id = file["id"]?.string ?? "0"
                                    fileModule.feature_id = file["feature_id"]?.string ?? ""
                                    fileModule.feature_type_id = file["feature_type_id"]?.string ?? ""
                                    fileModule.file_name = file["file_name"]?.string ?? ""
                                    fileModule.school_id = file["school_id"]?.string ?? ""
                                    fileModule.added_on = file["added_on"]?.string ?? ""
                                    fileModule.parent_request = file["parent_request"]?.string ?? ""
                                    fileModule.extension_file = file["extension"]?.string ?? ""
                                    fileModule.file_path = file["file_path"]?.string ?? ""
                                    DatabaseManager.getInstance().save_Parent_Attachment_Info(fileModule)
                                    filesArray.append(fileModule)
                                }
                                request.attachment = filesArray
                            }
                        }
                        DatabaseManager.getInstance().save_Parent_parent_request_Info(request)
                        requestsList.append(request)
                        
                    }
                    
                    if requestsList.count == 0{
                        complition(true, "found data", requestsList, false)
                    }else{
                        complition(true, "found data", requestsList, true)
                    }
                    
                }
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
}

