//
//  parentRequestClosedNib.swift
//  School
//
//  Created by ISCA-IOS on 7/23/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class parentRequestClosedNib: UITableViewCell {

    @IBOutlet weak var imgStudent: ImageOptions!
    @IBOutlet weak var lblStudentName: Blue12Label!
    @IBOutlet weak var lblStudentClass: Blue9Label!
    @IBOutlet weak var lblTeacherName: Blue9Label!
    @IBOutlet weak var tvDescription: GrayTextView!
    @IBOutlet weak var lblDate: Blue9Label!
    @IBOutlet weak var imgRequestAttachment: UIImageView!
    
    @IBOutlet weak var imgTeacher: ImageOptions!
    @IBOutlet weak var lblReplyTeacherName: Blue12Label!
    @IBOutlet weak var lblReplyDate: Blue9Label!
    @IBOutlet weak var imgReplyAttachment: UIImageView!
    @IBOutlet weak var tvReplydescription: GrayTextView!
    
    @IBOutlet weak var viReply: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func cellConfiguration(requestData: parentRequestModel){
        let url = URL(string: requestData.student_photo)
        imgStudent.kf.setImage(with: url)
        lblStudentName.text = requestData.name
        lblStudentClass.text = requestData.class_name
        lblTeacherName.text = "to: \(requestData.teacher_name)"
        tvDescription.text = requestData.request
        lblDate.text = requestData.create_date
        if requestData.attachment.count == 0{
            imgRequestAttachment.isHidden = true
        }else{
            imgRequestAttachment.isHidden = false
        }

        imgTeacher.kf.setImage(with: url)
        lblReplyTeacherName.text = requestData.teacher_name
        lblReplyDate.text = requestData.reply_date
        tvReplydescription.text = requestData.reply
        
    }
    
}
