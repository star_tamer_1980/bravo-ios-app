//
//  parentRequestNib.swift
//  School
//
//  Created by ISCA-IOS on 7/11/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Kingfisher

class parentRequestNib: UITableViewCell {

    @IBOutlet weak var imgStudent: ImageOptions!
    @IBOutlet weak var lblStudentName: BigLabel!
    @IBOutlet weak var lblStudentClass: SmallLabel!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var tvDescription: GrayTextView!
    @IBOutlet weak var lblDate: SmallLabel!
    @IBOutlet weak var btnAttachmentHandle: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func cellConfiguration(studentImage: String, studentName: String, studentClass: String, teacherName: String, description: String, dateAdd: String, haveAttachment: Bool){
        
        let url = URL(string: studentImage)
        imgStudent.kf.setImage(with: url)
        lblStudentName.text = studentName
        lblStudentClass.text = studentClass
        lblTeacherName.text = teacherName
        tvDescription.text = description
        lblDate.text = dateAdd
        if haveAttachment != true{
            btnAttachmentHandle.isHidden = true
        }else{
            btnAttachmentHandle.isHidden = false
        }
    }
    
}
