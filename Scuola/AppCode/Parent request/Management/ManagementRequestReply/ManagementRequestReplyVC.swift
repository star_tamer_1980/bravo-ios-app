//
//  ManagementRequestReplyVC.swift
//  School
//
//  Created by ISCA-IOS on 7/25/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import TTGSnackbar

class ManagementRequestReplyVC: UIViewController {
    
    @IBOutlet weak var viTitle: UIView!
    @IBOutlet weak var imgStudent: ImageOptions!
    @IBOutlet weak var lblStudentName: WhiteLabel!
    @IBOutlet weak var lblClassName: WhiteLabel!
    @IBOutlet weak var tvRequest: GrayTextView!
    @IBOutlet weak var tvReply: GrayTextView!
    @IBOutlet weak var btnAddReplyHandler: Blue!
    
    var request = parentRequestModel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viTitle.backgroundColor = GlobalColor.APP.blue
        let url = URL(string: request.student_photo)
        imgStudent.kf.setImage(with: url)
        lblStudentName.text = request.name
        lblClassName.text = request.class_name
        tvRequest.text = request.request
        
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(backToPrevious))
        viTitle.addGestureRecognizer(tapBack)
    }
    
    @objc func backToPrevious(_ tabgesture: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        IQKeyboardManager.shared.enable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(TeacherRequestReply.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TeacherRequestReply.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 140.0
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 140.0
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func btnAddReply(_ sender: Any) {
        
        guard let replyContent = tvReply.text, tvReply.text != "" else {
            let storyboard = UIStoryboard(name: "Messages", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "mes1") as! mes1)
            controller.titleSt = Alerts.textbox.phoneNumber.empty.head
            controller.descriptionSt = Alerts.textbox.mandatary.description
            self.present(controller, animated: true)
            return
        }
        self.btnAddReplyHandler.isEnabled = false
        API.managementReply(String(request.id), replyContent) { (result: Bool, msg: String?) in
            
            if result == true{
                KeyValueSettings.setIsSaveData("1")
            }
            self.btnAddReplyHandler.isEnabled = true
            self.dismiss(animated: true, completion: nil)
            
            let msg = TTGSnackbar(message: msg!, duration: .long)
            msg.show()
        }
    }
    
}
