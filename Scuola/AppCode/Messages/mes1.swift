//
//  mes1.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class mes1: UIViewController {

    var titleSt: String! = ""
    var descriptionSt: String! = ""
    @IBOutlet weak var tvTitle: RedTextView!
    @IBOutlet weak var tvContent: GrayTextView!
    @IBOutlet weak var btnOk: Green!
    override func viewDidLoad() {
        super.viewDidLoad()

        tvTitle.text = titleSt
        tvContent.text = descriptionSt
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
