//
//  menuListManagementVC.swift
//  School
//
//  Created by ISCA-IOS on 7/9/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class menuListManagementVC: menuManagementAdapter {

    @IBOutlet weak var imgSchool: ImageOptions!
    @IBOutlet weak var lblSchoolName: BigLabel!
    @IBOutlet weak var lblUsername: SmallLabel!
    @IBOutlet weak var lblYear: BlueSemiBold22Label!
    @IBOutlet weak var lblMonth: BlueSemiBold22Label!
    @IBOutlet weak var lblDays: BlueSemiBold22Label!
    
    var studentId: String = KeyValueShare.getStudentId()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        let url = URL(string: DatabaseManager.getInstance().getSchoolInfo(schooldId: KeyValueShare.getSchoolId()).logo)
        imgSchool.kf.setImage(with: url)
        lblSchoolName.text = DatabaseManager.getInstance().getSchoolInfo(schooldId: KeyValueShare.getSchoolId()).name
        lblUsername.text = "Management"
        
        self.getData()
    }

}
