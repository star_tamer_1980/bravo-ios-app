//
//  HomeIconsManagementVC.swift
//  School
//
//  Created by ISCA-IOS on 7/9/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class HomeIconsManagementVC: HomeIconsManagementAdapter {

    @IBOutlet weak var viTopBar: UIView!
    @IBOutlet weak var imgStudentInfo: ImageOptions!
    @IBOutlet weak var btnDrawable: UIButton!
    var schoolInfo = SchoolsModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        viTopBar.backgroundColor = GlobalColor.APP.blue
        schoolInfo = DatabaseManager.getInstance().getSchoolInfo(schooldId: KeyValueShare.getManagementSchoolId())
        let url = URL(string: schoolInfo.logo)
        imgStudentInfo.kf.setImage(with: url)
        btnDrawable.setTitle(schoolInfo.name, for: .normal)
        if let vc = revealViewController(){
            vc.rearViewRevealWidth = 260
            self.view.addGestureRecognizer(vc.panGestureRecognizer())
        }
    }
    
    @IBAction func btnDrawableAction(_ sender: Any) {
        openDrawable()
    }
    
    
    private func openDrawable(){
        if let vc = revealViewController(){
            vc.rearViewRevealWidth = 260
            vc.revealToggle(animated: true)
        }
    }

}
