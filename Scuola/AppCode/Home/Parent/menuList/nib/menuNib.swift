//
//  menuNib.swift
//  School
//
//  Created by ISCA-IOS on 7/8/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Kingfisher

class menuNib: UITableViewCell {
    @IBOutlet weak var lblTitle: SmallLabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(title: String, logo: String){
        lblTitle.text = title
        lblTitle.textColor = GlobalColor.APP.gray
        imgLogo.image = UIImage(named: logo)
    }
    
}
