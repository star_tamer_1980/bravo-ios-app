//
//  menuListVC.swift
//  School
//
//  Created by ISCA-IOS on 7/7/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class menuListVC: menuAdapter {
    @IBOutlet weak var imgSchool: ImageOptions!
    @IBOutlet weak var lblSchoolName: BigLabel!
    @IBOutlet weak var lblClassName: SmallLabel!
    @IBOutlet weak var lblUsername: SmallLabel!
    @IBOutlet weak var lblYear: BlueSemiBold22Label!
    @IBOutlet weak var lblMonth: BlueSemiBold22Label!
    @IBOutlet weak var lblDays: BlueSemiBold22Label!
    @IBOutlet weak var btnLogout: UIButton!
    
    var studentId: String = KeyValueShare.getStudentId()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: false)
        let studentInfo = DatabaseManager.getInstance().getStudentInfo(idStudent: studentId)
        let url = URL(string: studentInfo.school_logo)
        imgSchool.kf.setImage(with: url)
        lblSchoolName.text = studentInfo.school_name
        lblClassName.text = studentInfo.class_name
        lblUsername.text = studentInfo.first_name + " " + studentInfo.last_name
        let dateData = DateUtil.getDifferentBetween2Dates(startDate: studentInfo.birthdate)
        lblYear.text = String(dateData.year ?? 00)
        lblMonth.text = String(dateData.month ?? 00)
        lblDays.text = String(dateData.day ?? 00)
        self.getData()
    }
    @IBAction func btnLogoutAction(_ sender: Any) {
        
        
    }
    

}
