//
//  menuTable.swift
//  School
//
//  Created by ISCA-IOS on 7/8/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class menuAdapter: UIViewController{
    
    @IBOutlet weak var tblMenu: UITableView!
    let celTable = "cellTable"
    let celHeight: CGFloat = 45.0
    var menuList = [menuModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblMenu.tableFooterView = UIView()
        self.tblMenu.separatorInset = .zero
        self.tblMenu.contentInset = .zero
        self.tblMenu.separatorStyle = .none
        self.tblMenu.register(UINib.init(nibName: "menuNib", bundle: nil), forCellReuseIdentifier: celTable)
        self.tblMenu.dataSource = self
        self.tblMenu.delegate = self
    }
    
    
    func getData(){
        handleRefresh()
    }
    @objc func handleRefresh(){
        self.tblMenu.reloadData()
        print("now")
        print(menuList.count)
        
        let menuFeild2 = menuModel()
        menuFeild2.title = "Settings"
        menuFeild2.image = "menuSettingIcon"
        self.menuList.append(menuFeild2)
        
        
        let menuFeild3 = menuModel()
        menuFeild3.title = "Custom support"
        menuFeild3.image = "menuCustomIcon"
        self.menuList.append(menuFeild3)
        
        
        let menuFeild4 = menuModel()
        menuFeild4.title = "Switch student"
        menuFeild4.image = "menuSwitchStudent"
        self.menuList.append(menuFeild4)
        
        
        let menuFeild5 = menuModel()
        menuFeild5.title = "Switch Account"
        menuFeild5.image = "menuSwitchAccount"
        self.menuList.append(menuFeild5)
        
        
        
        let menuFeild6 = menuModel()
        menuFeild6.title = "Logout"
        menuFeild6.image = "menuLogout"
        self.menuList.append(menuFeild6)
        
        self.tblMenu.reloadData()
    }
}
extension menuAdapter: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: celTable, for: indexPath) as! menuNib
        cell.configureCell(title: menuList[indexPath.row].title, logo: menuList[indexPath.row].image)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = self.revealViewController(){
            
            vc.revealToggle(animated: true)
        }
        if indexPath.row == 0{
            
        }
        if indexPath.row == 1{
            
        }
        if indexPath.row == 2{
            self.dismiss(animated: true, completion: nil)
        }
        if indexPath.row == 3{
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let myController = (storyboard.instantiateViewController(withIdentifier: "memberListTypeVC")) as! memberListTypeVC
            self.present(myController, animated: true, completion: nil)
        }
        if indexPath.row == 4{
            let alert = UIAlertController(title: Alerts.account.logout.head, message: Alerts.account.logout.description, preferredStyle: .alert)
            let alertYesAction = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
                DatabaseManager.getInstance().deleteMemberInfoTable()
                DatabaseManager.getInstance().deleteStudentTable()
                DatabaseManager.getInstance().deleteSchoolsTable()
                DatabaseManager.getInstance().deleteHomeIconTable()
                DatabaseManager.getInstance().delete_Parent_request_Table()
                DatabaseManager.getInstance().delete_notes_Table()
                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                let myController = (storyboard.instantiateViewController(withIdentifier: "LoginVC")) as! LoginVC
                self.present(myController, animated: true, completion: nil)
            }
            let alertNoAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(alertYesAction)
            alert.addAction(alertNoAction)
            self.present(alert, animated: true)
        }
    }
}
extension menuAdapter: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return celHeight
    }
}
