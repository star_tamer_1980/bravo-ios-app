//
//  HomeIconsVC.swift
//  School
//
//  Created by ISCA-IOS on 7/7/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Kingfisher

class HomeIconsVC: HomeIconAdapter {
    @IBOutlet weak var viTopBar: UIView!
    @IBOutlet weak var imgStudentInfo: ImageOptions!
    @IBOutlet weak var btnDrawable: UIButton!
    var studentInfo = StudentModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        viTopBar.backgroundColor = GlobalColor.APP.blue
        studentInfo = DatabaseManager.getInstance().getStudentInfo(idStudent: KeyValueShare.getStudentId())
        let url = URL(string: studentInfo.student_photo)
        imgStudentInfo.kf.setImage(with: url)
        btnDrawable.setTitle(studentInfo.school_name, for: .normal)
        if let vc = revealViewController(){
            vc.rearViewRevealWidth = 260
            self.view.addGestureRecognizer(vc.panGestureRecognizer())
        }
    }
    
    @IBAction func btnDrawableAction(_ sender: Any) {
        openDrawable()
    }
    
    
    private func openDrawable(){
        if let vc = revealViewController(){
            vc.rearViewRevealWidth = 260
            vc.revealToggle(animated: true)
        }
    }

}
