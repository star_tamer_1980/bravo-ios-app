//
//  HomeIconNib.swift
//  School
//
//  Created by ISCA-IOS on 7/8/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class HomeIconNib: UICollectionViewCell {

    @IBOutlet weak var lblTitle: BlueLabel!
    @IBOutlet weak var imgIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellConfigure(title: String, urlImage: String){
        lblTitle.font = UIFont(name: "Poppins-Regular", size: 14)
        lblTitle.textColor = GlobalColor.APP.blue
        lblTitle.textAlignment = .center
        lblTitle.text = title
        let url = URL(string: urlImage)
        imgIcon.kf.setImage(with: url)
    }

}
