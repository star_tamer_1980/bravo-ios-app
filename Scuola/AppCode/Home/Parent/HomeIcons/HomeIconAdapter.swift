//
//  HomeIconAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/8/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class HomeIconAdapter: UIViewController {

    fileprivate let cellIdentifier = "HomeIconNib"
    @IBOutlet weak var cvHomeIcons: UICollectionView!
    var homeIcons = [HomeIconModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvHomeIcons.backgroundColor = .clear
        cvHomeIcons.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        cvHomeIcons.dataSource = self as UICollectionViewDataSource
        cvHomeIcons.delegate = self as UICollectionViewDelegate
        homeIcons = DatabaseManager.getInstance().getHomeIconList("1")
        self.cvHomeIcons.reloadData()
    }
    
    
}

extension HomeIconAdapter: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeIcons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? HomeIconNib else{ return UICollectionViewCell()}
        cell.cellConfigure(title: homeIcons[indexPath.row].feature, urlImage: homeIcons[indexPath.row].icon_path)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if homeIcons[indexPath.row].id == "1"{
            let storyboard = UIStoryboard(name: "ParentRequest", bundle: nil)
            let myController = storyboard.instantiateViewController(withIdentifier: "ParentRequestListVC")
            self.present(myController, animated: true)
        }else if homeIcons[indexPath.row].id == "2"{
            let storyboard = UIStoryboard(name: "parent_notes", bundle: nil)
            let myController = storyboard.instantiateViewController(withIdentifier: "NotesListVC")
            self.present(myController, animated: true)
        }else if homeIcons[indexPath.row].id == "4"{
            let storyboard = UIStoryboard(name: "Daily_plan", bundle: nil)
            let myController = storyboard.instantiateViewController(withIdentifier: "DailyPlanVC")
            self.present(myController, animated: true)
        }
    }
}
extension HomeIconAdapter: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.bounds.width / 2) - 20, height: 160)
    }
}
