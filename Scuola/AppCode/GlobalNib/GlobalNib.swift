//
//  GlobalNib.swift
//  School
//
//  Created by ISCA-IOS on 8/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class GlobalNib: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func cellConfiguration(title: String){
        self.lblTitle.text = title
    }
}
