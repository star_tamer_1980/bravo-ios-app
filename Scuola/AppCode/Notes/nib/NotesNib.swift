//
//  NotesNib.swift
//  School
//
//  Created by ISCA-IOS on 7/28/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class NotesNib: UITableViewCell {

    @IBOutlet weak var viBackgroundTitle: UIView!
    @IBOutlet weak var lblFrom: Blue12LeftLabel!
    @IBOutlet weak var lblTo: Blue12LeftLabel!
    @IBOutlet weak var lblTitle: Blue12Label!
    @IBOutlet weak var tvContent: GrayTextView!
    @IBOutlet weak var lblAdded_on: Blue9LeftLabel!
    @IBOutlet weak var btnAttachHandler: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viBackgroundTitle.backgroundColor = GlobalColor.APP.blue10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
 
    func cellConfigure(note: NotesModel){
        lblFrom.text = note.added_by
        lblTo.text = note.note_to
        lblTitle.text = note.category_name
        tvContent.text = note.note
        lblAdded_on.text = note.added_on
        btnAttachHandler.setTitle(String(note.attach.count), for: .normal)
    }
}
