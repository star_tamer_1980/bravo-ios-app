//
//  NoteAttachmentAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/29/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class NoteAttachmentAdapter: UIViewController{
    @IBOutlet weak var viContainScrollConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var tblAttachment: UITableView!
    @IBOutlet weak var tblAttachmentConstratin: NSLayoutConstraint!
    
    
    let cellIdentifier = "attachNib"
    var filesNote = [attachmentModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblAttachment.tableFooterView = UIView()
        self.tblAttachment.separatorInset = .zero
        self.tblAttachment.contentInset = .zero
        self.tblAttachment.separatorStyle = .none
        self.tblAttachment.register(UINib.init(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        self.tblAttachment.dataSource = self
        self.tblAttachment.delegate = self
        
    }
    
    func getData(noteAttach: [attachmentModel]){
        filesNote = noteAttach
        tblAttachmentConstratin.constant = CGFloat(filesNote.count * 64)
        viContainScrollConstrain.constant += CGFloat(filesNote.count * 64)
        tblAttachment.reloadData()
    }
    
    
    
}

extension NoteAttachmentAdapter: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == tblAttachment{
            count = filesNote.count
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! attachNib
        if tableView == tblAttachment{
            cell.celConfigerURL(nameFile: filesNote[indexPath.row].file_name)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Other", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoom
        if tableView == tblAttachment{
            controller.urlImage = filesNote[indexPath.row].file_path
        }
        self.present(controller, animated: true)
    }
    
}

extension NoteAttachmentAdapter: UITableViewDelegate{
    
}
