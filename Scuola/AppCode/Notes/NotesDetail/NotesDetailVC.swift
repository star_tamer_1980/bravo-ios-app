//
//  NotesDetailVC.swift
//  School
//
//  Created by ISCA-IOS on 7/29/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class NotesDetailVC: NoteAttachmentAdapter {
    @IBOutlet weak var viTitle: UIView!
    @IBOutlet weak var lblFrom: Blue12LeftLabel!
    @IBOutlet weak var lblTo: Blue12LeftLabel!
    @IBOutlet weak var lblTitle: BlueSemiBold22Label!
    @IBOutlet weak var tvNote: GrayTextView!
    
    var Note = NotesModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viTitle.backgroundColor = GlobalColor.APP.blue
        
        lblFrom.text = Note.added_by
        lblTo.text = Note.note_to
        lblTitle.text = Note.category_name
        tvNote.text = Note.note
        let tabGesture = UITapGestureRecognizer(target: self, action: #selector(backToPrevious(_:)))
        viTitle.addGestureRecognizer(tabGesture)
        getData(noteAttach: Note.attach)
    }
    

    @objc func backToPrevious(_ tabgesture: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    

}
