//
//  gradeModel.swift
//  School
//
//  Created by ISCA-IOS on 8/6/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class gradeModel{
    var id: String = ""
    var grade: String = ""
    var stage_id: String = ""
    var classes = [classModel]()
}
