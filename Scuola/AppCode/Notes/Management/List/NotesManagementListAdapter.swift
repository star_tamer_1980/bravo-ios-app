//
//  NotesManagementListAdapter.swift
//  School
//
//  Created by ISCA-IOS on 8/5/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class NotesManagementListAdapter: UIViewController {
    
    var celHeight: CGFloat = 150.0
    @IBOutlet weak var tblNotes: UITableView!
    var notesList = [NotesModel]()
    let cellNote = "NotesNib"
    var limit: String = "5"
    var isLoading: Bool = false
    var isLoadMore: Bool = true
    var teacherId = DatabaseManager.getInstance().getManagementInfo().id
    var schoolId = KeyValueShare.getManagementSchoolId()
    lazy var refresh: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(RefreshNotes), for: .valueChanged)
        return refresher
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
    }
    
    func getData(){
        print("get data")
        
        handleRefresh()
    }
    
    @objc func handleRefresh(){
        RefreshNotes()
    }
    
    
    @objc func RefreshNotes(){
        self.refresh.endRefreshing()
        self.tblNotes.reloadData()
        guard !isLoading else {
            return
        }
        self.isLoading = true
        API.managementNotesList(school_id: schoolId, item_count: "0", limit: self.limit) { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoading = false
            if result == true{
                if let notes = notes{
                    self.notesList = notes
                    self.tblNotes.reloadData()
                    self.isLoadMore = isLoadMore!
                    self.limit = "10"
                }
            }else{
                self.isLoadMore = false
            }
        }
        
    }
    
    
    
    
    @objc func handleRfresh(){
        self.refresh.endRefreshing()
        self.tblNotes.reloadData()
        guard !isLoading else {
            return
        }
        self.isLoading = true
        API.managementNotesList(school_id: schoolId, item_count: String(notesList.count), limit: self.limit) { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoading = false
            if result == true{
                if let notes = notes{
                    if self.notesList.count == 0{
                        self.notesList = notes
                    }else{
                        self.notesList.append(contentsOf: notes)
                    }
                    self.tblNotes.reloadData()
                    self.isLoadMore = isLoadMore!
                    self.limit = "10"
                }
            }else{
                self.isLoadMore = false
            }
        }
    }
    
    
    
    func loadMore(){
        guard !isLoading else {
            return
        }
        guard isLoadMore else {
            return
        }
        //isLoadingClosed = true
        handleRfresh()
    }
    
}

extension NotesManagementVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellNote, for: indexPath) as! NotesNib
        cell.cellConfigure(note: notesList[indexPath.row])
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "parent_notes", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "NotesDetailVC") as! NotesDetailVC
        myController.Note = notesList[indexPath.row]
        
        
        self.present(myController, animated: true)
        
        
    }
    
}

extension NotesManagementVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return celHeight
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        return view
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var count: Int?
        count = notesList.count
        if indexPath.row == count!-1{
            self.loadMore()
        }
        
    }
    
}
