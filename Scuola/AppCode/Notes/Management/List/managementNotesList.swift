//
//  managementNotesList.swift
//  School
//
//  Created by ISCA-IOS on 8/5/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func managementNotesList(school_id: String, item_count: String, limit: String, _ complition: @escaping(_ status: Bool?, _ msg: String?, _ notesList: [NotesModel]?, _ isLoadMore: Bool?) -> Void){
        let url = URLs.notesManagementList
        let parameter = [
            "school_id": school_id,
            "item_count": item_count,
            "limit": limit
        ]
        print("school_id \(school_id)")
        print("item_count \(item_count)")
        print("limit \(limit)")
        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let error):
                complition(false, error.localizedDescription, nil, false)
            case .success(let value):
                let json = JSON(value)
                print("notes lis \(json)")
                if json["status"].bool == false{
                    complition(false, json["msg"].string, nil, false)
                }else{
                    guard let resultArr = json["notes"].array else{
                        complition(false, json["msg"].string, nil, false)
                        return
                    }
                    if resultArr.count != 0 && item_count  == "0"{
                        let resultCount = DatabaseManager.getInstance().remove_notes_management_data( school_id, "2")
                        print("resultCount is: \(resultCount)")
                    }
                    var notesList = [NotesModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let note = NotesModel()
                        note.id = Int(data["id"]?.string ?? "0")!
                        note.category_id = data["category_id"]?.string ?? ""
                        note.category_name = data["category_name"]?.string ?? ""
                        note.note = data["note"]?.string ?? ""
                        note.note_type = data["note_type"]?.string ?? ""
                        note.student_id = data["student_id"]?.string ?? ""
                        note.stage_id = data["stage_id"]?.string ?? ""
                        note.grade_id = data["grade_id"]?.string ?? ""
                        note.class_id = data["class_id"]?.string ?? ""
                        note.school_id = data["school_id"]?.string ?? ""
                        note.added_by = data["added_by"]?.string ?? ""
                        note.added_on = data["added_on"]?.string ?? ""
                        note.note_to = data["note_to"]?.string ?? ""
                        
                        var allAttachments = [attachmentModel]()
                        if data["attach"] != nil{
                            if let attachments = data["attach"]?.array{
                                for allfiles in attachments{
                                    guard let file = allfiles.dictionary else {return}
                                    let fileModule = attachmentModel()
                                    fileModule.id = file["id"]?.string ?? "0"
                                    fileModule.feature_id = file["feature_id"]?.string ?? ""
                                    fileModule.feature_type_id = file["feature_type_id"]?.string ?? ""
                                    fileModule.file_name = file["file_name"]?.string ?? ""
                                    fileModule.school_id = file["school_id"]?.string ?? ""
                                    fileModule.added_on = file["added_on"]?.string ?? ""
                                    fileModule.parent_request = file["parent_request"]?.string ?? ""
                                    fileModule.extension_file = file["extension"]?.string ?? ""
                                    fileModule.file_path = file["file_path"]?.string ?? ""
                                    DatabaseManager.getInstance().save_Parent_Attachment_Info(fileModule)
                                    allAttachments.append(fileModule)
                                }
                                note.attach = allAttachments
                                
                            }
                        }
                        
                        DatabaseManager.getInstance().save_note_managment(note)
                        notesList.append(note)
                        
                    }
                    complition(true, json["msg"].string, notesList, true)
                }
            }
        }
    }
}


