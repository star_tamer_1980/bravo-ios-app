//
//  notesAddManagementNetwork.swift
//  School
//
//  Created by ISCA-IOS on 8/6/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func addNoteManagement(category_id: String, note: String, school_id: String, stage_id: String, grade_id: String, class_id: String, student_id: String, added_by: String, complition: @escaping(_ status: Bool, _ msg: String) -> Void){
        let url = URLs.addNoteManagement
        let parameters = [
            "category_id": category_id,
            "note": note,
            "school_id": school_id,
            "stage_id": stage_id,
            "grade_id": grade_id,
            "class_id": class_id,
            "student_id": student_id,
            "added_by": added_by
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                print("failed \(value.localizedDescription)")
                complition(false, value.localizedDescription)
            case .success(let value):
                print("success \(value)")
                let json = JSON(value)
                complition(json["status"].bool!, json["msg"].string!)
            }
            
        }
    }
}
