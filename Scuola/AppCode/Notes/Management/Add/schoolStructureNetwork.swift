//
//  schoolStructureNetwork.swift
//  School
//
//  Created by ISCA-IOS on 8/6/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func school_structure(school_id: String, _ complition: @escaping(_ result: Bool?, _ msg: String?, _ stageList: [stageModel]?) -> Void){
        let url = URLs.schoolStructure
        let parameters = [
            "school_id": school_id
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                complition(false, value.localizedDescription, nil)
            case .success(let value):
                let json = JSON(value)
                print("stages is \(json)")
                if json["status"].bool! == false{
                    complition(false, json["msg"].string!, nil)
                    return
                }else{
                    guard let resultArr = json["stages"].array else{
                        complition(false, json["msg"].string!, nil)
                        return
                    }
                    var stagesList = [stageModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let stage = stageModel()
                        stage.id = data["id"]?.string ?? ""
                        stage.stage = data["stage"]?.string ?? ""
                        print("")
                        print("")
                        print("")
                        print(" stage \(stage.id) : \(stage.stage)")

                        guard let gradesArr = data["grades"]!.array else{
                                complition(false, json["msg"].string!, nil)
                                return
                            }
                            var gradesList = [gradeModel]()
                            for dataGrae in gradesArr{
                                guard let datagrade = dataGrae.dictionary else {return}
                                let grade = gradeModel()
                                grade.id = datagrade["id"]?.string ?? ""
                                grade.grade = datagrade["grade"]?.string ?? ""
                                grade.stage_id = datagrade["stage_id"]?.string ?? ""
                                print("     grade \(grade.id) : \(grade.grade)")

                                    guard let classesArr = datagrade["classes"]!.array else{
                                        complition(false, json["msg"].string!, nil)
                                        return
                                    }
                                    var classesList = [classModel]()
                                    for dataClasses in classesArr{
                                        guard let dataclass = dataClasses.dictionary else {return}
                                        let classField = classModel()
                                        classField.id = dataclass["id"]?.string ?? ""
                                        classField.name = dataclass["name"]?.string ?? ""
                                        classField.grade_id = dataclass["grade_id"]?.string ?? ""
                                        classField.school_id = dataclass["school_id"]?.string ?? ""
                                        print("         class \(classField.id) : \(classField.name)")
                                        classesList.append(classField)
                                    }


                                grade.classes = classesList
                                gradesList.append(grade)
                            }



                        stage.grade = gradesList
                        stagesList.append(stage)
                    }
                    complition(true, "found data", stagesList)
                }
            }
        }
    }
}

