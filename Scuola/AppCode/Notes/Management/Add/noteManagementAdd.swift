//
//  noteManagementAdd.swift
//  School
//
//  Created by ISCA-IOS on 8/5/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import TTGSnackbar

class noteManagementAdd: notesAddManagementAdapter, UITextViewDelegate {

    @IBOutlet weak var tvContent    : editTextView!
    @IBOutlet weak var viCategory   : viewOptions!
    @IBOutlet weak var viStages     : viewOptions!
    @IBOutlet weak var viClasses    : viewOptions!
    @IBOutlet weak var viGrade      : viewOptions!
    @IBOutlet weak var viKid        : viewOptions!
    @IBOutlet weak var btnSendHandler: UIButton!
    @IBOutlet weak var btnAttachmentHandle: UIButton!
    
    @IBOutlet weak var viTitle: UIView!
    
    func textViewDidChange(_ textView: UITextView) {
        if tvContent.text.count != 0{
            btnSendHandler.backgroundColor = GlobalColor.APP.blue
            btnSendHandler.isEnabled = true
        }else{
            btnSendHandler.backgroundColor = GlobalColor.APP.blue50
            btnSendHandler.setTitleColor(GlobalColor.APP.white, for: .normal)
            btnSendHandler.isEnabled = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tvContent.delegate = self
        self.tblCategory.tableFooterView = UIView()
        self.tblCategory.separatorInset = .zero
        self.tblCategory.contentInset = .zero
        self.tblCategory.separatorStyle = .none
        self.tblCategory.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblCategory.dataSource = self
        self.tblCategory.delegate = self
        
        
        self.tblStages.tableFooterView = UIView()
        self.tblStages.separatorInset = .zero
        self.tblStages.contentInset = .zero
        self.tblStages.separatorStyle = .none
        self.tblStages.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblStages.dataSource = self
        self.tblStages.delegate = self
        
        
        self.tblGrade.tableFooterView = UIView()
        self.tblGrade.separatorInset = .zero
        self.tblGrade.contentInset = .zero
        self.tblGrade.separatorStyle = .none
        self.tblGrade.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblGrade.dataSource = self
        self.tblGrade.delegate = self
        
        self.tblClasses.tableFooterView = UIView()
        self.tblClasses.separatorInset = .zero
        self.tblClasses.contentInset = .zero
        self.tblClasses.separatorStyle = .none
        self.tblClasses.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblClasses.dataSource = self
        self.tblClasses.delegate = self
        
        self.tblKids.tableFooterView = UIView()
        self.tblKids.separatorInset = .zero
        self.tblKids.contentInset = .zero
        self.tblKids.separatorStyle = .none
        self.tblKids.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblKids.dataSource = self
        self.tblKids.delegate = self
        
        
        
        viTitle.backgroundColor = GlobalColor.APP.blue
        btnAttachmentHandle.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnSendHandler.backgroundColor = GlobalColor.APP.blue50
        btnSendHandler.setTitleColor(GlobalColor.APP.white, for: .normal)
        btnSendHandler.isEnabled = false
        
        
        let tapCategory = UITapGestureRecognizer(target: self, action: #selector(tapCategories(_:)))
        viCategory.addGestureRecognizer(tapCategory)
        
        let tapStage = UITapGestureRecognizer(target: self, action: #selector(tapStages(_:)))
        viStages.addGestureRecognizer(tapStage)
        
        let tapGrade = UITapGestureRecognizer(target: self, action: #selector(tapGrades(_:)))
        viGrade.addGestureRecognizer(tapGrade)
        
        let tapClass = UITapGestureRecognizer(target: self, action: #selector(tapClasses(_:)))
        viClasses.addGestureRecognizer(tapClass)
        
        let tapKid = UITapGestureRecognizer(target: self, action: #selector(tapKids(_:)))
        viKid.addGestureRecognizer(tapKid)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func tapCategories(_ tapGesture: UITapGestureRecognizer){
        tblStages.isHidden = true
        tblGrade.isHidden = true
        tblClasses.isHidden = true
        tblKids.isHidden = true
        
        viContentTables.isHidden = false
        tblCategory.isHidden = false
    }
    @objc func tapStages(_ tapGesture: UITapGestureRecognizer){
        tblCategory.isHidden = true
        tblGrade.isHidden = true
        tblClasses.isHidden = true
        tblKids.isHidden = true
        
        viContentTables.isHidden = false
        tblStages.isHidden = false
    }
    @objc func tapGrades(_ tapGesture: UITapGestureRecognizer){
        tblCategory.isHidden = true
        tblStages.isHidden = true
        tblClasses.isHidden = true
        tblKids.isHidden = true
        
        viContentTables.isHidden = false
        tblGrade.isHidden = false
    }
    @objc func tapClasses(_ tapGesture: UITapGestureRecognizer){
        tblStages.isHidden = true
        tblGrade.isHidden = true
        tblCategory.isHidden = true
        tblKids.isHidden = true
        
        viContentTables.isHidden = false
        tblClasses.isHidden = false
        
    }
    @objc func tapKids(_ tapGesture: UITapGestureRecognizer){
        tblStages.isHidden = true
        tblGrade.isHidden = true
        tblCategory.isHidden = true
        tblClasses.isHidden = true
        
        viContentTables.isHidden = true
        if self.studentModel.count != 0{
            viContentTables.isHidden = false
            tblKids.isHidden = false
        }else{
            TTGSnackbar.init(message: "Not found Students", duration: .long).show()
        }
        
    }
    @IBAction func btnSendAction(_ sender: Any) {
        if tvContent.text == ""{
            TTGSnackbar.init(message: Alerts.textbox.notesContentMandatary.description, duration: .middle).show()
            return
        }
        if self.currentCategory == "0"{
            TTGSnackbar.init(message: Alerts.textbox.notesCategoryMandatary.description, duration: .middle).show()
            return
        }
        API.addNoteManagement(category_id: self.currentCategory, note: tvContent.text!, school_id: KeyValueShare.getManagementSchoolId(), stage_id: self.currentStage, grade_id: self.currentGrade, class_id: self.currentClass, student_id: self.currentKid, added_by: DatabaseManager.getInstance().getManagementInfo().id) { (result: Bool, msg: String) in
            TTGSnackbar.init(message: msg, duration: .long).show()
            if result == true{
                self.dismiss(animated: true, completion: nil)
            }
        }

    }

}
