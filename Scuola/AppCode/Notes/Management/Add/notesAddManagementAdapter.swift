//
//  notesAddManagementAdapter.swift
//  School
//
//  Created by ISCA-IOS on 8/5/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import TTGSnackbar

class notesAddManagementAdapter: UIViewController{
    
    let cellIdentifierGlobal = "GlobalNib"
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var tblStages: UITableView!
    @IBOutlet weak var tblGrade: UITableView!
    @IBOutlet weak var tblClasses: UITableView!
    @IBOutlet weak var tblKids: UITableView!
    @IBOutlet weak var viContentTables: UIView!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblStageTitle: UILabel!
    @IBOutlet weak var lblGradeTitle: UILabel!
    @IBOutlet weak var lblClassesTitle: UILabel!
    @IBOutlet weak var lblKidTitle: UILabel!
    
    var categoryModel   = [publicModel]()
    var stagesModel     = [stageModel]()
    var gradesModel     = [gradeModel]()
    var classesModel    = [classModel]()
    var studentModel    = [publicModel]()
    
    
    var currentCategory = "0"
    var currentStage    = "0"
    var currentGrade    = "0"
    var currentClass    = "0"
    var currentKid      = "0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateDropCat()
        populateDropStage()
        API.categoryList { (result: Bool?, msg: String?, category: [publicModel]?) in
            self.populateDropCat()
            if result == true{
                self.categoryModel.append(contentsOf: category!)
            }
            self.tblCategory.reloadData()
        }
        
        API.school_structure(school_id: KeyValueShare.getSchoolId()) { (result: Bool?, msg: String?, stages: [stageModel]?) in
            self.populateDropStage()
            if result == true{
                self.stagesModel.append(contentsOf: stages!)
            }
            self.tblStages.reloadData()
        }
        
        
    }
    func populateDropCat(){
        self.currentCategory = "0"
        lblCategoryTitle.text = Alerts.dropDownTitle.selectCategory.title
        
        let firstCat = publicModel()
        firstCat.id = "0"
        firstCat.title = Alerts.dropDownTitle.selectCategory.title
        self.categoryModel = [firstCat]
        self.tblCategory.reloadData()
    }
    
    func populateDropStage(){
        self.currentStage = "0"
        lblStageTitle.text = Alerts.dropDownTitle.AllStage.title
        
        let firstStage = stageModel()
        firstStage.id = "0"
        firstStage.stage = Alerts.dropDownTitle.AllStage.title
        self.stagesModel = [firstStage]
        self.tblStages.reloadData()
    }
    func populateDropGrade(){
        self.currentGrade = "0"
        self.lblGradeTitle.text = Alerts.dropDownTitle.AllGrade.title
        let firstGrade = gradeModel()
        firstGrade.id = "0"
        firstGrade.grade = Alerts.dropDownTitle.AllGrade.title
        gradesModel.insert(firstGrade, at: 0)
        tblGrade.reloadData()
    }
    func populateDropClass(){
        self.currentClass = "0"
        self.lblClassesTitle.text = Alerts.dropDownTitle.AllClasses.title
        let firstClass = classModel()
        firstClass.id = "0"
        firstClass.name = Alerts.dropDownTitle.AllClasses.title
        classesModel.insert(firstClass, at: 0)
        tblClasses.reloadData()
    }
    func populateDrowKid(){
        self.currentKid = "0"
        lblKidTitle.text = Alerts.dropDownTitle.AllStudents.title
        
        let firstStudent = publicModel()
        firstStudent.id = "0"
        firstStudent.title = Alerts.dropDownTitle.AllStudents.title
        self.studentModel = [firstStudent]
        self.tblKids.reloadData()
    }
    
}

extension noteManagementAdd: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == tblCategory{
            count = categoryModel.count
        }else if tableView == tblStages{
            count = stagesModel.count
        }else if tableView == tblGrade{
            count = gradesModel.count
        }else if tableView == tblClasses{
            count = classesModel.count
        }else{
            count = studentModel.count
        }
        
        return count!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierGlobal, for: indexPath) as! GlobalNib
        if tableView == tblCategory{
            cell.cellConfiguration(title: categoryModel[indexPath.row].title)
        }else if tableView == tblStages{
            cell.cellConfiguration(title: stagesModel[indexPath.row].stage)
        }else if tableView == tblGrade{
            cell.cellConfiguration(title: gradesModel[indexPath.row].grade)
        }else if tableView == tblClasses{
            cell.cellConfiguration(title: classesModel[indexPath.row].name)
        }else{
            cell.cellConfiguration(title: studentModel[indexPath.row].title)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblCategory{
            self.currentCategory = categoryModel[indexPath.row].id
            lblCategoryTitle.text = categoryModel[indexPath.row].title
        }else if tableView == tblStages{
            self.currentStage = stagesModel[indexPath.row].id
            lblStageTitle.text = stagesModel[indexPath.row].stage
            self.gradesModel = stagesModel[indexPath.row].grade
            self.populateDropGrade()
            
            
        }else if tableView == tblGrade{
            self.currentGrade = gradesModel[indexPath.row].id
            lblGradeTitle.text = gradesModel[indexPath.row].grade
            self.classesModel = gradesModel[indexPath.row].classes
            self.populateDropClass()
        }else if tableView == tblClasses{
            self.currentClass = classesModel[indexPath.row].id
            lblClassesTitle.text = classesModel[indexPath.row].name
            
            
            API.studentList(class_id: self.currentClass, school_id: KeyValueShare.getSchoolId()) { (result: Bool?, msg: String?, students: [publicModel]?) in
                self.populateDrowKid()
                if result == true{
                    self.studentModel.append(contentsOf: students!)
                }
                self.tblKids.reloadData()
            }
        }else{
            self.currentKid = studentModel[indexPath.row].id
            lblKidTitle.text = studentModel[indexPath.row].title
        }
        self.viContentTables.isHidden = true
        tblCategory.isHidden = true
        tblClasses.isHidden = true
        tblKids.isHidden = true
    }
    
    
    
}


extension noteManagementAdd: UITableViewDelegate{
    
}

