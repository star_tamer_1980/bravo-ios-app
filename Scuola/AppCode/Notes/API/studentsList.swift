//
//  studentsList.swift
//  School
//
//  Created by ISCA-IOS on 8/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func studentList(class_id: String, school_id: String, _ complition: @escaping(_ result: Bool?, _ msg: String?, _ classesList: [publicModel]?) -> Void){
        let url = URLs.studentList
        let parameters = [
            "class_id": class_id,
            "school_id": school_id
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                complition(false, value.localizedDescription, nil)
            case .success(let value):
                let json = JSON(value)
                if json["status"].bool! == false{
                    complition(false, json["msg"].string!, nil)
                    return
                }else{
                    guard let resultArr = json["students"].array else{
                        complition(false, json["msg"].string!, nil)
                        return
                    }
                    var studentList = [publicModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let student = publicModel()
                        student.id = data["id"]?.string ?? "0"
                        student.title = data["student_name"]?.string ?? "0"
                        studentList.append(student)
                    }
                    complition(true, "found data", studentList)
                }
            }
        }
    }
}

