//
//  categoryList.swift
//  School
//
//  Created by ISCA-IOS on 8/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func categoryList(_ complition: @escaping(_ result: Bool?, _ msg: String?, _ categoryList: [publicModel]?) -> Void){
        let url = URLs.categoryList
        let parameters = [
            "id": "10"
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                complition(false, value.localizedDescription, nil)
            case .success(let value):
                let json = JSON(value)
                if json["status"].bool! == false{
                    complition(false, json["msg"].string!, nil)
                    return
                }else{
                    guard let resultArr = json["Result"].array else{
                        complition(false, json["msg"].string!, nil)
                        return
                    }
                    var categoryList = [publicModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let category = publicModel()
                        category.id = data["id"]?.string ?? "0"
                        category.title = data["category"]?.string ?? "0"
                        categoryList.append(category)
                    }
                    complition(true, "found data", categoryList)
                }
            }
        }
    }
}
