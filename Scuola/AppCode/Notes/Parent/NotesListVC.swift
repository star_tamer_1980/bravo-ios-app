//
//  NotesListVC.swift
//  School
//
//  Created by ISCA-IOS on 7/28/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class NotesListVC: NotesListAdapter {

    @IBOutlet weak var btnPublicHandler: UIButton!
    @IBOutlet weak var btnClassHandler: UIButton!
    @IBOutlet weak var btnPrivateHandler: UIButton!
    @IBOutlet weak var tblPublicConstrainWidth: NSLayoutConstraint!
    @IBOutlet weak var tblClassConstrainWidth: NSLayoutConstraint!
    @IBOutlet weak var tblPrivateConstrainWidth: NSLayoutConstraint!
    @IBOutlet weak var viNotesContainer: UIView!
    @IBOutlet weak var viTitle: UIView!
    var currentNotes = "public" // public, class, private
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tblPublicNotes.tableFooterView = UIView()
        self.tblPublicNotes.separatorInset = .zero
        self.tblPublicNotes.contentInset = .zero
        self.tblPublicNotes.separatorStyle = .none
        self.tblPublicNotes.addSubview(refreshForPublic)
        self.tblPublicNotes.register(UINib.init(nibName: self.cellPublic, bundle: nil), forCellReuseIdentifier: cellPublic)
        self.tblPublicNotes.dataSource = self
        self.tblPublicNotes.delegate = self
        
        self.tblClassNotes.tableFooterView = UIView()
        self.tblClassNotes.separatorInset = .zero
        self.tblClassNotes.contentInset = .zero
        self.tblClassNotes.separatorStyle = .none
        self.tblClassNotes.addSubview(refreshForClass)
        self.tblClassNotes.register(UINib.init(nibName: self.cellClass, bundle: nil), forCellReuseIdentifier: self.cellClass)
        self.tblClassNotes.dataSource = self
        self.tblClassNotes.delegate = self
        
        self.tblPrivateNotes.tableFooterView = UIView()
        self.tblPrivateNotes.separatorInset = .zero
        self.tblPrivateNotes.contentInset = .zero
        self.tblPrivateNotes.separatorStyle = .none
        self.tblPrivateNotes.addSubview(refreshForPrivate)
        self.tblPrivateNotes.register(UINib.init(nibName: self.cellPrivate, bundle: nil), forCellReuseIdentifier: self.cellPrivate)
        self.tblPrivateNotes.dataSource = self
        self.tblPrivateNotes.delegate = self
        
        DatabaseManager.getInstance().get_notes_List(self.studentId, "1") { (haveNotes: Bool, localPublicNotes: [NotesModel]) in
            print("local notes count \(localPublicNotes.count)")
            self.publicNotes = localPublicNotes
        }
        
        DatabaseManager.getInstance().get_notes_List(self.studentId, "2") { (haveNotes: Bool, localClassNotes: [NotesModel]) in
            print("local class notes \(localClassNotes.count)")
            self.classNotes = localClassNotes
        }
        
        DatabaseManager.getInstance().get_notes_List(self.studentId, "3") { (haveNotes: Bool, localPrivateNotes: [NotesModel]) in
            print("local private notes \(localPrivateNotes.count)")
            self.privateNotes = localPrivateNotes
        }
        
        
        self.tblPublicNotes.reloadData()
        self.tblClassNotes.reloadData()
        self.tblPrivateNotes.reloadData()
        
        btnClassHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnPrivateHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        viTitle.backgroundColor = GlobalColor.APP.blue
        
        tblClassConstrainWidth.constant = view.bounds.width
        tblPublicConstrainWidth.constant = view.bounds.width
        tblPrivateConstrainWidth.constant = view.bounds.width
        
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPublicAction(_ sender: Any) {
        animeToPublic()
    }
    @IBAction func btnClassAction(_ sender: Any) {
        animeToClass()
        
    }
    @IBAction func btnPrivateAction(_ sender: Any) {
        animeToPrivate()
    }
    
    
    func animeToPublic(){
        self.currentTable = 1
        
        btnPublicHandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
        btnClassHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnPrivateHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        
        if currentNotes == "class"{
            currentNotes = "public"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblPublicNotes.frame.origin.x += self.view.bounds.width
                self.tblClassNotes.frame.origin.x += self.view.bounds.width
                self.tblPrivateNotes.frame.origin.x += self.view.bounds.width
            }, completion: nil)
        }else if currentNotes == "private"{
            currentNotes = "public"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblPublicNotes.frame.origin.x += self.view.bounds.width * 2
                self.tblClassNotes.frame.origin.x += self.view.bounds.width * 2
                self.tblPrivateNotes.frame.origin.x += self.view.bounds.width * 2
            }, completion: nil)
        }
        
        
    }
    func animeToClass(){
        self.currentTable = 2
        
        btnPublicHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnClassHandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
        btnPrivateHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        
        if currentNotes == "public"{
            currentNotes = "class"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblPublicNotes.frame.origin.x -= self.view.bounds.width
                self.tblClassNotes.frame.origin.x -= self.view.bounds.width
                self.tblPrivateNotes.frame.origin.x -= self.view.bounds.width
            }, completion: nil)
        }else if currentNotes == "private"{
            currentNotes = "class"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblPublicNotes.frame.origin.x += self.view.bounds.width
                self.tblClassNotes.frame.origin.x += self.view.bounds.width
                self.tblPrivateNotes.frame.origin.x += self.view.bounds.width
            }, completion: nil)
        }
    }
    func animeToPrivate(){
        self.currentTable = 3
        
        btnPublicHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnClassHandler.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnPrivateHandler.setTitleColor(GlobalColor.APP.blue, for: .normal)
        
        if currentNotes == "public"{
            currentNotes = "private"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblPublicNotes.frame.origin.x -= self.view.bounds.width * 2
                self.tblClassNotes.frame.origin.x -= self.view.bounds.width * 2
                self.tblPrivateNotes.frame.origin.x -= self.view.bounds.width * 2
            }, completion: nil)
        }else if currentNotes == "class"{
            currentNotes = "private"
            UIView.animate(withDuration: AppValues.animeDuration.time2, delay: 0.0, options: [.curveEaseIn], animations: {
                self.tblPublicNotes.frame.origin.x -= self.view.bounds.width
                self.tblClassNotes.frame.origin.x -= self.view.bounds.width
                self.tblPrivateNotes.frame.origin.x -= self.view.bounds.width
            }, completion: nil)
        }
    }

}
