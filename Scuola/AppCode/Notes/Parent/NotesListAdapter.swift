//
//  NotesListAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/28/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
class NotesListAdapter: UIViewController{
    var celHeightPublic: CGFloat = 150.0
    var celHeightClass: CGFloat = 150.0
    var celHeightPrivate: CGFloat = 150.0
    @IBOutlet weak var tblPublicNotes: UITableView!
    @IBOutlet weak var tblClassNotes: UITableView!
    @IBOutlet weak var tblPrivateNotes: UITableView!
    var publicNotes = [NotesModel]()
    var classNotes = [NotesModel]()
    var privateNotes = [NotesModel]()
    let cellPublic = "NotesNib"
    let cellClass = "NotesNib"
    let cellPrivate = "NotesNib"
    var limitPublic: String = "5"
    var limitClass: String = "5"
    var limitPrivate: String = "5"
    var currentTable = 1 // 1 => public, 2 => class, 3 => private
    var isLoadingPublic: Bool = false
    var isLoadingClass: Bool = false
    var isLoadingPrivate: Bool = false
    var isLoadMorePublic: Bool = true
    var isLoadMoreClass: Bool = true
    var isLoadMorePrivate: Bool = true
    var studentId = KeyValueShare.getStudentId()
    lazy var refreshForPublic: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(RefreshPublicNotes), for: .valueChanged)
        return refresher
    }()
    lazy var refreshForClass: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(RefreshClassNotes), for: .valueChanged)
        return refresher
    }()
    lazy var refreshForPrivate: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(RefreshPrivateNotes), for: .valueChanged)
        return refresher
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
    }
    
    func getData(){
        print("get data")
        
        handleRefresh()
    }
    
    @objc func handleRefresh(){
        RefreshPublicNotes()
        RefreshClassNotes()
        RefreshPrivateNotes()
    }
    
    
    @objc func RefreshPublicNotes(){
        self.refreshForPublic.endRefreshing()
        self.tblPublicNotes.reloadData()
        guard !isLoadingPublic else {
            return
        }
        self.isLoadingPublic = true
        API.parentNotesListNetwork(student_id: studentId, note_type: "1", item_count: "0", limit: "20") { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoadingPublic = false
            if result == true{
                if let notes = notes{
                    self.publicNotes = notes
                    self.tblPublicNotes.reloadData()
                    self.isLoadMorePublic = isLoadMore!
                    self.limitPublic = "10"
                }
            }else{
                self.isLoadMorePublic = false
            }
        }
    }
    @objc func RefreshClassNotes(){
        self.refreshForClass.endRefreshing()
        self.tblClassNotes.reloadData()
        guard !isLoadingClass else {
            return
        }
        isLoadingClass = true
        API.parentNotesListNetwork(student_id: studentId, note_type: "2", item_count: "0", limit: "20") { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoadingPublic = false
            if result == true{
                if let notes = notes{
                    self.classNotes = notes
                    self.tblClassNotes.reloadData()
                    self.isLoadMoreClass = isLoadMore!
                    self.limitClass = "10"
                }
            }else{
                self.isLoadMoreClass = false
            }
        }
        
    }
    @objc func RefreshPrivateNotes(){
        self.refreshForPrivate.endRefreshing()
        self.tblPrivateNotes.reloadData()
        guard !isLoadingPrivate else {
            return
        }
        isLoadingPrivate = true
        API.parentNotesListNetwork(student_id: studentId, note_type: "3", item_count: "0", limit: "20") { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoadingPrivate = false
            if result == true{
                if let notes = notes{
                    self.privateNotes = notes
                    self.tblPrivateNotes.reloadData()
                    self.isLoadMorePrivate = isLoadMore!
                    self.limitPrivate = "10"
                }
            }else{
                self.isLoadMorePrivate = false
            }
        }
        
    }
    
    
    
    @objc func handleRfreshForPublic(){
        self.refreshForPublic.endRefreshing()
        self.tblPublicNotes.reloadData()
        guard !isLoadingPublic else {
            return
        }
        self.isLoadingPublic = true
        API.parentNotesListNetwork(student_id: studentId, note_type: "1", item_count: String(publicNotes.count), limit: self.limitPublic) { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoadingPublic = false
            if result == true{
                if let notes = notes{
                    if self.publicNotes.count == 0{
                        self.publicNotes = notes
                    }else{
                        self.publicNotes.append(contentsOf: notes)
                    }
                    self.tblPublicNotes.reloadData()
                    self.isLoadMorePublic = isLoadMore!
                    self.limitPublic = "10"
                }
            }else{
                self.isLoadMorePublic = false
            }
        }
    }
    
    @objc func handleRfreshForClass(){
        self.refreshForClass.endRefreshing()
        self.tblClassNotes.reloadData()
        guard !isLoadingClass else {
            return
        }
        isLoadingClass = true
        API.parentNotesListNetwork(student_id: studentId, note_type: "2", item_count: String(classNotes.count), limit: self.limitClass) { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoadingClass = false
            if result == true{
                if let notes = notes{
                    if self.classNotes.count == 0{
                        self.classNotes = notes
                    }else{
                        self.classNotes.append(contentsOf: notes)
                    }
                    self.tblClassNotes.reloadData()
                    self.isLoadMoreClass = isLoadMore!
                    self.limitClass = "10"
                }
            }else{
                self.isLoadMoreClass = false
            }
        }
        
    }
    
    @objc func handleRfreshForPrivate(){
        self.refreshForPrivate.endRefreshing()
        self.tblPrivateNotes.reloadData()
        guard !isLoadingPrivate else {
            return
        }
        isLoadingPrivate = true
        API.parentNotesListNetwork(student_id: studentId, note_type: "2", item_count: String(classNotes.count), limit: self.limitClass) { (result: Bool?, msg: String?, notes: [NotesModel]?, isLoadMore: Bool?) in
            self.isLoadingClass = false
            if result == true{
                if let notes = notes{
                    if self.privateNotes.count == 0{
                        self.privateNotes = notes
                    }else{
                        self.privateNotes.append(contentsOf: notes)
                    }
                    self.tblPrivateNotes.reloadData()
                    self.isLoadMorePrivate = isLoadMore!
                    self.limitPrivate = "10"
                }
            }else{
                self.isLoadMorePrivate = false
            }
        }
        
    }
    
    func loadPublicMore(){
        guard !isLoadingPublic else {
            return
        }
        guard isLoadMorePublic else {
            return
        }
        //isLoadingClosed = true
        handleRfreshForPublic()
    }
    
    func loadClassMore(){
        guard !isLoadingClass else {
            return
        }
        guard isLoadMoreClass else {
            return
        }
        //isLoadingOpened = true
        handleRfreshForClass()
    }
    
    func loadPrivateMore(){
        guard !isLoadingPrivate else {
            return
        }
        guard isLoadMorePrivate else {
            return
        }
        //isLoadingOpened = true
        handleRfreshForPrivate()
    }
    
    
}

extension NotesListVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == tblPublicNotes{
            count = publicNotes.count
        }else if tableView == tblClassNotes{
            count = classNotes.count
        }else{
            count = privateNotes.count
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == tblPublicNotes{
            let cell = tableView.dequeueReusableCell(withIdentifier: self.cellPublic, for: indexPath) as! NotesNib
            cell.cellConfigure(note: publicNotes[indexPath.row])
            return cell
        }else if tableView == tblClassNotes{
            let cell = tableView.dequeueReusableCell(withIdentifier: self.cellClass, for: indexPath) as! NotesNib
            cell.cellConfigure(note: classNotes[indexPath.row])
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: self.cellPrivate, for: indexPath) as! NotesNib
            cell.cellConfigure(note: privateNotes[indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "parent_notes", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "NotesDetailVC") as! NotesDetailVC
        if tableView == tblPublicNotes{
            myController.Note = publicNotes[indexPath.row]
        }else if tableView == tblClassNotes{
            myController.Note = classNotes[indexPath.row]
        }else{
            myController.Note = privateNotes[indexPath.row]
        }

        self.present(myController, animated: true)
        
        
        
        
    }
    
}

extension NotesListVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat?
        if tableView == tblPublicNotes{
            height = celHeightPublic
        }else if tableView == tblClassNotes{
            height = celHeightClass
        }else{
            height = celHeightPrivate
        }
        return height!
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        return view
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var count: Int?
        if tableView == tblPublicNotes {
            count = publicNotes.count
            if indexPath.row == count!-1{
                self.loadPublicMore()
            }
        }else if tableView == tblClassNotes{
            count = classNotes.count
            if indexPath.row == count!-1{
                self.loadClassMore()
            }
        }else{
            count = privateNotes.count
            if indexPath.row == count!-1{
                self.loadPrivateMore()
            }
        }
    }
}
