//
//  NotesModel.swift
//  School
//
//  Created by ISCA-IOS on 7/28/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class NotesModel{
    var id: Int = 0
    var category_id: String = ""
    var category_name: String = ""
    var note: String = ""
    var note_type: String = ""
    var student_id: String = ""
    var stage_id: String = ""
    var grade_id: String = ""
    var class_id: String = ""
    var school_id: String = ""
    var added_by: String = ""
    var added_on: String = ""
    var note_to: String = ""
    var attach = [attachmentModel]()
}

