//
//  notesAddAdapter.swift
//  School
//
//  Created by ISCA-IOS on 8/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import TTGSnackbar

class notesAddAdapter: UIViewController {

    let cellIdentifierGlobal = "GlobalNib"
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var tblTeacherClasses: UITableView!
    @IBOutlet weak var tblKids: UITableView!
    @IBOutlet weak var viContentTables: UIView!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblTeacherClassesTitle: UILabel!
    @IBOutlet weak var lblKidTitle: UILabel!
    
    var categoryModel   = [publicModel]()
    var teacherClasses  = [publicModel]()
    var studentModel    = [publicModel]()
    
    
    var currentCategory = "0"
    var currentClass    = "0"
    var currentKid      = "0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let teacherInfo = DatabaseManager.getInstance().getTeacherInfo()
        populateDropCat()
        populateDropClass()
        populateDrowKid()
        API.categoryList { (result: Bool?, msg: String?, category: [publicModel]?) in
            self.populateDropCat()
            if result == true{
                self.categoryModel.append(contentsOf: category!)
            }
            self.tblCategory.reloadData()
        }
        
        API.classesListForTeacher(user_id: teacherInfo.id, school_id: KeyValueShare.getSchoolId()) { (result: Bool?, msg: String?, classes: [publicModel]?) in
            self.populateDropClass()
            if result == true{
                self.teacherClasses.append(contentsOf: classes!)
            }
            self.tblTeacherClasses.reloadData()
        }
        
        
    }
    func populateDropCat(){
        self.currentClass = "0"
        lblTeacherClassesTitle.text = Alerts.dropDownTitle.selectCategory.title
        
        let firstCat = publicModel()
        firstCat.id = "0"
        firstCat.title = Alerts.dropDownTitle.selectCategory.title
        self.categoryModel = [firstCat]
        self.tblCategory.reloadData()
    }
    
    
    func populateDropClass(){
        self.currentClass = "0"
        lblTeacherClassesTitle.text = Alerts.dropDownTitle.selectClass.title
        
        let firstClass = publicModel()
        firstClass.id = "0"
        firstClass.title = Alerts.dropDownTitle.selectClass.title
        self.teacherClasses = [firstClass]
        self.tblTeacherClasses.reloadData()
    }
    
    func populateDrowKid(){
        self.currentKid = "0"
        lblKidTitle.text = Alerts.dropDownTitle.AllStudents.title
        
        let firstStudent = publicModel()
        firstStudent.id = "0"
        firstStudent.title = Alerts.dropDownTitle.AllStudents.title
        self.studentModel = [firstStudent]
        self.tblKids.reloadData()
    }

}

extension notesAddVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == tblCategory{
            count = categoryModel.count
        }else if tableView == tblTeacherClasses{
            count = teacherClasses.count
        }else{
            count = studentModel.count
        }
        
        return count!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierGlobal, for: indexPath) as! GlobalNib
        if tableView == tblCategory{
            cell.cellConfiguration(title: categoryModel[indexPath.row].title)
        }else if tableView == tblTeacherClasses{
            cell.cellConfiguration(title: teacherClasses[indexPath.row].title)
        }else{
            cell.cellConfiguration(title: studentModel[indexPath.row].title)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblCategory{
            self.currentCategory = categoryModel[indexPath.row].id
            lblCategoryTitle.text = categoryModel[indexPath.row].title
        }else if tableView == tblTeacherClasses{
            self.currentClass = teacherClasses[indexPath.row].id
            lblTeacherClassesTitle.text = teacherClasses[indexPath.row].title
            
            self.currentKid = ""
            self.lblKidTitle.text = Alerts.dropDownTitle.AllStudents.title
            
            API.studentList(class_id: self.currentClass, school_id: KeyValueShare.getSchoolId()) { (result: Bool?, msg: String?, students: [publicModel]?) in
                self.populateDrowKid()
                if result == true{
                    self.studentModel.append(contentsOf: students!)
                }
                self.tblKids.reloadData()
            }
        }else{
            self.currentKid = studentModel[indexPath.row].id
            lblKidTitle.text = studentModel[indexPath.row].title
        }
        self.viContentTables.isHidden = true
        tblCategory.isHidden = true
        tblTeacherClasses.isHidden = true
        tblKids.isHidden = true
    }
    
    
    
}


extension notesAddVC: UITableViewDelegate{
    
}
