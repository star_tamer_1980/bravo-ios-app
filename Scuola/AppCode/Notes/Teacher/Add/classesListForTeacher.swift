//
//  classesListForTeacher.swift
//  School
//
//  Created by ISCA-IOS on 8/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func classesListForTeacher(user_id: String, school_id: String, _ complition: @escaping(_ result: Bool?, _ msg: String?, _ classesList: [publicModel]?) -> Void){
        let url = URLs.classesList
        let parameters = [
            "user_id": user_id,
            "school_id": school_id
        ]
        print("user id : \(user_id)")
        print("school id: \(school_id)")
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let value):
                complition(false, value.localizedDescription, nil)
            case .success(let value):
                print("class is \(value)")
                let json = JSON(value)
                if json["status"].bool! == false{
                    complition(false, json["msg"].string!, nil)
                    return
                }else{
                    guard let resultArr = json["classes"].array else{
                        complition(false, json["msg"].string!, nil)
                        return
                    }
                    var classesList = [publicModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let classes = publicModel()
                        classes.id = data["class_id"]?.string ?? "0"
                        classes.title = data["class_name"]?.string ?? ""
                        classesList.append(classes)
                    }
                    complition(true, "found data", classesList)
                }
            }
        }
    }
}
