//
//  notesAddVC.swift
//  School
//
//  Created by ISCA-IOS on 8/1/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import TTGSnackbar

class notesAddVC: notesAddAdapter, UITextViewDelegate {

    @IBOutlet weak var tvContent: editTextView!
    @IBOutlet weak var viCategory: viewOptions!
    @IBOutlet weak var viTeacherClasses: viewOptions!
    @IBOutlet weak var viKid: viewOptions!
    @IBOutlet weak var btnSendHandler: UIButton!
    @IBOutlet weak var btnAttachmentHandle: UIButton!
    
    @IBOutlet weak var viTitle: UIView!
    
    func textViewDidChange(_ textView: UITextView) {
        if tvContent.text.count != 0{
            btnSendHandler.backgroundColor = GlobalColor.APP.blue
            btnSendHandler.isEnabled = true
        }else{
            btnSendHandler.backgroundColor = GlobalColor.APP.blue50
            btnSendHandler.setTitleColor(GlobalColor.APP.white, for: .normal)
            btnSendHandler.isEnabled = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tvContent.delegate = self
        self.tblCategory.tableFooterView = UIView()
        self.tblCategory.separatorInset = .zero
        self.tblCategory.contentInset = .zero
        self.tblCategory.separatorStyle = .none
        self.tblCategory.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblCategory.dataSource = self
        self.tblCategory.delegate = self

        self.tblTeacherClasses.tableFooterView = UIView()
        self.tblTeacherClasses.separatorInset = .zero
        self.tblTeacherClasses.contentInset = .zero
        self.tblTeacherClasses.separatorStyle = .none
        self.tblTeacherClasses.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblTeacherClasses.dataSource = self
        self.tblTeacherClasses.delegate = self
        
        self.tblKids.tableFooterView = UIView()
        self.tblKids.separatorInset = .zero
        self.tblKids.contentInset = .zero
        self.tblKids.separatorStyle = .none
        self.tblKids.register(UINib.init(nibName: self.cellIdentifierGlobal, bundle: nil), forCellReuseIdentifier: self.cellIdentifierGlobal)
        self.tblKids.dataSource = self
        self.tblKids.delegate = self


        
        viTitle.backgroundColor = GlobalColor.APP.blue
        btnAttachmentHandle.setTitleColor(GlobalColor.APP.gray, for: .normal)
        btnSendHandler.backgroundColor = GlobalColor.APP.blue50
        btnSendHandler.setTitleColor(GlobalColor.APP.white, for: .normal)
        btnSendHandler.isEnabled = false
        
        
        let tapCategory = UITapGestureRecognizer(target: self, action: #selector(tapCategories(_:)))
        viCategory.addGestureRecognizer(tapCategory)
        
        let tapClass = UITapGestureRecognizer(target: self, action: #selector(tapClasses(_:)))
        viTeacherClasses.addGestureRecognizer(tapClass)
        
        let tapKid = UITapGestureRecognizer(target: self, action: #selector(tapKids(_:)))
        viKid.addGestureRecognizer(tapKid)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func tapCategories(_ tapGesture: UITapGestureRecognizer){
        tblTeacherClasses.isHidden = true
        tblKids.isHidden = true
        viContentTables.isHidden = false
        tblCategory.isHidden = false
    }
    @objc func tapClasses(_ tapGesture: UITapGestureRecognizer){
        tblCategory.isHidden = true
        tblKids.isHidden = true
        
        viContentTables.isHidden = false
        tblTeacherClasses.isHidden = false
       
    }
    @objc func tapKids(_ tapGesture: UITapGestureRecognizer){
        tblCategory.isHidden = true
        tblTeacherClasses.isHidden = true
        viContentTables.isHidden = true
        if self.studentModel.count != 0{
            viContentTables.isHidden = false
            tblKids.isHidden = false
        }else{
            TTGSnackbar.init(message: "Not found Students", duration: .long).show()
        }
        
    }
    @IBAction func btnSendAction(_ sender: Any) {
        if tvContent.text == ""{
            TTGSnackbar.init(message: Alerts.textbox.notesContentMandatary.description, duration: .middle).show()
            return
        }
        if self.currentCategory == "0"{
            TTGSnackbar.init(message: Alerts.textbox.notesCategoryMandatary.description, duration: .middle).show()
            return
        }
        if self.currentClass == "0"{
            TTGSnackbar.init(message: Alerts.textbox.notesClassMandatary.description, duration: .middle).show()
            return
        }
        
        API.addNoteTeacher(category_id: self.currentCategory, note: tvContent.text!, school_id: KeyValueShare.getSchoolId(), class_id: self.currentClass, student_id: self.currentKid, added_by: DatabaseManager.getInstance().getTeacherInfo().id) { (result: Bool?, msg: String) in
            TTGSnackbar.init(message: msg, duration: .long).show()
            if result == true{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
