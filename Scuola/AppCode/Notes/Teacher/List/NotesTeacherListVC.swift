//
//  NotesTeacherListVC.swift
//  School
//
//  Created by ISCA-IOS on 7/31/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class NotesTeacherListVC: NotesTeacherListAdapter {
    
    @IBOutlet weak var viTitle: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblNotes.tableFooterView = UIView()
        self.tblNotes.separatorInset = .zero
        self.tblNotes.contentInset = .zero
        self.tblNotes.separatorStyle = .none
        self.tblNotes.addSubview(refresh)
        self.tblNotes.register(UINib.init(nibName: self.cellNote, bundle: nil), forCellReuseIdentifier: cellNote)
        self.tblNotes.dataSource = self
        self.tblNotes.delegate = self
        
        DatabaseManager.getInstance().get_notes_List_members(self.teacherId, self.schoolId) { (result: Bool, notes: [NotesModel]) in
            self.notesList = notes
        }
        
        self.tblNotes.reloadData()
        viTitle.backgroundColor = GlobalColor.APP.blue
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddNoteAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "teacher_notes", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "notesAddVC")
        self.present(myController, animated: true)
    }
    
}
