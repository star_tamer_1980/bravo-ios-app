//
//  ImageZoom.swift
//  School
//
//  Created by ISCA-IOS on 7/22/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Kingfisher

class ImageZoom: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var imgData: UIImageView!
    var urlImage: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll.delegate = self
        print("tests url : \(urlImage)")
        let url = URL(string: urlImage)
        imgData.kf.setImage(with: url)
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgData
    }

    @IBAction func btnBackAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
