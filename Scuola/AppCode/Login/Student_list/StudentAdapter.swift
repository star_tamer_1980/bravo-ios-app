//
//  StudentAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class StudentAdapter: UIViewController {

    let celTable = "cellTable"
    let celHeight: CGFloat = 130.0
    @IBOutlet weak var tblKids: UITableView!
    var Students = [StudentModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblKids.tableFooterView = UIView()
        self.tblKids.separatorInset = .zero
        self.tblKids.contentInset = .zero
        self.tblKids.separatorStyle = .none
        self.tblKids.register(UINib.init(nibName: "listStudentXib", bundle: nil), forCellReuseIdentifier: celTable)
        self.tblKids.dataSource = self
        self.tblKids.delegate = self
    }
    

    func getData(){
        DatabaseManager.getInstance().getStudentsList { (result: Bool, students: [StudentModel]) in
            if result == true{
                self.Students = students
                self.tblKids.reloadData()
            }
        }
    }
    
    

}


extension StudentAdapter: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Students.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: celTable, for: indexPath) as! listStudentXib
        
        cell.cellConfiguration(schoolName: self.Students[indexPath.row].school_name, className: self.Students[indexPath.row].class_name, studentName: self.Students[indexPath.row].first_name + self.Students[indexPath.row].last_name, studentImage: self.Students[indexPath.row].student_photo)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        KeyValueShare.setStudentId(self.Students[indexPath.row].id)
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller = (storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController)
        self.present(controller, animated: true)
    }
}

extension StudentAdapter: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return celHeight
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        return view
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
