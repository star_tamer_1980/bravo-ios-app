//
//  listStudentXib.swift
//  School
//
//  Created by ISCA-IOS on 7/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Kingfisher

class listStudentXib: UITableViewCell {
    @IBOutlet weak var lblSchoolName: Green50Label!
    @IBOutlet weak var imgStudent: UIImageView!
    @IBOutlet weak var lblStudentName: Label!
    @IBOutlet weak var lblClassName: Label!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblSchoolName.textColor = GlobalColor.APP.blue
        lblStudentName.textColor = GlobalColor.APP.blue
        lblClassName.textColor = GlobalColor.APP.blue
        lblClassName.font.withSize(10)
    }

    func cellConfiguration(schoolName: String, className: String, studentName: String, studentImage: String){
        self.lblSchoolName.text = schoolName
        self.lblClassName.text = className
        self.lblStudentName.text = studentName
        print("student image: \(studentImage)")
        let url = URL(string: studentImage)
        self.imgStudent.kf.setImage(with: url)
    }
    
}
