//
//  Student_list_VC.swift
//  School
//
//  Created by ISCA-IOS on 7/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class Student_list_VC: StudentAdapter {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
