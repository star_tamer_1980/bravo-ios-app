//
//  LoginVC.swift
//  School
//
//  Created by ISCA-IOS on 6/23/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import CountryPicker
import IQKeyboardManagerSwift
import AudioToolbox

class LoginVC: UIViewController, CountryPickerDelegate {
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    var phoneCodeCountry: String = ""
    @IBOutlet weak var btnOpenCodeCountryHandler: UIButton!
    @IBOutlet weak var stCountry: UIStackView!
    @IBOutlet weak var btnLoginHandle: UIButton!
    @IBOutlet weak var btnLogin: Blue!
    @IBOutlet weak var CountryCodePicker: CountryPicker!
    @IBOutlet weak var txtMobile: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMobile.setStyle()
        
       
        txtMobile.keyboardType = UIKeyboardType.numberPad
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        
        
        btnLoginHandle.pulsete()
        btnLoginHandle.flash()
        btnLoginHandle.shake()
        startLoadButton()
        
        
        if(txtMobile.text == ""){
            stopLoadButton()
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            let storyboard = UIStoryboard(name: "Messages", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "mes1") as! mes1)
            controller.titleSt = Alerts.textbox.phoneNumber.empty.head
            controller.descriptionSt = Alerts.textbox.phoneNumber.empty.description
            self.present(controller, animated: true)
        }else{
            if var phoneNumber = txtMobile.text{
                phoneNumber = validatePhoneNumber.remove0(value: &phoneNumber)
                phoneNumber = phoneCodeCountry + phoneNumber
                let alert = UIAlertController(title: "Phone number", message: "This is your number?  \(phoneNumber)", preferredStyle: .alert)
                let actionYes = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
                    self.syncNetworkAPI()
                }
                let actionNo = UIAlertAction(title: "No", style: .cancel) { (UIAlertAction) in
                    self.stopLoadButton()
                }
                alert.addAction(actionYes)
                alert.addAction(actionNo)
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
    }
    
    private func syncNetworkAPI(){
        view.endEditing(true)
        
        if !Reachability.isConnectedToInternet(){
            self.stopLoadButton()
            return
        }
        if self.syncAppVersionAPI() != true{
            return
        }
        API.login(phoneCodeCountry, txtMobile.text!) { (result: Bool?, message: String?) in
            if result != true{
                let storyboard = UIStoryboard(name: "Messages", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "mes1") as! mes1)
                controller.titleSt = Alerts.textbox.phoneNumber.empty.head
                controller.descriptionSt = message
                self.present(controller, animated: true)
            }else{
                
                 let storyboard = UIStoryboard(name: "Login", bundle: nil)
                let myController =
                    storyboard.instantiateViewController(withIdentifier: "memberListTypeVC") as! memberListTypeVC
                self.present(myController, animated: true)
 
            }
            self.stopLoadButton()
        }
    }
    
    private func syncAppVersionAPI() -> Bool{
        
        return true
    }
    
    private func startLoadButton(){
        
        loading.startAnimating()
        btnLoginHandle.isEnabled = false
        btnLoginHandle.alpha = 0.7
    }
    private func stopLoadButton(){
        btnLoginHandle.isEnabled = true
        btnLoginHandle.alpha = 1.0
        loading.stopAnimating()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        IQKeyboardManager.shared.enable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
        CountryCodePicker.countryPickerDelegate = self
        CountryCodePicker.showPhoneNumbers = true
        //        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .white, rowBackgroundColor: .black, showFlagsBorder: true) //optional
        //        picker.theme = theme //optional
        
        CountryCodePicker.setCountry(code!)
        print("phone c \(phoneCodeCountry)")
        self.btnOpenCodeCountryHandler.setTitle(phoneCodeCountry, for: .normal)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 30.0
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 30.0
            }
        }
    }
    
    @IBAction func btnOpenCodeCountryAction(_ sender: Any) {
        self.stCountry.isHidden = false
    }
    @IBAction func btnCountrypickerHiddenAction(_ sender: Any) {
        self.stCountry.isHidden = true
    }
    @IBAction func btnOkCountry(_ sender: Any) {
        self.btnOpenCodeCountryHandler.setTitle(phoneCodeCountry, for: .normal)
        self.stCountry.isHidden = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        print("phone code \(phoneCode)")
        phoneCodeCountry = phoneCode
    }
}
