//
//  LoginNetwork.swift
//  School
//
//  Created by ISCA-IOS on 7/3/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func login(_ code: String, _ phoneNumber: String,_ completion: @escaping(_ status: Bool?, _ message: String?) -> Void){
        let url = URLs.login
        let parameters = [
            "c_code" : code,
            "mobile" : phoneNumber
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let error):
                completion(false, error.localizedDescription)
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].bool! == false{
                    completion(false, json["msg"].string!)
                }else{
                    guard let resultArr = json["Result"].array else{
                        completion(false, "")
                        return
                    }
                    DatabaseManager.getInstance().deleteMemberInfoTable()
                    DatabaseManager.getInstance().createMemberInfoTable()
                    DatabaseManager.getInstance().deleteHomeIconTable()
                    DatabaseManager.getInstance().createHomeIconTable()
                    DatabaseManager.getInstance().deleteStudentTable()
                    DatabaseManager.getInstance().createStudentTable()
                    DatabaseManager.getInstance().deleteSchoolsTable()
                    DatabaseManager.getInstance().createSchoolsTable()
                    DatabaseManager.getInstance().delete_notes_Table()
                    DatabaseManager.getInstance().create_notes_InfoTable()
                    for result in resultArr{
                        
                        guard let data = result.dictionary else {return}
                        let memberInfo = memberInfoModel()
                        memberInfo.id = data["id"]?.string ?? "0"
                        memberInfo.name = data["name"]?.string ?? ""
                        memberInfo.c_code = data["c_code"]?.string ?? ""
                        memberInfo.mobile = data["mobile"]?.string ?? ""
                        memberInfo.user_type = data["user_type"]?.string ?? ""
                        memberInfo.active = data["active"]?.string ?? ""
                        memberInfo.added_on = data["added_on"]?.string ?? ""
                        let isSave = DatabaseManager.getInstance().saveMemberInfo(memberInfo)
                        if isSave == true{
                            if data["home"] != nil{
                                if let homeIcons = data["home"]!.array{
                                    print("home true")
                                    for homeIcon in homeIcons{
                                        guard let homeIcon = homeIcon.dictionary else {return}
                                        let icon  = HomeIconModel()
                                        
                                        icon.id = homeIcon["id"]?.string ?? "0"
                                        icon.member_type = memberInfo.user_type
                                        icon.feature = homeIcon["feature"]?.string ?? ""
                                        icon.icon_path = homeIcon["icon_path"]?.string ?? ""
                                        icon.active = homeIcon["active"]?.string ?? ""
                                        icon.sort = homeIcon["sort"]?.string ?? ""
                                        if(DatabaseManager.getInstance().saveHomeIconList(icon) == true){
                                            print("save home true")
                                        }else{
                                            print("save home false")
                                        }
                                    }
                                }
                            }
                            if data["students"] != nil{
                                if let studetns = data["students"]!.array{
                                    for studetn in studetns{
                                        guard let studetn = studetn.dictionary else {return}
                                        let student  = StudentModel()
                                        student.id = studetn["id"]?.string ?? "0"
                                        student.first_name = studetn["first_name"]?.string ?? ""
                                        student.last_name = studetn["last_name"]?.string ?? ""
                                        student.birthdate = studetn["birthdate"]?.string ?? ""
                                        student.emergency_number = studetn["emergency_number"]?.string ?? ""
                                        student.emergency_number2 = studetn["emergency_number2"]?.string ?? ""
                                        student.class_id = studetn["class_id"]?.string ?? ""
                                        student.school_id = studetn["school_id"]?.string ?? ""
                                        student.active = studetn["active"]?.string ?? ""
                                        student.student_photo = studetn["student_photo"]?.string ?? ""
                                        student.school_name = studetn["school_name"]?.string ?? ""
                                        student.school_logo = studetn["school_logo"]?.string ?? ""
                                        student.class_name = studetn["class_name"]?.string ?? ""
                                        
                                        if(DatabaseManager.getInstance().saveStudent(student) == true){
                                            print("save student true")
                                        }else{
                                            print("save student false")
                                        }
                                    }
                                }
                            }
                            if data["schools"] != nil{
                                if let schools = data["schools"]!.array{
                                    for school in schools{
                                        guard let school = school.dictionary else {return}
                                        let sch  = SchoolsModel()
                                        sch.id = school["id"]?.string ?? "0"
                                        sch.member_type = memberInfo.user_type
                                        sch.name = school["name"]?.string ?? ""
                                        sch.active = school["active"]?.string ?? ""
                                        sch.added_on = school["added_on"]?.string ?? ""
                                        sch.added_by = school["added_by"]?.string ?? ""
                                        sch.phone1 = school["phone1"]?.string ?? ""
                                        sch.phone2 = school["phone2"]?.string ?? ""
                                        sch.email = school["email"]?.string ?? ""
                                        sch.facebook = school["facebook"]?.string ?? ""
                                        sch.teacher_add_news = school["teacher_add_news"]?.string ?? ""
                                        sch.logo = school["logo"]?.string ?? ""
                                        sch.user_id = school["user_id"]?.string ?? ""
                                        if(DatabaseManager.getInstance().saveSchool(sch) == true){
                                            print("save school true")
                                        }else{
                                            print("save school false")
                                        }
                                    }
                                }
                            }
                                
                                
                            
                        }
                    }
                    
                   
                    
                    
                    completion(true, json["msg"].string!)
                }
                
                completion(true, "")
            }
            
        }
    }
}
