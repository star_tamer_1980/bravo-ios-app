//
//  schoolTeacherAdapter.swift
//  School
//
//  Created by ISCA-IOS on 7/9/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class schoolTeacherAdapter: UIViewController {

    let celTable = "cellTable"
    let celHeight: CGFloat = 130.0
    @IBOutlet weak var tblKids: UITableView!
    var Schools = [SchoolsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblKids.tableFooterView = UIView()
        self.tblKids.separatorInset = .zero
        self.tblKids.contentInset = .zero
        self.tblKids.separatorStyle = .none
        self.tblKids.register(UINib.init(nibName: "ListSchoolNib", bundle: nil), forCellReuseIdentifier: celTable)
        self.tblKids.dataSource = self
        self.tblKids.delegate = self
    }
    
    
    func getData(){
        DatabaseManager.getInstance().getSchoolList("2") { (haveSchools: Bool, schools: [SchoolsModel]) in
            if haveSchools == true{
                self.Schools = schools
                self.tblKids.reloadData()
            }
        }
    }
    
    
    
}


extension schoolTeacherAdapter: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Schools.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: celTable, for: indexPath) as! ListSchoolNib
        cell.cellConfiguration(schoolName: Schools[indexPath.row].name, schoolImage: Schools[indexPath.row].logo)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        KeyValueShare.setSchoolId(Schools[indexPath.row].id)
        let storyboard = UIStoryboard(name: "HomeTeacher", bundle: nil)
        let controller = (storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController)
        self.present(controller, animated: true)
    }
}

extension schoolTeacherAdapter: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return celHeight
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: 10))
        return view
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}
