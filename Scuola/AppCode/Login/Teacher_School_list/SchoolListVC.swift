//
//  SchoolListVC.swift
//  School
//
//  Created by ISCA-IOS on 7/3/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class SchoolListVC: schoolTeacherAdapter {

    override func viewDidLoad() {
        super.viewDidLoad()

        getData()
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
