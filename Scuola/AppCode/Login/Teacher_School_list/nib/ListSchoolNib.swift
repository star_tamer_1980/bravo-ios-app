//
//  ListSchoolNib.swift
//  School
//
//  Created by ISCA-IOS on 7/9/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class ListSchoolNib: UITableViewCell {
    @IBOutlet weak var lblSchoolName: Green50Label!
    @IBOutlet weak var imgSchool: UIImageView!
    @IBOutlet weak var lblTeacherName: Label!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblSchoolName.textColor = GlobalColor.APP.blue
        lblTeacherName.textColor = GlobalColor.APP.blue
    }
    
    func cellConfiguration(schoolName: String, schoolImage: String){
        self.lblSchoolName.text = schoolName
        self.lblTeacherName.text = DatabaseManager.getInstance().getAnyMemberInfo().name
        let url = URL(string: schoolImage)
        self.imgSchool.kf.setImage(with: url)
    }
}
