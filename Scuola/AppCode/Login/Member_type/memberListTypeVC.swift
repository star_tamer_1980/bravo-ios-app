//
//  member_type_VC.swift
//  School
//
//  Created by ISCA-IOS on 7/3/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class memberListTypeVC: UIViewController {

    @IBOutlet weak var viParent: radiousView!
    @IBOutlet weak var viParentConstraint: NSLayoutConstraint!
    @IBOutlet weak var viTeacher: radiousView!
    @IBOutlet weak var viTeacherConstraint: NSLayoutConstraint!
    @IBOutlet weak var viManagement: radiousView!
    @IBOutlet weak var viManagementConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viTitle: UIView!
    @IBOutlet weak var titleVC: Label!
    override func viewDidLoad() {
        super.viewDidLoad()
        viTitle.backgroundColor = GlobalColor.APP.blue
        //viTeacherConstraint.constant = 0
        DatabaseManager.getInstance().getTypeOfMember { (parent: Bool, teacher: Bool, management: Bool) in
            if parent == false{
                self.viParentConstraint.constant = 0
            }else{
                self.viParentConstraint.constant = 70
            }
            if teacher == false{
                self.viTeacherConstraint.constant = 0
            }else{
                self.viTeacherConstraint.constant = 70
            }
            if management == false{
                self.viManagementConstraint.constant = 0
            }else{
                self.viManagementConstraint.constant = 70
            }
            
        }
        
        let parentGesture = UITapGestureRecognizer(target: self, action: #selector(self.parentAction))
        viParent.addGestureRecognizer(parentGesture)
        let teacherGesture = UITapGestureRecognizer(target: self, action: #selector(self.teacherAction))
        viTeacher.addGestureRecognizer(teacherGesture)
        let managementGesture = UITapGestureRecognizer(target: self, action: #selector(self.managmentAction))
        viManagement.addGestureRecognizer(managementGesture)
    }
    
    @objc func parentAction(sender: UITapGestureRecognizer){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "Student_list_VC") as! Student_list_VC
        self.present(myController, animated: true)
    }
    @objc func teacherAction(sender: UITapGestureRecognizer){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "SchoolListVC") as! SchoolListVC
        self.present(myController, animated: true)
    }

    @objc func managmentAction(sender: UITapGestureRecognizer){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let myController = storyboard.instantiateViewController(withIdentifier: "ManageMentSchoolListVC") as! ManageMentSchoolListVC
        self.present(myController, animated: true)
    }
    

}
