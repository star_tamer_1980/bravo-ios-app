//
//  attachmentModel.swift
//  School
//
//  Created by ISCA-IOS on 7/22/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

class attachmentModel{
    /*
     "id": "89",
     "feature_type_id": "1",
     "feature_id": "87",
     "file_name": "0prJPEG_1562766326081_1562838259.jpg",
     "school_id": "1",
     "added_on": "2019-07-11 11:44:19",
     "parent_request": "0",
     "extension": "jpg",
     "file_path": "http:\/\/scuola.isca-group.com\/files\/0prJPEG_1562766326081_1562838259.jpg"
 */
    var id: String = ""
    var feature_type_id: String = ""
    var feature_id: String = ""
    var file_name: String = ""
    var school_id: String = ""
    var added_on: String = ""
    var parent_request: String = ""
    var extension_file: String = ""
    var file_path: String = ""
    
}
