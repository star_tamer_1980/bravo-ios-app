//
//  DailyPlanMangementVC.swift
//  School
//
//  Created by ISCA-IOS on 8/19/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class DailyPlanManagementVC: DailyPlanManagementAdapter {
    
    @IBOutlet weak var viTitle: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viTitle.backgroundColor = GlobalColor.APP.blue
        
        let tapGeusture = UITapGestureRecognizer(target: self, action: #selector(tapTitleGeusture(_:)))
        viTitle.addGestureRecognizer(tapGeusture)
        self.getData()
    }
    
    @objc func tapTitleGeusture(_ tapGesutre: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
