//
//  planModel.swift
//  School
//
//  Created by ISCA-IOS on 8/18/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class planModel{
    var plan_id: String = ""
    var schedule_id: String = ""
    var day_name: String = ""
    var subject_id: String = ""
    var subject_name: String = ""
    var class_id: String = ""
    var class_name: String = ""
    var teacher_user_id: String = ""
    var teacher_name: String = ""
    var plan_date: String = ""
    var plan: String = ""
    var updated_on: String = ""
    var attach = [attachmentModel]()
}

