//
//  datePickerModel.swift
//  School
//
//  Created by ISCA-IOS on 8/8/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

class datePickerModel: NSObject {
    var fullDate: String = ""
    var yearDate: String = ""
    var monthDate: String = ""
    var monthNameDate: String = ""
    var weekNumber: String = ""
    var dayNameDate: String = ""
    var dayNumberDate: String = ""
    var isActive: Bool = false
    
}
