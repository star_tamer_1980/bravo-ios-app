//
//  nibPlan.swift
//  School
//
//  Created by ISCA-IOS on 8/18/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class nibPlan: UITableViewCell {
    @IBOutlet weak var imgAttach: UIImageView!
    @IBOutlet weak var lblSubjectType: UILabel!
    @IBOutlet weak var lblSubjectName: UILabel!
    @IBOutlet weak var txtSubjectDetails: UITextView!
    @IBOutlet weak var viContainer: radiousView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func configureCell(name: String, category: String, details: String, plan_id: String, hasAttach: Bool){
        if plan_id == ""{
            self.viContainer.backgroundColor = GlobalColor.APP.gray10
            self.lblSubjectName.textColor = UIColor.black
            self.lblSubjectType.textColor = UIColor.black
        }else{
            self.viContainer.backgroundColor = GlobalColor.APP.blue
            self.lblSubjectName.textColor = GlobalColor.APP.white
            self.lblSubjectType.textColor = GlobalColor.APP.white
        }
        self.lblSubjectName.text = name
        self.lblSubjectType.text = category
        self.txtSubjectDetails.text = details
        if hasAttach == true{
            self.imgAttach.isHidden = false
        }else{
            self.imgAttach.isHidden = true
        }
    }
    
}
