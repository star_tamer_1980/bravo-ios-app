//
//  datePickerNib.swift
//  School
//
//  Created by ISCA-IOS on 8/8/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class datePickerNib: UICollectionViewCell {

@IBOutlet weak var lblDayName: UILabel!
    @IBOutlet weak var lblDateMonth: UILabel!
    var isCurrnet: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(dayName: String, dayNumber: String, monthNumber: String, isCurrent: Bool){
        self.lblDayName.text = dayName
        self.lblDateMonth.text = dayNumber + "-" + monthNumber
        if isCurrent == true{
            self.lblDayName.textColor = GlobalColor.APP.green
            self.lblDateMonth.textColor = GlobalColor.APP.green
        }else{
            self.lblDayName.textColor = GlobalColor.APP.gray
            self.lblDateMonth.textColor = GlobalColor.APP.gray
        }
    }
}
