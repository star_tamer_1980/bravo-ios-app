//
//  DailyPlanModifiVC.swift
//  School
//
//  Created by ISCA-IOS on 8/19/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import TTGSnackbar

class DailyPlanModifiVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var indiLoad: UIActivityIndicatorView!
    @IBOutlet weak var tvPlan: editTextView!
    @IBOutlet weak var btnSaveHandler: Blue!
    @IBOutlet weak var lblSubjectDate: Blue9Label!
    @IBOutlet weak var lblSubjectName: Blue9Label!
    @IBOutlet weak var lblClassName: Blue9LeftLabel!
    @IBOutlet weak var imgDismiss: UIImageView!
    @IBOutlet weak var viTitle: UIView!
    @IBOutlet weak var btnConstrainBottom: NSLayoutConstraint!
    
    var planData = planModel()
    var planCurrentDate: String = ""
    
    func textViewDidChange(_ textView: UITextView) {
        if tvPlan.text.count != 0{
            btnSaveHandler.isEnabled = true
        }else{
            btnSaveHandler.isEnabled = false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tvPlan.delegate = self
        viTitle.backgroundColor = GlobalColor.APP.blue
        let tapDismiss = UITapGestureRecognizer(target: self, action: #selector(dismissVC(_:)))
        imgDismiss.addGestureRecognizer(tapDismiss)
        self.lblClassName.text = planData.class_name
        self.lblSubjectName.text = planData.subject_name
        if planData.plan_id != ""{
            self.lblSubjectDate.text = planData.plan_date
            self.tvPlan.text = planData.plan
        }else{
            self.lblSubjectDate.text = planCurrentDate
        }
        
        
        
        if tvPlan.text.count != 0{
            btnSaveHandler.isEnabled = true
        }
    }
    @objc func dismissVC(_ tapGesture: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSaveAction(_ sender: Any) {
        self.indiLoad.startAnimating()
        self.btnSaveHandler.isEnabled = false
        if planData.plan_id == ""{
            API.planAdd(teacher_id: DatabaseManager.getInstance().getTeacherInfo().id, schedule_id: planData.schedule_id, plan: tvPlan.text!, dateAdd: planCurrentDate) { (result: Bool, msg: String) in
                TTGSnackbar.init(message: msg, duration: .long).show()
                if result == true{
                    self.dismiss(animated: true, completion: nil)
                }
                self.indiLoad.stopAnimating()
                self.btnSaveHandler.isEnabled = true
            }
        }else{
            API.planEdit(plan_id: planData.plan_id, plan: tvPlan.text!) { (result: Bool, msg: String) in
                TTGSnackbar.init(message: msg, duration: .long).show()
                if result == true{
                    self.dismiss(animated: true, completion: nil)
                }
                self.indiLoad.stopAnimating()
                self.btnSaveHandler.isEnabled = true
            }
        }
        
    }
}
