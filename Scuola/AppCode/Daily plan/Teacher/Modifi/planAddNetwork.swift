//
//  planAddNetwork.swift
//  School
//
//  Created by ISCA-IOS on 8/19/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func planAdd(teacher_id: String, schedule_id: String, plan: String, dateAdd: String, _ completion: @escaping(_ result: Bool, _ msg: String) -> Void){
        let url = URLs.planAdd
        let parameters = [
            "teacher_id": teacher_id,
            "schedule_id": schedule_id,
            "plan": plan,
            "plan_date": dateAdd
        ]
        print("teacher_id \(teacher_id)")
        print("schedule_id \(schedule_id)")
        print("plan \(plan)")
        print("dateadd \(dateAdd)")
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let error):
                print(error.localizedDescription)
                completion(false, error.localizedDescription)
            case .success(let value):
                print(value)
                let json = JSON(value)
                completion(json["status"].bool!, json["msg"].string!)
            }
        }
    }
}
