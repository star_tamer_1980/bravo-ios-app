//
//  DailyPlanListTeacherNetwork.swift
//  School
//
//  Created by ISCA-IOS on 8/19/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//


import Foundation
import Alamofire
import SwiftyJSON

extension API{
    class func dailyPlanParentListTeacher(teacher_id: String, school_id: String, plan_date: String, _ complition: @escaping(_ result: Bool?, _ msg: String?, _ planData: [planModel]?) -> Void){
        let url = URLs.planTeacherList
        let parameters = [
            "teacher_id": teacher_id,
            "school_id": school_id,
            "plan_date": plan_date
        ]
        print("teacher_id \(teacher_id)")
        print("school_id \(school_id)")
        print("plan date \(plan_date)")
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result {
            case .failure(let error):
                complition(false, error.localizedDescription, nil)
            case .success(let value):
                let json = JSON(value)
                print(json)
                if json["status"].bool! == false{
                    print("test not found data")
                    complition(false, json["msg"].string!, nil)
                }else{
                    print("test found data")
                    guard let resultArr = json["plan"].array else{
                        complition(false, json["msg"].string!, nil)
                        return
                    }
                    print("result count is \(resultArr.count)")
                    if resultArr.count == 0{
                        complition(false, json["msg"].string!, nil)
                        return
                        //DatabaseManager.getInstance().remove_Parent_request_data("1", isClosed, studentId)
                    }
                    var planList = [planModel]()
                    for data in resultArr{
                        guard let data = data.dictionary else {return}
                        let plan = planModel()
                        plan.plan_id = data["plan_id"]?.string ?? "0"
                        plan.plan_date = data["plan_date"]?.string ?? ""
                        plan.plan = data["plan"]?.string ?? ""
                        plan.schedule_id = data["schedule_id"]?.string ?? ""
                        plan.day_name = data["day_name"]?.string ?? ""
                        plan.subject_id = data["subject_id"]?.string ?? ""
                        plan.subject_name = data["subject_name"]?.string ?? ""
                        print("subject_name is \(plan.subject_name)")
                        plan.class_id = data["class_id"]?.string ?? ""
                        plan.class_name = data["class_name"]?.string ?? ""
                        plan.teacher_user_id = data["teacher_user_id"]?.string ?? ""
                        plan.teacher_name = data["teacher_name"]?.string ?? ""
                        plan.updated_on = data["updated_on"]?.string ?? ""
                        
                        var filesArray = [attachmentModel]()
                        if data["attach"] != nil{
                            if let attach_files = data["attach"]!.array{
                                for allFiles in attach_files{
                                    guard let file = allFiles.dictionary else {return}
                                    let fileModule = attachmentModel()
                                    fileModule.id = file["id"]?.string ?? "0"
                                    fileModule.feature_id = file["feature_id"]?.string ?? ""
                                    fileModule.feature_type_id = file["feature_type_id"]?.string ?? ""
                                    fileModule.file_name = file["file_name"]?.string ?? ""
                                    fileModule.school_id = file["school_id"]?.string ?? ""
                                    fileModule.added_on = file["added_on"]?.string ?? ""
                                    fileModule.parent_request = file["parent_request"]?.string ?? ""
                                    fileModule.extension_file = file["extension"]?.string ?? ""
                                    fileModule.file_path = file["file_path"]?.string ?? ""
                                    DatabaseManager.getInstance().save_Parent_Attachment_Info(fileModule)
                                    filesArray.append(fileModule)
                                }
                                plan.attach = filesArray
                            }
                        }
                        //DatabaseManager.getInstance().save_Parent_parent_request_Info(plan)
                        planList.append(plan)
                        
                    }
                    complition(true, "found data", planList)
                    
                }
            }
            
        }
    }
}

