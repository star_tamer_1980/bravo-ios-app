//
//  planDelete.swift
//  School
//
//  Created by ISCA-IOS on 8/19/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

extension API{
    class func planDelete(plan_id: String, _ completion: @escaping(_ result: Bool, _ msg: String) -> Void){
        let url = URLs.planDelete
        let parameters = [
            "plan_id": plan_id
        ]
        print("plan_id \(plan_id)")
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON{ response in
            switch response.result{
            case .failure(let error):
                print(error.localizedDescription)
                completion(false, error.localizedDescription)
            case .success(let value):
                print(value)
                let json = JSON(value)
                completion(json["status"].bool!, json["msg"].string!)
            }
        }
    }
}

