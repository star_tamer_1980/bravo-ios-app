//
//  DailyPlanAdapter.swift
//  School
//
//  Created by ISCA-IOS on 8/8/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
import TTGSnackbar

class DailyPlanAdapter: UIViewController{
    
    @IBOutlet weak var lblNoDataFound: Blue12Label!
    @IBOutlet weak var indiLoading: UIActivityIndicatorView!
    @IBOutlet weak var viNoDataFound: UIView!
    @IBOutlet weak var tblDailyPlan: UITableView!
    @IBOutlet weak var cvDatePicker: UICollectionView!
    //let celTable = "celTable"
    let celCollection = "datePickerNib"
    let celTable = "nibPlan"
    let celHeight: CGFloat = 65.0
    var datePickerList = [datePickerModel]()
    var planList = [planModel]()
    var dateNow: String = String(describing: DateUtil.getCustomDate().year!) + "-" + String(describing: DateUtil.getCustomDate().month!) + "-" + String(describing: DateUtil.getCustomDate().day!)
    var CurrentDate: String = DateUtil.getCurrentDate()
    var selectedIndexTab: Int = 300
    var studentInfo = StudentModel()
//    lazy var refresher: UIRefreshControl = {
//        let refresher = UIRefreshControl()
//        refresher.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
//        return refresher
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        studentInfo = DatabaseManager.getInstance().getStudentInfo(idStudent: KeyValueShare.getStudentId())
        self.tblDailyPlan.tableFooterView = UIView()
        self.tblDailyPlan.separatorInset = .zero
        self.tblDailyPlan.contentInset = .zero
        self.tblDailyPlan.separatorStyle = .none
        tblDailyPlan.register(UINib.init(nibName: celTable, bundle: nil), forCellReuseIdentifier: celTable)
        tblDailyPlan.dataSource = self
        tblDailyPlan.delegate = self
        
        cvDatePicker.backgroundColor = .clear
        cvDatePicker.register(UINib.init(nibName: celCollection, bundle: nil), forCellWithReuseIdentifier: celCollection)
        
        cvDatePicker.dataSource = self
        cvDatePicker.delegate = self
        self.fillDateModule(From: -300, To: 2)
        cvDatePicker.reloadData()
    }
    
    func fillDateModule(From: Int, To: Int){
        for i in From...To{
            let calField = datePickerModel()
            calField.yearDate = DateUtil.getNextDayMonth(vDate: i).year
            calField.monthDate = DateUtil.getNextDayMonth(vDate: i).month
            calField.monthNameDate = DateUtil.getNextDayMonth(vDate: i).monthName
            calField.dayNumberDate = DateUtil.getNextDayMonth(vDate: i).dayDate
            calField.dayNameDate = DateUtil.getNextDayMonth(vDate: i).dayWeek
            calField.fullDate = DateUtil.getNextDayMonth(vDate: i).year + "-"
                + DateUtil.getNextDayMonth(vDate: i).month + "-"
                + DateUtil.getNextDayMonth(vDate: i).dayDate
            datePickerList.append(calField)
        }
        self.cvDatePicker.reloadData()
    }
    func getData(){
        getDateForKid(dayMonth: CurrentDate)
        self.cvDatePicker.selectItem(at: [0, 302], animated: true, scrollPosition: .centeredHorizontally)
    }
    
    func getDateForKid(dayMonth: String){
        print("connection \(Reachability.isConnectedToInternet())")
        if Reachability.isConnectedToInternet() == false{
            print("connection status false")
            self.indiLoading.stopAnimating()
            self.lblNoDataFound.text = Alerts.internet.connection.notFound.description
            self.viNoDataFound.isHidden = false
            return
        }
        print("connection status true")
        self.indiLoading.isHidden = false
        self.viNoDataFound.isHidden = true
        self.CurrentDate = dayMonth
        API.dailyPlanParentList(class_id: studentInfo.class_id, school_id: studentInfo.school_id, plan_date: self.CurrentDate) { (result: Bool?, msg: String?, plan: [planModel]?) in
            self.indiLoading.isHidden = true
            if result == true{
                self.viNoDataFound.isHidden = true
                self.planList = plan!
                self.tblDailyPlan.reloadData()
            }else{
                self.planList.removeAll()
                self.tblDailyPlan.reloadData()
                self.lblNoDataFound.text = msg
                self.viNoDataFound.isHidden = false
            }
        }
        
    }
    
}
extension DailyPlanAdapter: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datePickerList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: celCollection, for: indexPath) as? datePickerNib else{ return UICollectionViewCell()}
        
        
        
        print("selectedIndexTab after  is: \(selectedIndexTab) Index path after is: \(indexPath.row)")
        
        if indexPath.row == selectedIndexTab{
            cell.configureCell(dayName: datePickerList[indexPath.row].dayNameDate, dayNumber: datePickerList[indexPath.row].dayNumberDate, monthNumber: datePickerList[indexPath.row].monthDate, isCurrent: true)
            print("selectedIndexTab is: \(selectedIndexTab) Index path is: \(indexPath.row)")
            cvDatePicker.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.view.layoutIfNeeded()
        }else{
            cell.configureCell(dayName: datePickerList[indexPath.row].dayNameDate, dayNumber: datePickerList[indexPath.row].dayNumberDate, monthNumber: datePickerList[indexPath.row].monthDate, isCurrent: false)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row <= 300{
            selectedIndexTab = indexPath.row
            self.getDateForKid(dayMonth: datePickerList[indexPath.row].fullDate)
            print("date is \(datePickerList[indexPath.row].fullDate)")
            cvDatePicker.reloadData()
            
        }
        print(indexPath.row)
    }
}
extension DailyPlanAdapter: UICollectionViewDelegate{
    
}


extension DailyPlanAdapter: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.celTable, for: indexPath) as? nibPlan else {return UITableViewCell()}
        if planList[indexPath.row].attach.count == 0{
            cell.configureCell(name: planList[indexPath.row].subject_name, category: planList[indexPath.row].class_name, details: planList[indexPath.row].plan, plan_id: planList[indexPath.row].plan_id, hasAttach: false)
        }else{
            cell.configureCell(name: planList[indexPath.row].subject_name, category: planList[indexPath.row].class_name, details: planList[indexPath.row].plan, plan_id: planList[indexPath.row].plan_id, hasAttach: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}
extension DailyPlanAdapter: UITableViewDelegate{
    
}
