//
//  Blue9LeftLabel.swift
//  School
//
//  Created by ISCA-IOS on 7/22/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//


import UIKit
@IBDesignable

class Blue9LeftLabel: UILabel{
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    private func setup(){
        self.textColor = GlobalColor.APP.blue
        self.font = UIFont(name: "Poppins-Regular", size: 9)
    }
}

