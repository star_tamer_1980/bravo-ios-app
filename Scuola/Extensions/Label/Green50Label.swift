//
//  Green50Label.swift
//  School
//
//  Created by ISCA-IOS on 7/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class Green50Label: Label{
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    private func setup(){
        self.textColor = GlobalColor.APP.blue
        self.backgroundColor = GlobalColor.APP.green50
        self.font = UIFont(name: "Poppins-SemiBold", size: 17)
    }
}
