//
//  SmallLabel.swift
//  School
//
//  Created by ISCA-IOS on 7/7/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class SmallLabel: Label{
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    private func setup(){
        self.font = UIFont(name: "Poppins-Regular", size: 14)
    }
}

