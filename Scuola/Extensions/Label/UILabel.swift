//
//  UILabel.swift
//  School
//
//  Created by ISCA-IOS on 7/4/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class Label: UILabel{
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    private func setup(){
        self.textColor = GlobalColor.APP.white
        self.font = UIFont(name: "Poppins-Regular", size: 17)
        self.textAlignment = NSTextAlignment.center
    }
}
