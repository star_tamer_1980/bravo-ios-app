//
//  editTextView.swift
//  School
//
//  Created by ISCA-IOS on 7/16/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//


import UIKit
@IBDesignable

class editTextView: GrayTextView{
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    private func setup(){
        self.layer.borderColor = GlobalColor.APP.blue.cgColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 5
        //self.backgroundColor = GlobalColor.APP.gray10
    }
}
