//
//  Red.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class RedTextView: UITextView{
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    private func setup(){
        self.textColor = GlobalColor.APP.red
        self.font = UIFont(name: "Poppins-SemiBold", size: 15)
        self.textAlignment = NSTextAlignment.left
    }
}
