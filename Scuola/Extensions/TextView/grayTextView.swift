//
//  grayTextView.swift
//  School
//
//  Created by ISCA-IOS on 7/1/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class GrayTextView: UITextView{
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    private func setup(){
        self.textColor = GlobalColor.APP.gray
        self.font = UIFont(name: "Poppins-Regular", size: 12)
        self.textAlignment = NSTextAlignment.left
    }
}
