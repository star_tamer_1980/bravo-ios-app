//
//  ButtonsGlobal.swift
//  School
//
//  Created by ISCA-IOS on 6/24/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

extension UIButton{
    
    func setGlobalStyle(){
        setBorderRadius()
        setBorderWidth()
        setBorderColor()
        setFont17semiBold()
    }
    
    
    
    private func setBorderColor(){
        self.layer.borderColor = GlobalColor.APP.white.cgColor
    }
    private func setBorderWidth(){
        self.layer.borderWidth = 1.0
    }
    private func setBorderRadius(){
        self.layer.cornerRadius = 10.0
    }
    
    func setFont17semiBold() {
        self.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 17)
    }
    
    func setLeftImage(imageName: UIImage, renderMode: UIImage.RenderingMode){
        self.setImage(imageName.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: imageName.size.width)
        self.contentHorizontalAlignment = .left
        self.imageView?.contentMode = .scaleAspectFit
    }
    
    func setRightImage(imageName: UIImage, renderMode: UIImage.RenderingMode){
        self.setImage(imageName.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: imageName.size.width / 2, bottom: 0, right: 0)
        self.contentHorizontalAlignment = .right
        self.imageView?.contentMode = .scaleAspectFit
    }
    
}
