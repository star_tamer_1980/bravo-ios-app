//
//  Gray.swift
//  School
//
//  Created by ISCA-IOS on 6/30/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class Gray: UIButton{
    open override func draw(_ rect: CGRect) {
        self.layer.backgroundColor = GlobalColor.APP.gray.cgColor
        self.setTitleColor(GlobalColor.APP.white, for: .normal)
        setGlobalStyle()
    }
    
}
