//
//  Red.swift
//  School
//
//  Created by ISCA-IOS on 6/24/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class Red: UIButton{
    open override func draw(_ rect: CGRect) {
        self.layer.backgroundColor = GlobalColor.APP.red.cgColor
        self.setTitleColor(GlobalColor.APP.white, for: .normal)
        setGlobalStyle()
    }
    
}


