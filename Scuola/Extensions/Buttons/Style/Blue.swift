//
//  Blue.swift
//  School
//
//  Created by ISCA-IOS on 6/24/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class Blue: UIButton{
    
    
    
    open override func draw(_ rect: CGRect) {
        
        if self.isEnabled == true{
            self.layer.backgroundColor = GlobalColor.APP.blue.cgColor
            self.setTitleColor(GlobalColor.APP.white, for: .normal)
            setGlobalStyle()
        }else{
            self.layer.backgroundColor = GlobalColor.APP.gray10.cgColor
            self.setTitleColor(GlobalColor.APP.white, for: .disabled)
            setGlobalStyle()
        }
        self.layoutIfNeeded()
    }
    
}
