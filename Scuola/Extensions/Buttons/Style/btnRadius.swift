//
//  btnRadius.swift
//  School
//
//  Created by ISCA-IOS on 8/1/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

class btnRadius: Blue{
    open override func draw(_ rect: CGRect) {
        self.layer.backgroundColor = GlobalColor.APP.blue.cgColor
        self.setTitleColor(GlobalColor.APP.white, for: .normal)
    }
    @IBInspectable var borderColor: UIColor = UIColor.white{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2.0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0.0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}

