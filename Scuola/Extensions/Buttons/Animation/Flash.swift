//
//  Flash.swift
//  School
//
//  Created by ISCA-IOS on 6/26/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    func flash(){
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration      = 0.4
        flash.fromValue     = 1
        flash.toValue       = 0.5
        flash.autoreverses  = true
        CALayer().add(flash, forKey: nil)
    }
}
