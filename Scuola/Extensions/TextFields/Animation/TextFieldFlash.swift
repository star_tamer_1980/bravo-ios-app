//
//  TextFieldFlash.swift
//  School
//
//  Created by ISCA-IOS on 6/26/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func flash(){
        
    }
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: .allowUserInteraction, animations: {
            self.backgroundColor = GlobalColor.Basic.textFieldActive
        }, completion: nil)
        return super.touchesBegan(touches, with: event)
    }
}

