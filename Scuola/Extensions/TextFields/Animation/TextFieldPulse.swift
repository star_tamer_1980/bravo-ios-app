//
//  TextFieldPulse.swift
//  School
//
//  Created by ISCA-IOS on 6/26/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func pulsete(){
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration          = 0.6
        pulse.fromValue         = 0.95
        pulse.toValue           = 1.0
        //pulse.autoreverses      = true
        //pulse.repeatCount       = 1
        pulse.initialVelocity   = 0.5
        pulse.damping           = 1.0
        layer.add(pulse, forKey: nil)
    }
}
