//
//  EditTextExtension.swift
//  School
//
//  Created by ISCA-IOS on 6/23/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit
@IBDesignable

extension UITextField{
    
    func setStyle(){
        self.setTextColor()
        self.setPlaceholderColor()
        self.borderColor()
        self.setBackgroundColor()
        self.cornerRadius()
        self.setRightPaddingPoints(5)
        self.setLeftPaddingPoints(5)
    }
    
    // set text color
    private func setTextColor(){
        self.textColor = GlobalColor.APP.blue
    }
    
    // set background color
    private func setBackgroundColor(){
        self.layer.backgroundColor = GlobalColor.APP.white.cgColor
    }
    
    // set placeholder color
    private func setPlaceholderColor(){
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : GlobalColor.APP.metalic])
    }
    
    //set bordercolor
    private func borderColor(){
        self.layer.borderColor = GlobalColor.APP.blue.cgColor
        self.layer.borderWidth = 1.0
    }
    
    //set cornerRadius
    private func cornerRadius(){
        self.layoutIfNeeded()
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
    }
    private func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    private func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
