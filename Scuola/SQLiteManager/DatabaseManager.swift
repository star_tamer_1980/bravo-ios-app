//
//  DatabaseManager.swift
//  School
//
//  Created by ISCA-IOS on 7/2/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
var shareInstance = DatabaseManager()
class DatabaseManager: NSObject{
    var database: FMDatabase? = nil
    class func getInstance() -> DatabaseManager{
        if shareInstance.database == nil{
            shareInstance.database = FMDatabase(path: SQLiteConnection.getPath("login_db.db"))
        }
        return shareInstance
    }
    
}

