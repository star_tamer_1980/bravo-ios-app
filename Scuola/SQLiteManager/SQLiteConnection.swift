//
//  BaseSQL.swift
//  School
//
//  Created by ISCA-IOS on 7/1/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import UIKit

class SQLiteConnection: NSObject{
    
    class func getPath(_ fileName: String) -> String{
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileUrl = documentDirectory.appendingPathComponent(fileName)
        print("path url \(fileUrl.path)")
        return fileUrl.path
    }
    
    class func getDatabase(_ filename: String){
        let dbPath = getPath("login_db.db")
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath){
            
            let bundle = Bundle.main.resourceURL
            let file = bundle?.appendingPathComponent(filename)
            var error: NSError?
            do{
                try fileManager.copyItem(atPath: (file?.path)!, toPath: dbPath)
            }catch let error1 as NSError{
                error = error1
            }
            
            if error == nil{
                print("error in db \(error?.localizedDescription)")
            }else{
                print("Yeah !!")
            }
        }
    }
    
}
