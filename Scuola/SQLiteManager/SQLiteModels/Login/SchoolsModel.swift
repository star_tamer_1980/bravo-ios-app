//
//  SchoolsModel.swift
//  School
//
//  Created by ISCA-IOS on 7/3/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

class SchoolsModel: NSObject{
   
    var id: String = ""
    var name: String = ""
    var member_type: String = ""
    var active: String = ""
    var added_on: String = ""
    var added_by: String = ""
    var phone1: String = ""
    var phone2: String = ""
    var email: String = ""
    var facebook: String = ""
    var teacher_add_news: String = ""
    var logo: String = ""
    var user_id: String = ""
}
