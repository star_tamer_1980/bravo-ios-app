//
//  HomeIconModel.swift
//  School
//
//  Created by ISCA-IOS on 7/2/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
class HomeIconModel: NSObject{
    var id: String = ""
    var member_type: String = ""
    var feature: String = ""
    var icon_path: String = ""
    var active: String = ""
    var sort: String = ""
}
