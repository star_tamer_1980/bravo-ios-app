//
//  studentModel.swift
//  School
//
//  Created by ISCA-IOS on 7/2/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

class StudentModel: NSObject{
    var id: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var birthdate: String = ""
    var emergency_number: String = ""
    var emergency_number2: String = ""
    var class_id: String = ""
    var school_id: String = ""
    var active: String = ""
    var student_photo: String = ""
    var school_name: String = ""
    var school_logo: String = ""
    var class_name: String = ""
}
