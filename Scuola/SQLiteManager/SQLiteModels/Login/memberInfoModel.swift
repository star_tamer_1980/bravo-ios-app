//
//  memberInfoModel.swift
//  School
//
//  Created by ISCA-IOS on 7/2/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

class memberInfoModel: NSObject {
    var id          : String = ""
    var name        : String = ""
    var c_code      : String = ""
    var mobile      : String = ""
    var user_type   : String = ""
    var active      : String = ""
    var added_on    : String = ""
    
}
