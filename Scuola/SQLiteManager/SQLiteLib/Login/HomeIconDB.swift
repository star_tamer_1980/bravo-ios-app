//
//  HomeIconListDB.swift
//  School
//
//  Created by ISCA-IOS on 7/2/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension DatabaseManager{
    
    
    func createHomeIconTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS 'home' (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `member_type` TEXT, `idFeature` INTEGER, `feature` TEXT, `icon_path` TEXT, `active` INTEGER, `sort` TEXT)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func deleteHomeIconTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("DROP TABLE IF EXISTS home", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func saveHomeIconList(_ homeData: HomeIconModel) -> Bool{
        shareInstance.database?.open()
        
        let isSave = shareInstance.database?.executeUpdate("Insert into home(idFeature, member_type, feature, icon_path, active, sort) values (?,?,?,?,?,?)", withArgumentsIn: [homeData.id, homeData.member_type, homeData.feature, homeData.icon_path, homeData.active, homeData.sort])
        print("error message is \(shareInstance.database?.lastErrorMessage())")
        shareInstance.database?.close()
        return isSave!
    }
    
    func getHomeIconList(_ member_type: String) -> [HomeIconModel]{
        shareInstance.database?.open()
        var HomeIconList = [HomeIconModel]()
        let query = try! shareInstance.database?.executeQuery("select * from home where member_type = \(member_type) order by sort asc", values: nil)
        while (query?.next())! {
            let HomeIconD = HomeIconModel()
            
            HomeIconD.id = query?.string(forColumn: "idFeature") ?? "0"
            HomeIconD.member_type = query?.string(forColumn: "member_type") ?? "0"
            HomeIconD.feature = query?.string(forColumn: "feature") ?? ""
            HomeIconD.icon_path = query?.string(forColumn: "icon_path") ?? ""
            HomeIconD.active = query?.string(forColumn: "active") ?? ""
            HomeIconD.sort = query?.string(forColumn: "sort") ?? ""
            
            HomeIconList.append(HomeIconD)
        }
        return HomeIconList
    }
    func deleteHomeIcons() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from home", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        
    }
}
