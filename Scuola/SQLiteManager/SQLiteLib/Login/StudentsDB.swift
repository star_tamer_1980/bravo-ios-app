//
//  StudentsDB.swift
//  School
//
//  Created by ISCA-IOS on 7/2/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension DatabaseManager{
    
    
    func createStudentTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS 'students' (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `idStudent` INTEGER, `first_name` TEXT, `last_name` TEXT, `student_photo` TEXT, `birthdate` TEXT, `active` INTEGER, `emergency_number` TEXT, `emergency_number2` TEXT, `school_id` INTEGER, `school_name` TEXT, `school_logo` TEXT, `class_id` INTEGER, `class_name` TEXT)", values: nil)
            while rs!.next() {
            }
            print(shareInstance.database?.lastErrorMessage())
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func deleteStudentTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("DROP TABLE IF EXISTS students", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    
    func saveStudent(_ studentData: StudentModel) -> Bool{
        shareInstance.database?.open()
        let isSave = shareInstance.database?.executeUpdate("Insert into students(idStudent, first_name, last_name, birthdate, emergency_number, emergency_number2, class_id, school_id, active, student_photo, school_name, school_logo, class_name) values (?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [studentData.id, studentData.first_name, studentData.last_name, studentData.birthdate, studentData.emergency_number, studentData.emergency_number2, studentData.class_id, studentData.school_id, studentData.active, studentData.student_photo, studentData.school_name, studentData.school_logo, studentData.class_name])
        shareInstance.database?.close()
        return isSave!
    }
    
    
    func getStudentInfo(idStudent: String) -> StudentModel{
        shareInstance.database?.open()
        let studentInfo = StudentModel()
        let query = try! shareInstance.database?.executeQuery("select * from students where idStudent = '\(idStudent)' limit 1", values: nil)
        while (query?.next())! {
            studentInfo.id = query?.string(forColumn: "idStudent") ?? "0"
            studentInfo.first_name = query?.string(forColumn: "first_name") ?? ""
            studentInfo.last_name = query?.string(forColumn: "last_name") ?? ""
            studentInfo.birthdate = query?.string(forColumn: "birthdate") ?? ""
            studentInfo.emergency_number = query?.string(forColumn: "emergency_number") ?? ""
            studentInfo.emergency_number2 = query?.string(forColumn: "emergency_number2") ?? ""
            studentInfo.class_id = query?.string(forColumn: "class_id") ?? ""
            studentInfo.school_id = query?.string(forColumn: "school_id") ?? ""
            studentInfo.active = query?.string(forColumn: "active") ?? ""
            studentInfo.student_photo = query?.string(forColumn: "student_photo") ?? ""
            studentInfo.school_name = query?.string(forColumn: "school_name") ?? ""
            studentInfo.school_logo = query?.string(forColumn: "school_logo") ?? ""
            studentInfo.class_name = query?.string(forColumn: "class_name") ?? ""
        }
        return studentInfo
    }
    
    
    
    func getStudentsList(_ complition: @escaping( _ result: Bool, _ students: [StudentModel]) -> Void){
        var haveStudent = false
        shareInstance.database?.open()
        var studentList = [StudentModel]()
        let query = try! shareInstance.database?.executeQuery("select * from students order by first_name asc", values: nil)
        while (query?.next())! {
            haveStudent = true
            let StudentD = StudentModel()
            
            StudentD.id = query?.string(forColumn: "idStudent") ?? "0"
            StudentD.first_name = query?.string(forColumn: "first_name") ?? ""
            StudentD.last_name = query?.string(forColumn: "last_name") ?? ""
            StudentD.birthdate = query?.string(forColumn: "birthdate") ?? ""
            StudentD.emergency_number = query?.string(forColumn: "emergency_number") ?? ""
            StudentD.emergency_number2 = query?.string(forColumn: "emergency_number2") ?? ""
            StudentD.class_id = query?.string(forColumn: "class_id") ?? ""
            StudentD.school_id = query?.string(forColumn: "school_id") ?? ""
            StudentD.active = query?.string(forColumn: "active") ?? ""
            StudentD.student_photo = query?.string(forColumn: "student_photo") ?? ""
            StudentD.school_name = query?.string(forColumn: "school_name") ?? ""
            StudentD.school_logo = query?.string(forColumn: "school_logo") ?? ""
            StudentD.class_name = query?.string(forColumn: "class_name") ?? ""
            
            studentList.append(StudentD)
        }
        complition(haveStudent, studentList)
    }
    
    
    func deleteStudents() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from students", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        return false
    }
}
