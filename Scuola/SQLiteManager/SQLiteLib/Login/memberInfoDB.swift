//
//  memberInfo.swift
//  School
//
//  Created by ISCA-IOS on 7/2/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension DatabaseManager{
    
    func createMemberInfoTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS 'member' ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `idMember` INTEGER NOT NULL, `name` TEXT, `c_code` TEXT, `mobile` TEXT, `user_type` INTEGER, `active` INTEGER, `added_on` TEXT)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func deleteMemberInfoTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("DROP TABLE IF EXISTS member", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func saveMemberInfo(_ memberInfo: memberInfoModel) -> Bool{
        shareInstance.database?.open()
        let isSave = shareInstance.database?.executeUpdate("Insert into member(idMember, name, c_code, mobile, user_type, active, added_on) values (?,?,?,?,?,?,?)", withArgumentsIn: [memberInfo.id, memberInfo.name, memberInfo.c_code, memberInfo.mobile, memberInfo.user_type, memberInfo.active, memberInfo.added_on])
        shareInstance.database?.close()
        return isSave!
    }
    
    func getAnyMemberInfo() -> memberInfoModel{
        shareInstance.database?.open()
        let memberInfoD = memberInfoModel()
        let query = try! shareInstance.database?.executeQuery("select * from member limit 1", values: nil)
        while (query?.next())! {
            memberInfoD.id = query?.string(forColumn: "idMember") ?? ""
            memberInfoD.name = query?.string(forColumn: "name") ?? ""
            memberInfoD.c_code = query?.string(forColumn: "c_code") ?? ""
            memberInfoD.mobile = query?.string(forColumn: "mobile") ?? ""
            memberInfoD.user_type = query?.string(forColumn: "user_type") ?? ""
            memberInfoD.active = query?.string(forColumn: "active") ?? ""
            memberInfoD.added_on = query?.string(forColumn: "added_on") ?? ""
        }
        return memberInfoD
    }
    func getTeacherInfo() -> memberInfoModel{
        shareInstance.database?.open()
        let memberInfoD = memberInfoModel()
        let query = try! shareInstance.database?.executeQuery("select * from member where user_type = 2 limit 1", values: nil)
        while (query?.next())! {
            memberInfoD.id = query?.string(forColumn: "idMember") ?? ""
            memberInfoD.name = query?.string(forColumn: "name") ?? ""
            memberInfoD.c_code = query?.string(forColumn: "c_code") ?? ""
            memberInfoD.mobile = query?.string(forColumn: "mobile") ?? ""
            memberInfoD.user_type = query?.string(forColumn: "user_type") ?? ""
            memberInfoD.active = query?.string(forColumn: "active") ?? ""
            memberInfoD.added_on = query?.string(forColumn: "added_on") ?? ""
        }
        return memberInfoD
    }
    func getManagementInfo() -> memberInfoModel{
        shareInstance.database?.open()
        let memberInfoD = memberInfoModel()
        let query = try! shareInstance.database?.executeQuery("select * from member where user_type = 3 limit 1", values: nil)
        while (query?.next())! {
            memberInfoD.id = query?.string(forColumn: "idMember") ?? ""
            memberInfoD.name = query?.string(forColumn: "name") ?? ""
            memberInfoD.c_code = query?.string(forColumn: "c_code") ?? ""
            memberInfoD.mobile = query?.string(forColumn: "mobile") ?? ""
            memberInfoD.user_type = query?.string(forColumn: "user_type") ?? ""
            memberInfoD.active = query?.string(forColumn: "active") ?? ""
            memberInfoD.added_on = query?.string(forColumn: "added_on") ?? ""
        }
        return memberInfoD
    }
    
    
    
    func getTypeOfMember(_ completion: @escaping(_ parent: Bool, _ teacher: Bool, _ management: Bool) -> Void){
        var parentStatus: Bool = false
        var teacherStatus: Bool = false
        var managementStatus: Bool = false
        shareInstance.database?.open()
        let query = try! shareInstance.database?.executeQuery("select * from member", values: nil)
        while (query?.next())! {
            let user_type = query?.string(forColumn: "user_type") ?? ""
            if(user_type == "1"){
                parentStatus = true
            }
            if(user_type == "2"){
                teacherStatus = true
            }
            if(user_type == "3"){
                managementStatus = true
            }
        }
        completion(parentStatus, teacherStatus, managementStatus)
    }
    
    
    func deleteMemberInfo() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from member", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        
    }
}
