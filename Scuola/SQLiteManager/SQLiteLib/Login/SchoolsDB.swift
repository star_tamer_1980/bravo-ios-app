//
//  SchoolsDB.swift
//  School
//
//  Created by ISCA-IOS on 7/3/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension DatabaseManager{
    
    func createSchoolsTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS 'schools' (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `idSchool` INTEGER, `name` TEXT, `member_type` INTEGER, `active` INTEGER, `added_on` TEXT, `added_by` TEXT, `phone1` TEXT, `phone2` TEXT, `email` TEXT, `facebook` TEXT, `teacher_add_news` INTEGER, `logo` TEXT, `user_id` INTEGER)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func deleteSchoolsTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("DROP TABLE IF EXISTS schools", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    
    func saveSchool(_ SchoolData: SchoolsModel) -> Bool{
        shareInstance.database?.open()
        let isSave = shareInstance.database?.executeUpdate("Insert into schools(idSchool, member_type, name, active, added_on, added_by, phone1, phone2, email, facebook, teacher_add_news, logo, user_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [SchoolData.id, SchoolData.member_type, SchoolData.name, SchoolData.active, SchoolData.added_on, SchoolData.added_by, SchoolData.phone1, SchoolData.phone2, SchoolData.email, SchoolData.facebook, SchoolData.teacher_add_news, SchoolData.logo, SchoolData.user_id])
        shareInstance.database?.close()
        return isSave!
    }
    
    func getSchoolInfo( schooldId: String) -> SchoolsModel{
        shareInstance.database?.open()
        var schoolInfo = SchoolsModel()
        let query = try! shareInstance.database?.executeQuery("select * from schools where idSchool = '\(schooldId)' order by name asc", values: nil)
        while (query?.next())! {
            schoolInfo.id = query?.string(forColumn: "idSchool") ?? "0"
            schoolInfo.name = query?.string(forColumn: "name") ?? ""
            schoolInfo.active = query?.string(forColumn: "active") ?? ""
            schoolInfo.added_on = query?.string(forColumn: "added_on") ?? ""
            schoolInfo.added_by = query?.string(forColumn: "added_by") ?? ""
            schoolInfo.phone1 = query?.string(forColumn: "phone1") ?? ""
            schoolInfo.phone2 = query?.string(forColumn: "phone2") ?? ""
            schoolInfo.email = query?.string(forColumn: "email") ?? ""
            schoolInfo.facebook = query?.string(forColumn: "facebook") ?? ""
            schoolInfo.teacher_add_news = query?.string(forColumn: "teacher_add_news") ?? ""
            schoolInfo.logo = query?.string(forColumn: "logo") ?? ""
            schoolInfo.user_id = query?.string(forColumn: "user_id") ?? ""
            
        }
        return schoolInfo
    }
    
    func getSchoolList(_ memberType: String, _ complition: @escaping(_ haveSchools: Bool, _ schools: [SchoolsModel]) -> Void){
        var haveSchools = false
        shareInstance.database?.open()
        var schoolstList = [SchoolsModel]()
        let query = try! shareInstance.database?.executeQuery("select * from schools where member_type = '\(memberType)' order by name asc", values: nil)
        print("inside school list")
        while (query?.next())! {
            let SchoolD = SchoolsModel()
            print("id school is: \(query?.string(forColumn: "idSchool") ?? "0")")
            haveSchools = true
            SchoolD.id = query?.string(forColumn: "idSchool") ?? "0"
            SchoolD.name = query?.string(forColumn: "name") ?? ""
            SchoolD.active = query?.string(forColumn: "active") ?? ""
            SchoolD.added_on = query?.string(forColumn: "added_on") ?? ""
            SchoolD.added_by = query?.string(forColumn: "added_by") ?? ""
            SchoolD.phone1 = query?.string(forColumn: "phone1") ?? ""
            SchoolD.phone2 = query?.string(forColumn: "phone2") ?? ""
            SchoolD.email = query?.string(forColumn: "email") ?? ""
            SchoolD.facebook = query?.string(forColumn: "facebook") ?? ""
            SchoolD.teacher_add_news = query?.string(forColumn: "teacher_add_news") ?? ""
            SchoolD.logo = query?.string(forColumn: "logo") ?? ""
            SchoolD.user_id = query?.string(forColumn: "user_id") ?? ""
            
            schoolstList.append(SchoolD)
        }
        complition(haveSchools, schoolstList)
    }
    
    
    func deleteSchools() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from students", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        return false
    }
}
