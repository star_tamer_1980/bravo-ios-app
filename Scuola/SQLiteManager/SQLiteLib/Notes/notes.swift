//
//  notes.swift
//  School
//
//  Created by ISCA-IOS on 7/31/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
extension DatabaseManager{
    // type_notes 1 => public, 2 => class, 3 => private
    func create_notes_InfoTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS notes ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `note_id` INTEGER NOT NULL, `category_id` TEXT, `category_name` TEXT, `note` TEXT, `note_type` TEXT, `student_id` TEXT, `stage_id` TEXT, `grade_id` TEXT, `class_id` TEXT, `school_id` INTEGER, `added_by` TEXT, `added_on` TEXT, `note_to` TEXT, `teacher_id` INTEGER NOT NULL)", values: nil)
            while rs!.next() {
                print("create")
            }
            shareInstance.database?.close()
            return true
        } catch let error {
            print("error insertion failed: \(error)")
        }
    }
    
    func save_note(_ note: NotesModel) -> Bool{
        var isSave: Bool = false
            shareInstance.database?.open()
            isSave = (shareInstance.database?.executeUpdate("Insert into notes (note_id, category_id, category_name, note, note_type, student_id, school_id, stage_id, grade_id, class_id, added_by, added_on, note_to, teacher_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [Int(note.id), note.category_id, note.category_name, note.note, note.note_type, note.student_id, Int(note.school_id)!, note.stage_id, note.grade_id, note.class_id, note.added_by, note.added_on, note.note_to, 0]))!
            for attach in note.attach{
                DatabaseManager.getInstance().save_Parent_Attachment_Info(attach)
            }
            shareInstance.database?.close()
        
        return isSave
    }
    
    func save_note_teacher(_ note: NotesModel) -> Bool{
        var isSave: Bool = false
        shareInstance.database?.open()
        isSave = (shareInstance.database?.executeUpdate("Insert into notes (note_id, category_id, category_name, note, note_type, student_id, school_id, stage_id, grade_id, class_id, added_by, added_on, note_to, teacher_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [Int(note.id), note.category_id, note.category_name, note.note, note.note_type, note.student_id, Int(note.school_id)!, note.stage_id, note.grade_id, note.class_id, note.added_by, note.added_on, note.note_to, Int(DatabaseManager.getInstance().getTeacherInfo().id)!]))!
        for attach in note.attach{
            DatabaseManager.getInstance().save_Parent_Attachment_Info(attach)
        }
        shareInstance.database?.close()
        
        return isSave
    }
    
    func save_note_managment(_ note: NotesModel) -> Bool{
        var isSave: Bool = false
        shareInstance.database?.open()
        isSave = (shareInstance.database?.executeUpdate("Insert into notes (note_id, category_id, category_name, note, note_type, student_id, school_id, stage_id, grade_id, class_id, added_by, added_on, note_to, teacher_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [Int(note.id), note.category_id, note.category_name, note.note, note.note_type, note.student_id, Int(note.school_id)!, note.stage_id, note.grade_id, note.class_id, note.added_by, note.added_on, note.note_to, Int(DatabaseManager.getInstance().getManagementInfo().id)!]))!
        for attach in note.attach{
            DatabaseManager.getInstance().save_Parent_Attachment_Info(attach)
        }
        shareInstance.database?.close()
        
        return isSave
    }
    
    func get_note_data(note_id: String) -> NotesModel{
        shareInstance.database?.open()
        let noteData = NotesModel()
        let query = try! shareInstance.database?.executeQuery("select * from notes where `note_id` = \(note_id) limit 1", values: nil)
        while (query?.next())! {
            noteData.id = Int(query?.int(forColumn: "note_id") ?? 0)
            noteData.category_id = query?.string(forColumn: "category_id") ?? ""
            noteData.category_name = query?.string(forColumn: "category_name") ?? ""
            noteData.note = query?.string(forColumn: "note") ?? ""
            noteData.note_type = query?.string(forColumn: "note_type") ?? ""
            noteData.student_id = query?.string(forColumn: "student_id") ?? ""
            noteData.stage_id = query?.string(forColumn: "stage_id") ?? ""
            noteData.grade_id = query?.string(forColumn: "grade_id") ?? ""
            noteData.class_id = query?.string(forColumn: "class_id") ?? ""
            noteData.school_id = String(query?.int(forColumn: "school_id") ?? 0)
            noteData.added_by = query?.string(forColumn: "added_by") ?? ""
            noteData.added_on = query?.string(forColumn: "added_on") ?? ""
            noteData.note_to = query?.string(forColumn: "note_to") ?? ""
            noteData.attach = self.get_parent_request_attach_List(String(noteData.id))
            
        }
        return noteData
    }
    
    
    
    
    func get_notes_List(_ studentId: String, _ noteType: String, _ complition: @escaping(_ haveNotes: Bool, _ notes: [NotesModel]) -> Void){
        var haveNotes = false
        shareInstance.database?.open()
        var notesList = [NotesModel]()
        let query = try! shareInstance.database?.executeQuery("select * from notes where student_id = \(studentId) and note_type = \(noteType) order by note_id desc", values: nil)
        while (query?.next())! {
            let noteData = NotesModel()
            haveNotes = true
            noteData.id = Int(query?.int(forColumn: "note_id") ?? 0)
            noteData.category_id = query?.string(forColumn: "category_id") ?? ""
            noteData.category_name = query?.string(forColumn: "category_name") ?? ""
            noteData.note = query?.string(forColumn: "note") ?? ""
            noteData.note_type = query?.string(forColumn: "note_type") ?? ""
            noteData.student_id = query?.string(forColumn: "student_id") ?? ""
            noteData.stage_id = query?.string(forColumn: "stage_id") ?? ""
            noteData.grade_id = query?.string(forColumn: "grade_id") ?? ""
            noteData.class_id = query?.string(forColumn: "class_id") ?? ""
            noteData.school_id = String(query?.int(forColumn: "school_id") ?? 0)
            noteData.added_by = query?.string(forColumn: "added_by") ?? ""
            noteData.added_on = query?.string(forColumn: "added_on") ?? ""
            noteData.note_to = query?.string(forColumn: "note_to") ?? ""
            noteData.attach = self.get_parent_request_attach_List(String(noteData.id))
            notesList.append(noteData)
        }
        complition(haveNotes, notesList)
    }
    
    
    
    func get_notes_List_members(_ teacherId: String, _ schoolId: String, _ complition: @escaping(_ haveNotes: Bool, _ notes: [NotesModel]) -> Void){
        var haveNotes = false
        shareInstance.database?.open()
        var notesList = [NotesModel]()
        let query = try! shareInstance.database?.executeQuery("select * from notes where teacher_id = \(teacherId) and school_id = \(schoolId) order by note_id desc", values: nil)
        while (query?.next())! {
            let noteData = NotesModel()
            haveNotes = true
            noteData.id = Int(query?.int(forColumn: "note_id") ?? 0)
            noteData.category_id = query?.string(forColumn: "category_id") ?? ""
            noteData.category_name = query?.string(forColumn: "category_name") ?? ""
            noteData.note = query?.string(forColumn: "note") ?? ""
            noteData.note_type = query?.string(forColumn: "note_type") ?? ""
            noteData.student_id = query?.string(forColumn: "student_id") ?? ""
            noteData.stage_id = query?.string(forColumn: "stage_id") ?? ""
            noteData.grade_id = query?.string(forColumn: "grade_id") ?? ""
            noteData.class_id = query?.string(forColumn: "class_id") ?? ""
            noteData.school_id = String(query?.int(forColumn: "school_id") ?? 0)
            noteData.added_by = query?.string(forColumn: "added_by") ?? ""
            noteData.added_on = query?.string(forColumn: "added_on") ?? ""
            noteData.note_to = query?.string(forColumn: "note_to") ?? ""
            noteData.attach = self.get_parent_request_attach_List(String(noteData.id))
            notesList.append(noteData)
        }
        complition(haveNotes, notesList)
    }
    
    
    
    
    func delete_notes_Table() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("DROP TABLE IF EXISTS notes", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        
    }
    
    func remove_notes_data(_ studentId: String, _ noteType: String) -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from notes where note_type = \(noteType) and student_id = \(studentId)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func remove_notes_teacher_data(_ teacherId: String, _ schoolId: String, _ noteType: String) -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from notes where teacher_id = \(teacherId) and school_id = \(schoolId) and note_type = \(noteType)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func remove_notes_management_data(_ schoolId: String, _ noteType: String) -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from notes where school_id = \(schoolId) and note_type = \(noteType)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
}

