//
//  Parent_Parent_Request_Attachment.swift
//  School
//
//  Created by ISCA-IOS on 7/22/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension DatabaseManager{
    
    func create_Parent_Attachment_InfoTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS `parent_parent_request_attach` ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `id_attach` TEXT, `feature_type_id` TEXT, `feature_id` TEXT, `file_name` TEXT, `school_id` TEXT, `added_on` TEXT, `parent_request` TEXT, `extension` TEXT, `file_path` TEXT)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("test 3 failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func save_Parent_Attachment_Info(_ attachments: attachmentModel) -> Bool{
        let if_exist = self.get_Parent_attach_Info(attachId: attachments.id)
        var isSave: Bool = false
        if if_exist.id == ""{
            shareInstance.database?.open()
            isSave = (shareInstance.database?.executeUpdate("Insert into parent_parent_request_attach (id_attach, feature_type_id, feature_id, file_name, school_id, added_on, parent_request, extension, file_path) values (?,?,?,?,?,?,?,?,?)", withArgumentsIn: [attachments.id, attachments.feature_type_id,attachments.feature_id, attachments.file_name, attachments.school_id, attachments.added_on, attachments.parent_request, attachments.extension_file, attachments.file_path]))!
            shareInstance.database?.close()
        }
        return isSave
    }
    
    func get_Parent_attach_Info(attachId: String) -> attachmentModel{
        shareInstance.database?.open()
        let attachmentData = attachmentModel()
        let query = try! shareInstance.database?.executeQuery("select * from parent_parent_request_attach where `id_attach` = \(attachId) limit 1", values: nil)
        while (query?.next())! {
            attachmentData.id              = query?.string(forColumn: "id_attach") ?? ""
            attachmentData.feature_type_id = query?.string(forColumn: "feature_type_id") ?? ""
            attachmentData.feature_id      = query?.string(forColumn: "feature_id") ?? ""
            attachmentData.file_name       = query?.string(forColumn: "file_name") ?? ""
            attachmentData.school_id       = query?.string(forColumn: "school_id") ?? ""
            attachmentData.added_on        = query?.string(forColumn: "added_on") ?? ""
            attachmentData.parent_request  = query?.string(forColumn: "parent_request") ?? ""
            attachmentData.extension_file  = query?.string(forColumn: "extension") ?? ""
            attachmentData.file_path       = query?.string(forColumn: "file_path") ?? ""
        }
        return attachmentData
    }
    
    
    func get_parent_request_attach_List(_ id_parent_request: String) -> [attachmentModel]{
        var haveAttachs = false
        shareInstance.database?.open()
        var attachList = [attachmentModel]()
        let query = try! shareInstance.database?.executeQuery("select * from parent_parent_request_attach where feature_id = \(id_parent_request)", values: nil)
        while (query?.next())! {
            let attachmentFile = attachmentModel()
            haveAttachs = true
            attachmentFile.id              = query?.string(forColumn: "id_attach") ?? "0"
            attachmentFile.feature_type_id = query?.string(forColumn: "feature_type_id") ?? ""
            attachmentFile.feature_id      = query?.string(forColumn: "feature_id") ?? ""
            attachmentFile.file_name       = query?.string(forColumn: "file_name") ?? ""
            attachmentFile.school_id       = query?.string(forColumn: "school_id") ?? ""
            attachmentFile.added_on        = query?.string(forColumn: "added_on") ?? ""
            attachmentFile.parent_request  = query?.string(forColumn: "parent_request") ?? ""
            attachmentFile.extension_file  = query?.string(forColumn: "extension") ?? ""
            attachmentFile.file_path       = query?.string(forColumn: "file_path") ?? ""
            attachList.append(attachmentFile)
        }
        return attachList
    }
    
}

