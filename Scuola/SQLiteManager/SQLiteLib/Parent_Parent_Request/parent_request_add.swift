//
//  parent_request_add.swift
//  School
//
//  Created by ISCA-IOS on 7/23/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation
extension DatabaseManager{
    
    func create_Parent_request_add_InfoTable() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS `parent_request_add` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `student_id` TEXT, `teacher_user_id` TEXT, `teacher_name` TEXT , `request_from` TEXT , `request` TEXT , `class_id` TEXT , `school_id` TEXT)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("test 3 failed: \(error.localizedDescription)")
            return false
        }
    }
    
    func save_Parent_request_add_Info(_ parentRequest: parentRequestModel) -> Bool{
            shareInstance.database?.open()
            let isSave = (shareInstance.database?.executeUpdate("Insert into parent_request_add (student_id, teacher_user_id, teacher_name, request_from, request, class_id, school_id) values (?,?,?,?,?,?,?)", withArgumentsIn: [parentRequest.student_id, parentRequest.teacher_user_id, parentRequest.teacher_name, parentRequest.request_from, parentRequest.request, parentRequest.class_id, parentRequest.school_id]))!
            shareInstance.database?.close()
        
        return isSave
    }
    
    func get_parent_request_add_data(student_id: String) -> parentRequestModel {
        shareInstance.database?.open()
        let parentRequestData = parentRequestModel()
        let query = try! shareInstance.database?.executeQuery("select * from parent_request_add where `student_id` = \(student_id) limit 1", values: nil)
        while (query?.next())! {
            parentRequestData.student_id = query?.string(forColumn: "student_id") ?? ""
            parentRequestData.teacher_user_id = query?.string(forColumn: "teacher_user_id") ?? ""
            parentRequestData.teacher_name = query?.string(forColumn: "teacher_name") ?? ""
            parentRequestData.request_from = query?.string(forColumn: "request_from") ?? ""
            parentRequestData.request = query?.string(forColumn: "request") ?? ""
            parentRequestData.class_id = query?.string(forColumn: "class_id") ?? ""
            parentRequestData.school_id = query?.string(forColumn: "school_id") ?? ""
        }
        return parentRequestData
    }
    
    
    func delete_parent_request_add(student_id: String) -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from `parent_request_add` where `student_id` = \(student_id)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        
    }
    
    
    func delete_Parent_request_add_Table() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("DROP TABLE IF EXISTS `parent_request_add`", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        
    }
    
}

