//
//  Parent_Parent_Request.swift
//  School
//
//  Created by ISCA-IOS on 7/22/19.
//  Copyright © 2019 ISCA-IOS-SCHOOL. All rights reserved.
//

import Foundation

extension DatabaseManager{
    // type_member 1 => parent, 2 => teacher, 3 => management
    func create_Parent_parent_request_InfoTable() -> Bool{
        print("test dd 1")
        do {
            print("test dd 2")
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("CREATE TABLE IF NOT EXISTS parent_parent_request ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `id_parent_request` INTEGER, `request` TEXT,  `req_status` TEXT, `reply` TEXT, `request_from` TEXT, `teacher_user_id` TEXT, `school_id` TEXT, `create_date` TEXT, `reply_date` TEXT, `student_id` TEXT, `class_id` TEXT, `name` TEXT, `class_name` TEXT, `student_photo` TEXT, `teacher_name` TEXT, `type_member` TEXT)", values: nil)
            print("create 1")
            while rs!.next() {
                print("create 2")
            }
            shareInstance.database?.close()
            return true
        } catch let error {
            print("error insertion failed: \(error)")
        }
    }
    
    func save_Parent_parent_request_Info(_ parentRequest: parentRequestModel) -> Bool{
        let if_exist = self.get_Parent_parent_request_Info(requestId: String(parentRequest.id))
        var isSave: Bool = false
        if if_exist.id == 0{
            shareInstance.database?.open()
            isSave = (shareInstance.database?.executeUpdate("Insert into parent_parent_request (id_parent_request, request, req_status, reply, request_from, teacher_user_id, school_id,create_date, reply_date, student_id, class_id, name, student_photo, teacher_name, type_member) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", withArgumentsIn: [parentRequest.id, parentRequest.request, parentRequest.req_status, parentRequest.reply, parentRequest.request_from, parentRequest.teacher_user_id, parentRequest.school_id, parentRequest.create_date, parentRequest.reply_date, parentRequest.student_id, parentRequest.class_id, parentRequest.name, parentRequest.student_photo, parentRequest.teacher_name, parentRequest.type_member]))!
            shareInstance.database?.close()
        }
        
        return isSave
    }
    
    func get_Parent_parent_request_Info(requestId: String) -> parentRequestModel{
        shareInstance.database?.open()
        let parentRequestData = parentRequestModel()
        let query = try! shareInstance.database?.executeQuery("select * from parent_parent_request where `id_parent_request` = \(requestId) limit 1", values: nil)
        while (query?.next())! {
            parentRequestData.id = Int(query?.int(forColumn: "id_parent_request") ?? 0)
            parentRequestData.request = query?.string(forColumn: "request") ?? ""
            parentRequestData.req_status = query?.string(forColumn: "req_status") ?? ""
            parentRequestData.reply = query?.string(forColumn: "reply") ?? ""
            parentRequestData.request_from = query?.string(forColumn: "request_from") ?? ""
            parentRequestData.teacher_user_id = query?.string(forColumn: "teacher_user_id") ?? ""
            parentRequestData.school_id = query?.string(forColumn: "school_id") ?? ""
            parentRequestData.create_date = query?.string(forColumn: "create_date") ?? ""
            parentRequestData.reply_date = query?.string(forColumn: "reply_date") ?? ""
            parentRequestData.student_id = query?.string(forColumn: "student_id") ?? ""
            parentRequestData.class_id = query?.string(forColumn: "class_id") ?? ""
            parentRequestData.name = query?.string(forColumn: "name") ?? ""
            parentRequestData.student_photo = query?.string(forColumn: "student_photo") ?? ""
            parentRequestData.teacher_name = query?.string(forColumn: "teacher_name") ?? ""
        }
        return parentRequestData
    }
    
    
    
    
    func get_parent_request_List(_ statusRequest: String, _ type_member: String, _ complition: @escaping(_ haveRequests: Bool, _ requests: [parentRequestModel]) -> Void){
        var haveRequests = false
        shareInstance.database?.open()
        var requestList = [parentRequestModel]()
        let query = try! shareInstance.database?.executeQuery("select * from parent_parent_request where req_status = \(statusRequest) and type_member = \(type_member) order by id_parent_request desc", values: nil)
        while (query?.next())! {
            let requestField = parentRequestModel()
            haveRequests = true
            requestField.id             = Int(query?.int(forColumn: "id_parent_request") ?? 0)
            requestField.request        = query?.string(forColumn: "request") ?? ""
            requestField.req_status     = query?.string(forColumn: "req_status") ?? ""
            requestField.reply          = query?.string(forColumn: "reply") ?? ""
            requestField.request_from   = query?.string(forColumn: "request_from") ?? ""
            requestField.teacher_user_id = query?.string(forColumn: "teacher_user_id") ?? ""
            requestField.school_id      = query?.string(forColumn: "school_id") ?? ""
            requestField.create_date    = query?.string(forColumn: "create_date") ?? ""
            requestField.reply_date     = query?.string(forColumn: "reply_date") ?? ""
            requestField.student_id     = query?.string(forColumn: "student_id") ?? ""
            requestField.class_id       = query?.string(forColumn: "class_id") ?? ""
            requestField.name           = query?.string(forColumn: "name") ?? ""
            requestField.class_name     = query?.string(forColumn: "class_name") ?? ""
            requestField.student_photo  = query?.string(forColumn: "student_photo") ?? ""
            requestField.teacher_name   = query?.string(forColumn: "teacher_name") ?? ""
            requestField.attachment = self.get_parent_request_attach_List(query?.string(forColumn: "id_parent_request") ?? "0")
            requestList.append(requestField)
        }
        complition(haveRequests, requestList)
    }
    
    
    
    
    func delete_Parent_request_Table() -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("DROP TABLE IF EXISTS parent_parent_request", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
        
    }
    
    func remove_Parent_request_data(_ member_type: String, _ req_status: String, _ student_id: String) -> Bool{
        do {
            shareInstance.database?.open()
            let rs = try! shareInstance.database?.executeQuery("delete from parent_parent_request where type_member = \(member_type) and req_status = \(req_status) and student_id = \(student_id)", values: nil)
            while rs!.next() {
            }
            shareInstance.database?.close()
            return true
        } catch {
            print("failed: \(error.localizedDescription)")
            return false
        }
    }
    
}

